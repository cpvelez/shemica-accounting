function includeModal(ok_url)
{
	document.write('\
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">	\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div name="modalHeader" class="modal-header" style="background-color: #990000; color: white">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<i name="warning" class="fa fa-warning fa-fw" style="color: #FFCC00; size: 14; visibility: hidden"></i>\
					<i name="check" class="fa fa-check fa-fw" style="color: green; size: 14; visibility: hidden"></i>\
					<label class="modal-title" name="modalTitle">Warning</label>\
				</div>\
				<div class="modal-body">\
					<label name="modalBody">Remarks</label>\
				</div>\
				<div class="modal-footer">\
					<a href="' + ok_url + '"><button href="" type="button" name="btnSuccess" class="btn btn-success" style="visibility: hidden">OK</button></a>\
					<button href="" type="button" name="btnError" class="btn btn-danger" style="visibility: hidden"data-dismiss="modal">OK</button>\
				</div>\
			</div>\
			<!-- /.modal-content -->\
		</div>\
		<!-- /.modal-dialog -->\
	</div>\
	');
}

function includeModalWithSecondButton(ok_url, ok_url2)
{
	document.write('\
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">	\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div name="modalHeader" class="modal-header" style="background-color: #990000; color: white">\
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>\
					<i name="warning" class="fa fa-warning fa-fw" style="color: #FFCC00; size: 14; visibility: hidden"></i>\
					<i name="check" class="fa fa-check fa-fw" style="color: green; size: 14; visibility: hidden"></i>\
					<label class="modal-title" name="modalTitle">Warning</label>\
				</div>\
				<div class="modal-body">\
					<label name="modalBody">Remarks</label>\
				</div>\
				<div class="modal-footer">\
					<a href="' + ok_url + '"><button href="" type="button" name="btnSuccess" class="btn btn-success" style="visibility: hidden">OK</button></a>\
					<a href="' + ok_url2 + '"><button href="" type="button" name="btnSuccess2" class="btn btn-success" style="visibility: hidden">Add Another</button></a>\
					<button href="" type="button" name="btnError" class="btn btn-danger" style="visibility: hidden"data-dismiss="modal">OK</button>\
				</div>\
			</div>\
			<!-- /.modal-content -->\
		</div>\
		<!-- /.modal-dialog -->\
	</div>\
	');
}

function displaySuccessModal(message)
{
	$('#modal').find("button[name$='btnSuccess']").attr("style", "display: initial");
	$('#modal').find("button[name$='btnSuccess2']").attr("style", "display: none");
	$('#modal').find("button[name$='btnError']").attr("style", "display: none");
	$('#modal').find("i[name$='check']").attr("style", "display: initial");
	$('#modal').find("i[name$='warning']").attr("style", "display: none");
	$('#modal').find("label[name$='modalTitle']").html("Successful");
	$('#modal').find("label[name$='modalBody']").html(message);
	$('#modal').find("div[name$='modalHeader']").attr("style", "background-color: #009900; color: white");
	$('#modal').modal('show');
}

function displaySuccessModal2Buttons(message)
{
	$('#modal').find("button[name$='btnSuccess']").attr("style", "display: initial");
	$('#modal').find("button[name$='btnSuccess2']").attr("style", "display: initial");
	$('#modal').find("button[name$='btnSuccess2']").attr("style", "display: initial");
	$('#modal').find("button[name$='btnError']").attr("style", "display: none");
	$('#modal').find("i[name$='check']").attr("style", "display: initial");
	$('#modal').find("i[name$='warning']").attr("style", "display: none");
	$('#modal').find("label[name$='modalTitle']").html("Successful");
	$('#modal').find("label[name$='modalBody']").html(message);
	$('#modal').find("div[name$='modalHeader']").attr("style", "background-color: #009900; color: white");
	
	
	
	$('#modal').modal('show');
}

function displayErrorModal(message)
{
	$('#modal').find("button[name$='btnSuccess']").attr("style", "display: none");
	$('#modal').find("button[name$='btnSuccess2']").attr("style", "display: none");
	$('#modal').find("button[name$='btnError']").attr("style", "display: initial");
	$('#modal').find("i[name$='check']").attr("style", "display: none");
	$('#modal').find("i[name$='warning']").attr("style", "display: initial");
	$('#modal').find("label[name$='modalTitle']").html("Error");
	$('#modal').find("label[name$='modalBody']").html(message);
	$('#modal').find("div[name$='modalHeader']").attr("style", "background-color: #990000; color: white");
	$('#modal').modal('show');
}
