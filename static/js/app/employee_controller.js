function addEmployee() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		$.ajax({
			url: '/entities/employees/add',
			type: 'POST',
			data: {
				'employee_first_name': $( "#employee_first_name" ).val(),
				'employee_middle_name': $( "#employee_middle_name" ).val(),
				'employee_last_name': $( "#employee_last_name" ).val(),
				'employee_email': $( "#employee_email" ).val(),
				'employee_position': $( "#employee_position" ).val(),
				'employee_department': $( "#employee_department" ).val(),
				'employee_type': $( "#employee_type" ).val(),
				'employee_access_type': $( "#employee_access_type" ).val(),
				'employee_active_status': $( "#employee_active_status" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}else if(data=="duplicate") {
						displayErrorModal("Employee has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
	    });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function editEmployee() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		employee_key = $( "#employee_edit_key" ).val(),
		$.ajax({
			url: '/entities/employees/edit/'+employee_key,
			type: 'POST',
			data: {
				'employee_first_name': $( "#employee_first_name" ).val(),
				'employee_middle_name': $( "#employee_middle_name" ).val(),
				'employee_last_name': $( "#employee_last_name" ).val(),
				'employee_email': $( "#employee_email" ).val(),
				'employee_department': $( "#employee_department" ).val(),
				'employee_position': $( "#employee_position" ).val(),
				'employee_type': $( "#employee_type" ).val(),
				'employee_access_type': $( "#employee_access_type" ).val(),
				'employee_active_status': $( "#employee_active_status" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}else if(data=="duplicate") {
						displayErrorModal("Employee has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
	  });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	  }
	return false;
}

function addVendor() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		$.ajax({
			url: '/entities/vendors/add',
			type: 'POST',
			data: {
				'vendor_name': $( "#vendor_name" ).val(),
				'vendor_short_name': $( "#vendor_short_name" ).val(),
				'vendor_address': $( "#vendor_address" ).val(),
				'vendor_TIN': $( "#vendor_TIN" ).val(),
				'vendor_is_VAT_registered': $( "#vendor_is_VAT_registered").val(),
				'vendor_contact_no': $( "#vendor_contact_no" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else if(data=="duplicate") {
						displayErrorModal("Vendor name has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
		  
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
	    });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
  }
  return false;
}

function editVendor() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       }
    })
	if(empty == 0) {
		vendor_key = $( "#vendor_edit_key" ).val(),
		$.ajax({
			url: '/entities/vendors/edit/'+vendor_key,
			type: 'POST',
			data: {
				'vendor_name': $( "#vendor_name" ).val(),
				'vendor_short_name': $( "#vendor_short_name" ).val(),
				'vendor_address': $( "#vendor_address" ).val(),
				'vendor_TIN': $( "#vendor_TIN" ).val(),
				'vendor_is_VAT_registered': $( "#vendor_is_VAT_registered" ).val(),
				'vendor_contact_no': $( "#vendor_contact_no" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else if(data=="duplicate") {
						displayErrorModal("Vendor name has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
	  });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function addClient() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		$.ajax({
			url: '/entities/clients/add',
			type: 'POST',
			data: {
				'client_name': $( "#client_name" ).val(),
				'contact_no': $( "#contact_no" ).val(),
				'active_status': $( "#active_status" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else if(data=="duplicate") {
						displayErrorModal("Client has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
		  
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
			}
	    });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
  }
  return false;
}

function editClient() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	if(empty == 0) {
		client_key = $( "#client_edit_key" ).val(),
		$.ajax({
			url: '/entities/clients/edit/'+client_key,
			type: 'POST',
			data: {
				'client_name': $( "#client_name" ).val(),
				'contact_no': $( "#contact_no" ).val(),
				'active_status': $( "#active_status" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else if(data=="duplicate") {
						displayErrorModal("Client has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
			}
	  });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	  }
	 return false;
}

function create_dummy_email()
{
	var first_name = $( "#employee_first_name" ).val();
	var middle_name = $( "#employee_middle_name" ).val();
	var last_name = $( "#employee_last_name" ).val();
	var dummy_email = first_name + "_" + middle_name + "_" + last_name;
	dummy_email = dummy_email.replaceAll("@", "");
	dummy_email = dummy_email.replaceAll(".", "");
	dummy_email = dummy_email.replaceAll(",", "");
	dummy_email = dummy_email.replaceAll("'", "");
	dummy_email = dummy_email.replaceAll(" ", "");
	dummy_email = dummy_email + "@dummy_email.com";
	$( "#employee_email" ).val(dummy_email);
}