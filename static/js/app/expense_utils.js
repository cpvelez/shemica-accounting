// constants
expense_type_id = "expense_type"
expense_item_name = "item_name"
expense_trans_type_id = "transaction_type"
// constructor for expense type
function addExpense(expval, exptranstype, expname, expkeywords)
{
	this.expval  = expval;
	this.exptranstype  = exptranstype;
    this.expname = expname;
    this.expkeywords  = expkeywords;
}

// List expenses and their values
var exp_list = [
	new addExpense( 0, 1, "13th Month Pay", "13th month" ),
	new addExpense( 1, 1, "Brokerage Fee", "broker brokerage reality air" ),
	new addExpense( 2, 1, "Business Registration", "business registration dti brgy bir 2000 permit 0605 license clearance sec" ),
	new addExpense( 3, 1, "Cash Advance", "advance" ),
	new addExpense( 4, 1, "Communication", "load communication globe smart pldt" ),
	new addExpense( 5, 1, "Construction", "construction" ),
	new addExpense( 6, 1, "Listing Fee", "listing distribution display intro" ),
	new addExpense( 7, 1, "Donation", "donation charity assistance" ),
	new addExpense( 8, 0, "Gas, Fuel and Oil", "gas fuel oil motor shell petron caltex" ),
	new addExpense( 9, 1, "Import Fees", "import duty wharfage arrastre demurrage" ),
	new addExpense( 10, 1, "Income Tax", "income tax 1702" ),
	new addExpense( 11, 1, "Insurance", "insurance" ),
	new addExpense( 12, 0, "Inventory - Goods", "import coffee noodles goods aik cheong uniben" ),
	new addExpense( 13, 0, "Machineries & Equipment", "machine equipment" ),
	new addExpense( 14, 0, "Marketing", "marketing printing agency ad facebook wobbler atake social media influencer diabolic plan z atake near" ),
	new addExpense( 15, 0, "Meals", "food jollibee mcdo" ),
	new addExpense( 16, 0, "Meeting, Seminars and Teambuilding", "meeting seminar teambuilding outing travel" ),
	new addExpense( 17, 0, "Minor Properties", "container cctv machine shaker dispenser jug tool cooler chest tables chair furniture" ),
	new addExpense( 18, 0, "Miscellaneous", "miscellanous storehub cc card terminal bdo" ),
	new addExpense( 19, 1, "Office Supplies", "pen pad staple scotch tape correction a4 office national book booklet receipt slip"),
	new addExpense( 20, 1, "Other Current Assets", "load deposit" ),
	new addExpense( 21, 1, "Other Employee Benefits", "allowance uniform benefit" ),
	new addExpense( 22, 1, "Pag-ibig EE", "pag ibig ee hdmf calamity loan" ),
	new addExpense( 23, 1, "Pag-ibig ER", "pag ibig er hdmf" ),
	new addExpense( 24, 1, "Pantry Supplies", "pantry supply water refill medicine mercury" ),
	new addExpense( 25, 1, "Philhealth EE", "philhealth ee"),
	new addExpense( 26, 1, "Philhealth ER", "philhealth er"),
	new addExpense( 27, 1, "Postage and Courier Charges", "lbc padala del priority notary postage grab lalamove"),
	new addExpense( 28, 1, "Professional Fees", "accounting shannen mamuri fees professional pf"),
	new addExpense( 29, 1, "Rent", "rent t12 pkc" ),
	new addExpense( 30, 0, "Repairs & Maintenance", "screw carpenter cris nail wood maintenance repair" ),
	new addExpense( 31, 1, "Salary & Allowances", "payroll salary allowance" ),
	new addExpense( 32, 1, "Security Services", "gold cross security"),
	new addExpense( 33, 1, "SSS EE", "sss ee loan calamity"),
	new addExpense( 34, 1, "SSS ER", "sss er"),
	new addExpense( 35, 1, "Taxes & Licenses", "tax bir license fda permit ipo"),
	new addExpense( 36, 0, "Transportation Equipment", "truck motor"),
	new addExpense( 37, 1, "Transportation, Travel, Parking & Toll", "transpo fare parking toll"),
	new addExpense( 38, 1, "Utilities", "meralco water util utilities"),
	new addExpense( 39, 1, "VAT", "vat 2550 VAT bir" ),
	new addExpense( 40, 0, "Warehouse Equipment", "racking warehouse equipment reach forklift lift pallet" ),
	new addExpense( 41, 0, "Warehouse Supplies", "warehouse supply stretch film tape" ),
	new addExpense( 42, 1, "Withholding tax - compensation", "1601-c compensation withholding expanded bir" ),
	new addExpense( 43, 1, "Withholding tax - expanded", "1601-e withholding expanded bir" ),
	new addExpense( 44, 1, "Loan Interest", "loan interest" ),
	new addExpense( 45, 0, "Office Equipment", "cabinet office equipment printer laptop computer" ),
	new addExpense( 46, 1, "Bank Charges", "bdo dst charges transfer bank convenience cable" ),
	new addExpense( 47, 1, "Shipment and Forwarding Fees", "rcl shipment forwarding priority trucking" ),
	new addExpense( 48, 1, "IT Fees", "google it" ),
	new addExpense( 49, 1, "Custom Tax", "customs tax" ),
	new addExpense( 50, 0, "Marketing Supplies", "cups bowls cups spoon fork" ),
	new addExpense( 51, 0, "Research and Development", "research development product lab test" ),
	new addExpense( 52, 0, "Loan Payment", "loan payment principal" ),
	new addExpense( 53, 0, "Marketing - Key Visual", "poster design print wobbler topper vertical talker signage billboard sintra booth tarp" ),
	new addExpense( 54, 0, "Marketing - Merchandising", "merchandise merchandiser coordinator diser" ),
	new addExpense( 55, 0, "Marketing - Merchandising (Payola)", "payola" ),
	new addExpense( 56, 0, "Marketing - Digital", "digital shopee lazada ads online facebook fb" ),
	new addExpense( 57, 0, "Marketing - Outlet Support", "outlet support display anniversary christmas promo samp sampling" ),
	new addExpense( 58, 0, "Logistics - Delivery", "delivery trip driver helper logistics truck suzuki mistubishi moreta gas fuel toll parking shipment unicagracia" ),
	new addExpense( 59, 0, "Non-recurring Professional Fees", "greg pf" ),
	new addExpense( 60, 0, "Logistics - Transfer", "logistics transfer" ),
	new addExpense( 61, 0, "Final Pay", "final pay termination separation" )
];
// function that classifies expense to a particular expense type based on name/keyword
function classifyAndSetExpense()
{
	// retrieve user input to lowercase and break down
    var user_input = getExpenseParticulars() + " " + getExpenseVendor() + " " + getExpensePurpose();
	console.log("User Input: " + user_input);
	user_input = user_input.toLowerCase();
    var user_arr   = user_input.split(" ");
	var result = -1;
	
	// initialize form value
    document.getElementById(expense_type_id).value = -1;
	
	result = classifyExpense(user_arr);
	
	// sets expense_type dropdown to highest occuring expense type
	document.getElementById(expense_type_id).value = result;
	document.getElementById(expense_trans_type_id).value = getTransactionTypeFromExpenseType(result);
}

function getTransactionTypeFromExpenseType(expense_type)
{
	idx = getExpIndexFromCode(expense_type)
	if (idx == -1)
	{
		return -1;
	}
	else
	{
		return exp_list[idx].exptranstype;
	}
}

// actual classification
function classifyExpense(user_arr)
{
    var classificationArray = [];
	var keywords_list;
	var matchScore = -1;
	
	
	
    // loop through user input and search for a match
    for ( var j = 0; j < exp_list.length; j++ )
    {
        for( var i = 0; i < user_arr.length; i++ )
        {
			keywords_list =  exp_list[j].expkeywords.split(" ");
			
			for (var k = 0; k < keywords_list.length; k++)
			{
				matchScore = user_arr[i].toLowerCase().search(keywords_list[k].toLowerCase());
				if (matchScore == 0 )
				{
					//add classification in an array
					classificationArray.push(exp_list[j].expval);
					console.log("Pushed: " + exp_list[j].expval + " " + user_arr[i] + ":" + keywords_list[k]);
				}
			}
        }
    }
	
	var frequency = {};  // array of frequency.
	var max = 0;  // holds the max frequency.
	var result = -1;   // holds the max frequency element.
	for(var v in classificationArray) {
			frequency[classificationArray[v]]=(frequency[classificationArray[v]] || 0)+1; // increment frequency.
			if(frequency[classificationArray[v]] > max) { // is this frequency > max so far ?
					max = frequency[classificationArray[v]];  // update max.
					result = classificationArray[v];          // update result.
			}
	}
	
    return result;
}

// function for adding options to dropdown list
function populateDropdown(expense_type_id)
{
    var sel = document.getElementById(expense_type_id);
    
    // loop until all options are added
    for(var i = 0; i < exp_list.length; i++)
    {
        // get option details
        var newOpt = document.createElement( "option" );
        newOpt.textContent = exp_list[i].expname;
        newOpt.value = exp_list[i].expval;
        // append to list
        sel.appendChild( newOpt );
    }
}

// function for adding options to dropdown list
function populateExpenseTypeFilterDropdown(exp_type_filter_id)
{
    var sel = document.getElementById(exp_type_filter_id);
	var opts = [];
    
	// Add All
	var newOpt = document.createElement( "option" );
        newOpt.textContent = "All";
        newOpt.value = "All";
		newOpt.className = "shortened_text"
        // append to list
        sel.appendChild( newOpt );
	
	// Add Unclassified
	var newOpt = document.createElement( "option" );
        newOpt.textContent = "Unclassified";
        newOpt.value = "-1";
		newOpt.className = "shortened_text"
        // append to list
        sel.appendChild( newOpt );

    // loop until all options are added
    for(var i = 0; i < exp_list.length; i++)
    {
        // get option details
        var newOpt = document.createElement( "option" );
        newOpt.textContent = exp_list[i].expname;
        newOpt.value = exp_list[i].expval;
		newOpt.className = "shortened_text"
        // append to list
        sel.appendChild( newOpt );
    }
 
    // Sort Options
    for (var i=sel.options.length-1; i >= 2; i--)
        opts.push( sel.removeChild(sel.options[i]) );

    // Sort them
    opts.sort(function (a, b) { 
        return a.innerHTML.localeCompare( b.innerHTML );
    });

    // Put them back into the <select>
    while(opts.length)
    {
        sel.appendChild(opts.shift());
    }
}

// sorts dropdown list entries
function sortOptions(list_id)
{
    var sel = document.getElementById( list_id );
    var opts = [];

    // Extract the elements into an array
    for (var i=sel.options.length-1; i >= 2; i--)
        opts.push( sel.removeChild(sel.options[i]) );

    // Sort them
    opts.sort(function (a, b) { 
        return a.innerHTML.localeCompare( b.innerHTML );
    });

    // Put them back into the <select>
    while(opts.length)
    {
        sel.appendChild(opts.shift());
    }
}

function getExpenseParticulars()
{
	particulars = "";
	i = 0;
	$( "input[name$='item_name']" ).each(function(){
		if ( i == 0)
		{
			particulars += this.value;
		}
		else
		{
			particulars += " " + this.value;
		}
		i++;
	});
	return particulars;
}

function getExpenseVendor()
{
	vendor = "";
	vendorSelect = document.getElementById('vendor_id');
	if (vendorSelect)
	{
		vendor = vendorSelect.options[ vendorSelect.selectedIndex ].text;
	}
	return vendor;
}

function getExpensePurpose()
{
	purpose = "";
	purposeElem = document.getElementById('release_fund_purpose');
	if(purposeElem)
	{
		purpose = purposeElem.value;
	}
	return purpose;
}

function getExpenseBudgetName()
{
	budgetname = "";
	budgetnameElem = document.getElementById('budget_name');
	if(budgetnameElem)
	{
		budgetname = budgetnameElem.value;
	}
	return budgetname;
}

function getExpenseTypeNameFromCode(exp_code)
{	
	idx = getExpIndexFromCode(exp_code)
	
	if (idx == -1)
	{
		return "Unknown";
	}
	else
	{
		return exp_list[idx].expname;
	}
}

function getExpIndexFromCode(exp_code)
{
	explist_len = exp_list.length;
	for (i = 0; i < explist_len; i++) {

		if (exp_list[i].expval == exp_code)
		{
			return i;
		}
	}
	return -1;
}

function addExpenseTypeinSpan(elem_id, exp_code)
{
	var expense_type_name = "";
	span_elem = document.getElementById(elem_id);
	
	if (exp_code == -1)
	{
		expense_type_name = "Unclassified"
	}
	else
	{
		expense_type_name= getExpenseTypeNameFromCode(exp_code);
	}
	span_elem.innerHTML = expense_type_name;
	span_elem.title = expense_type_name;
}