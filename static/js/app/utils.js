//Config
BUFFER_DAYS = 7;
CHECK_BUFFER_DAYS = 3;

function round(value, decimals) {
    return Number(value).toFixed(decimals);
}

function numberWithCommas(x) {
	x = round(x, 2);
	return x.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
}

function formatAllCurrency()
{
	$('.number').each(function(){
		if(!isNaN(parseFloat(this.innerHTML)))
		{
			this.innerHTML = numberWithCommas(this.innerHTML);
		}
    });
}

function formatThisNumber(object)
{
	if(!isNaN(parseFloat(object.html())))
	{
		object.html(numberWithCommas(object.html()));
	}
}

function shortenTexts()
{
	var textlimit = 50;
	$(".shortened_text").map(function() {
		var wholeText = this.innerHTML;
		if(wholeText.length > textlimit)
		{
			shortenedText = wholeText.substring(0,textlimit) + "...";
			this.innerHTML = shortenedText;
		}
		this.title = wholeText;
    });
}

function writePercentage(id, actual, total)
{
	if (total != 0)
	{
		document.getElementById(id).innerHTML = Math.round(actual / total * 100 * 100 / 100) + "%";
	}
	else
	{
		document.getElementById(id).innerHTML = "0%";
	}
}

function decodeToHTML(textToDecode)
{
	return $('<div>').html(textToDecode).text();
}
	
// create DIV element and append to the table cell
function createDivCell(cell, text, style, row_number) {
	var div = document.createElement('div'), // create DIV element
		txt = document.createTextNode(text), // create text node
		input = document.createElement('input');
	div.appendChild(txt);                    // append text node to the DIV
	div.setAttribute('class', style);        // set DIV class attribute
	div.setAttribute('className', style);    // set DIV class attribute for IE (?!)
	input.type = 'hidden';
	input.setAttribute('class', 'input-time-sheet');        // set hidden class attribute
	input.setAttribute('className', 'input-time-sheet');    // set hidden class attribute for IE (?!)
	input.setAttribute('name', 'date'+row_number);
	input.value = text;
	cell.appendChild(div);                   // append DIV to the table cell
	cell.appendChild(input);
}

// append row to the HTML table
function appendExpenseItemRow(table_name) {
	var tbl = document.getElementById(table_name); // table reference
	var max_tbl_row_cnt = 15;
	// check if max table count is reached
	if (tbl.rows.length - 2 == max_tbl_row_cnt)
	{
		alert("Max rows reached. " + "Max: " + max_tbl_row_cnt);
		return;
	}
	var row = tbl.insertRow(tbl.rows.length-1);      // append table row;
		
	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
		
	// insert table cells to the new row
	var cellIdx = 0;
	createInputCell(idx, row.insertCell(cellIdx), "", "text", "item_name", 'name', true);
	cellIdx++;
	//createInputCell(idx, row.insertCell(cellIdx), "", "number", "qty", '', false);
	//cellIdx++;
	//createUnitsSelectionCell(row.insertCell(cellIdx), "", "units", '', false, "");
	//cellIdx++;
	createInputCell(idx, row.insertCell(cellIdx), "", "number", "expense_amount", 'expense', true);
	cellIdx++;
	createInputCell(idx, row.insertCell(cellIdx), "0.0", "number", "withholding_tax", 'wtax', true);
	cellIdx++;
	createInputCell(idx, row.insertCell(cellIdx), "0.0", "number", "input_tax", 'inputtax', true);
}

// append charge item row
function appendChargeItemRow(table_name) {
	var tbl = document.getElementById(table_name), // table reference
		row = tbl.insertRow(tbl.rows.length-1);      // append table row;
		
	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
		
	// insert table cells to the new row
	var cellIdx = 0;
	createChargeItemInputCell(idx, row.insertCell(cellIdx), "", "text", "item_name", 'name', true);
	cellIdx++;
	createChargeItemInputCell(idx, row.insertCell(cellIdx), "", "number", "expense_amount", 'expense', true);
}

// append charge row
function appendChargeRow(table_name) {
	var tbl = document.getElementById(table_name); // table reference
	var max_tbl_row_cnt = 12;
	// check if max table count is reached
	if (tbl.rows.length - 2 == max_tbl_row_cnt)
	{
		alert("Max rows reached. " + "Max: " + max_tbl_row_cnt);
		return;
	}
	
	var row = tbl.insertRow(tbl.rows.length-1);      // append table row;
		
	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
		
	// insert table cells to the new row
	var cellIdx = 0;
	// create charge_to_id select cell
	createEmployeeDropdownCell(idx, row.insertCell(cellIdx), "", "charged_to_id");
	cellIdx++;
	
	// create department select cell
	createDepartmentDropdownCell(idx, row.insertCell(cellIdx), "", "department");
	cellIdx++;
	
	// create charge amount cell
	createChargeInputCell(idx, row.insertCell(cellIdx), "", "number", "charge_amount", 'charge', true);
	cellIdx++;
}

// append charge row
function appendSettlementRow(table_name) {
	var tbl = document.getElementById(table_name), // table reference
		row = tbl.insertRow(tbl.rows.length-1);      // append table row;
		
	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
		
	// insert table cells to the new row
	var cellIdx = 0;
	// create charge_to_id select cell
	createEmployeeDropdownCell(idx, row.insertCell(cellIdx), "", "credit_to_id");
	cellIdx++;
	
	// create department select cell
	createDepartmentDropdownCell(idx, row.insertCell(cellIdx), "", "department");
	cellIdx++;

	// create charge amount cell
	createSettlementInputCell(idx, row.insertCell(cellIdx), "", "number", "settled_amount", 'settled_amount', true);
	cellIdx++;
}

// append trip expense row to the HTML table
function appendExpenseRow() {
	var tbl = document.getElementById('expense_table'), // table reference
		row = tbl.insertRow(tbl.rows.length-1);      // append table row;

	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
	var max_tbl_row_cnt = 15;
	// check if max table count is reached
	if (tbl.rows.length - 2 == max_tbl_row_cnt)
	{
		alert("Max rows reached. " + "Max: " + max_tbl_row_cnt);
		return;
	}
	
	// insert table cells to the new row
	var cellIdx = 0;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "-", "text", "OR_number", 'OR_number', true);
	cellIdx++;
	createVendorDropdownCell(idx, row.insertCell(cellIdx), "", "vendor");
	cellIdx++;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "", "text", "item_name", 'name_multiple', true);
	cellIdx++;
	createExpenseTypeDropdownCell(idx, row.insertCell(cellIdx), "", "expense_type");
	cellIdx++;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "", "number", "expense_amount", 'expense', true);
	cellIdx++;

}

// append trip expense row to the HTML table
function appendExpenseSingleORRow() {
	var tbl = document.getElementById('expense_table'), // table reference
		row = tbl.insertRow(tbl.rows.length-1);      // append table row;

	row.setAttribute('class', 'table-data');
	row.setAttribute('className', 'table-data');
	idx = tbl.rows.length - 2;
	var max_tbl_row_cnt = 15;
	// check if max table count is reached
	if (tbl.rows.length - 2 == max_tbl_row_cnt)
	{
		alert("Max rows reached. " + "Max: " + max_tbl_row_cnt);
		return;
	}
	
	// insert table cells to the new row
	var cellIdx = 0;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "", "text", "item_name", 'name_multiple_single_or', true);
	cellIdx++;
	createExpenseTypeDropdownCell(idx, row.insertCell(cellIdx), "", "expense_type");
	cellIdx++;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "", "number", "expense_amount", 'expense_single_OR', true);
	cellIdx++;
	createInputCellForExpense(idx, row.insertCell(cellIdx), "", "number", "input_tax", 'inputtax', true);
	cellIdx++;

}

// create INPUT element and append to the table cell
function createInputCell(rowIdx, cell, value, type, name, listener, is_required) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = type;
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	if (is_required)
	{
		input.required = true;
	}
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');
	if (listener == 'name')
	{
		input.onchange =  function() { computeWTaxesOfEachItem();classifyAndSetExpense(); };
	}
	if (listener == 'expense')
	{
		input.id = "expense_amount"+(rowIdx-1);
		input.onchange =  function() { computeItemInputTax(rowIdx - 1);computeWTaxesOfEachItem();computeTotalExpenses(); };
	}
	if (listener == 'input_tax')
	{
		input.id = "input_tax"+(rowIdx-1);
		input.onchange =  function() { computeItemInputTax(rowIdx - 1);computeWTaxesOfEachItem();computeTotalExpenses(); };
	}
	else if (listener == 'wtax')
	{
		input.id = "withholding_tax"+(rowIdx-1);
		input.setAttribute('class', 'form-control round-green');
		input.onchange =  function() { computeWTax(); };
	}
	else if (listener == 'inputtax')
	{
		input.id = "input_tax"+(rowIdx-1);
		input.setAttribute('class', 'form-control round-green');
		input.onchange =  function() { computeInputTax(); };
	}
	cell.appendChild(input);                   // append DIV to the table cell
}

// create Charge INPUT element and append to the table cell
function createChargeItemInputCell(rowIdx, cell, value, type, name, listener, is_required) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = type;
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	if (is_required)
	{
		input.required = true;
	}
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');

	if (listener == 'expense')
	{
		input.id = "expense_amount"+(rowIdx-1);
		input.onchange =  function() { computeTotalExpenses(); };
	}

	cell.appendChild(input);                   // append DIV to the table cell
}

// create Charge INPUT element and append to the table cell
function createChargeInputCell(rowIdx, cell, value, type, name, listener, is_required) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = type;
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	if (is_required)
	{
		input.required = true;
	}
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');

	if (listener == 'charge')
	{
		input.id = "charge_amount"+(rowIdx-1);
		input.onchange =  function() { computeTotalCharges(); };
	}

	cell.appendChild(input);                   // append DIV to the table cell
}

// create Charge INPUT element and append to the table cell
function createSettlementInputCell(rowIdx, cell, value, type, name, listener, is_required) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = type;
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	if (is_required)
	{
		input.required = true;
	}
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');

	if (listener == 'settled_amount')
	{
		input.id = "settled_amount"+(rowIdx-1);
		input.onchange =  function() { computeTotalSettlements(); };
	}

	cell.appendChild(input);                   // append DIV to the table cell
}

// create HIDDEN element and append to the table cell
function createHiddenCell(rowIdx, cell, value, name) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = "hidden";
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');

	cell.appendChild(input);                   // append DIV to the table cell
}

// create INPUT element and append to the table cell
function createInputCellForExpense(rowIdx, cell, value, type, name, listener, is_required) {
	var input = document.createElement('input'), // create INPUT element
		txt = document.createTextNode(value); // create text node
	input.appendChild(txt);                    // append text node to the DIV
	input.type = type;
	input.value = value;
	input.setAttribute('step', 'any');
	input.setAttribute('name', name);
	if (is_required)
	{
		input.required = true;
	}
	input.setAttribute('autocomplete', 'off');
	input.setAttribute('class', 'form-control');
	input.setAttribute('className', 'form-control');
	if (listener == 'expense')
	{
		input.id = "expense_amount"+(rowIdx-1);
		input.onchange =  function() { computeRowInputTax(rowIdx-1);computeTotalExpenses(); };
	}
	if (listener == 'expense_single_OR')
	{
		input.id = "expense_amount"+(rowIdx-1);
		input.onchange =  function() { computeTotalExpenses(); computeItemInputTax(rowIdx - 1); };
	}
	else if (listener == 'name_multiple')
	{
		input.id = "name"+(rowIdx-1);
		input.onchange =  function() {
			var vendorSel = document.getElementById("vendor"+(rowIdx-1));
			var user_input = document.getElementById("name"+(rowIdx-1)).value + " " + vendorSel.options[vendorSel.selectedIndex].text;
			user_input = user_input.toLowerCase();
			var user_arr   = user_input.split(" ");
			var result = classifyExpense(user_arr);
			console.log("User Input: " + user_input + " res: "+ result);
			document.getElementById("expense_type"+(rowIdx-1)).value = result;
		};
	}
	else if (listener == 'name_multiple_single_or')
	{
		input.id = "name"+(rowIdx-1);
		input.onchange =  function() {
			var vendorSel = document.getElementById("vendor_id");
			var user_input = document.getElementById("name"+(rowIdx-1)).value + " " + vendorSel.options[vendorSel.selectedIndex].text;
			user_input = user_input.toLowerCase();
			var user_arr   = user_input.split(" ");
			var result = classifyExpense(user_arr);
			console.log("User Input: " + user_input + " res: "+ result);
			document.getElementById("expense_type"+(rowIdx-1)).value = result;
		};
	}
	else if (listener == 'wtax')
	{
		input.id = "withholding_tax"+(rowIdx-1);
		input.setAttribute('class', 'form-control round-green');
	}
	else if (listener == 'inputtax')
	{
		input.id = "input_tax"+(rowIdx-1);
		input.setAttribute('class', 'form-control round-green');
		input.onchange =  function() {
		computeInputTax();
		}
	}
	
	cell.appendChild(input);                   // append DIV to the table cell
}

function createVendorDropdownCell(idx, cell, value, name)
{
	//Create and append select list
	var selectList = document.createElement("select");
	selectList.setAttribute('class', 'form-control');
	selectList.setAttribute('className', 'form-control');
	selectList.setAttribute('name', name);
	selectList.id = "vendor"+(idx-1);
	cell.appendChild(selectList);
	selectList.required = true;
	selectList.onchange =  function() { computeRowInputTax(idx-1);};
	
	// add null option
	var option = document.createElement("option");
	option.text = "[Select Vendor]";
	option.value = "";
	selectList.appendChild(option);

	//Create and append the options
	for (var i = 0; i < vendorKeys.length; i++) {
		var option = document.createElement("option");
		vendorKey = vendorKeys[i];
		option.text = vendors[vendorKey]["name"];
		option.value = vendors[vendorKey]["key"];
		if (value == vendors[vendorKey]["key"])
		{
			option.selected = true;
		}
		selectList.appendChild(option);
	}
	
	selectList.onchange =  function() {
	
		var user_input = document.getElementById("name"+(idx-1)).value + " " + selectList.options[selectList.selectedIndex].text;
		user_input = user_input.toLowerCase();
		var user_arr   = user_input.split(" ");
		var result = classifyExpense(user_arr);
		console.log("User Input: " + user_input + " res: "+ result);
		document.getElementById("expense_type"+(idx-1)).value = result;
	};
}

function createExpenseTypeDropdownCell(idx, cell, value, name)
{
	//Create and append select list
	var selectList = document.createElement("select");
	selectList.setAttribute('class', 'form-control round-green');
	selectList.setAttribute('className', 'form-control round-green');
	
	selectList.setAttribute('name', name);
	selectList.id = "expense_type"+(idx-1);
	cell.appendChild(selectList);
	selectList.required = true;
	selectList.onchange =  function() { computeRowInputTax(idx-1);};

	//add initial drop down options
	// get option details
	var selectOpt = document.createElement( "option" );
	selectOpt.textContent = "[Select Expense Type]";
	selectOpt.value = "";
	// append to list
	selectList.appendChild( selectOpt );
	
	var unclassOpt = document.createElement( "option" );
	unclassOpt.textContent = "Unclassified";
	unclassOpt.value = "-1";
	// append to list
	selectList.appendChild( unclassOpt );
	
	//Create and append the options
	populateDropdown(selectList.id);
}

function createEmployeeDropdownCell(idx, cell, value, name)
{
	//Create and append select list
	var selectList = document.createElement("select");
	selectList.setAttribute('class', 'form-control');
	selectList.setAttribute('className', 'form-control');
	selectList.setAttribute('name', name);
	selectList.id = "charged_to_id"+(idx-1);
	cell.appendChild(selectList);
	selectList.required = true;
	
	// add null option
	var option = document.createElement("option");
	option.text = "[Select Employee]";
	option.value = "";
	selectList.appendChild(option);

	//Create and append the options
	for (var i = 0; i < employeeKeys.length; i++) {
		var option = document.createElement("option");
		employeeKey = employeeKeys[i];
		option.text = employees[employeeKey]["name"];
		option.value = employees[employeeKey]["key"];
		if (value == employees[employeeKey]["key"])
		{
			option.selected = true;
		}
		selectList.appendChild(option);
	}
}

function createDepartmentDropdownCell(idx, cell, value, name)
{
	//Create and append select list
	var selectList = document.createElement("select");
	selectList.setAttribute('class', 'form-control');
	selectList.setAttribute('className', 'form-control');
	selectList.setAttribute('name', name);
	selectList.id = "department"+(idx-1);
	cell.appendChild(selectList);
	selectList.required = true;
	
	// add null option
	var option = document.createElement("option");
	option.text = "[Select Department]";
	option.value = "";
	selectList.appendChild(option);
	
	var departments = [ 
		{idx: "0", name: "Unspecified"},
		{idx: "1", name: "General Admin"},
		{idx: "2", name: "Sales / Distribution"},
		{idx: "3", name: "Marketing"},
		{idx: "4", name: "R&D"},
		{idx: "5", name: "Storage"}
	];
	
	//Create and append the options
	for (var i = 0; i < departments.length; i++) {
		var option = document.createElement("option");
		option.text = departments[i]["name"];
		option.value = departments[i]["idx"];
		if (value == departments[i]["idx"])
		{
			option.selected = true;
		}
		selectList.appendChild(option);
	}
}

// create Select element and append to the table cell
function createUnitsSelectionCell(cell, value, name, is_required, id) {
	//Create array of options to be added
	var array = ["[Select Units]", "pcs", "packs", "g", "kgs", "lbs", "L", "mL", "sacks", "boxes", "m", "km", "yard", "units", "rolls", "KW", "others"];

	//Create and append select list
	var selectList = document.createElement("select");
	selectList.setAttribute('class', 'form-control');
	selectList.setAttribute('className', 'form-control');
	selectList.setAttribute('name', name);
	cell.appendChild(selectList);
	
	if (is_required)
	{
		selectList.required = true;
	}
	if (id != "")
	{
		selectList.id = id;
	}

	//Create and append the options
	for (var i = 0; i < array.length; i++) {
		var option = document.createElement("option");
		option.text = array[i];
		if (i == 0)
		{
			option.value = "";
		}
		else
		{
			option.value = array[i];
		}
		
		if (value == i)
		{
			option.selected = true;
		}
		selectList.appendChild(option);
	}
}
// delete table rows with index greater then 0
function deleteRows() {
	var tbl = document.getElementById('expense_item_table'), // table reference
		lastRow = tbl.rows.length - 1,             // set the last row index
		i;
	// delete rows with index greater then 0
	for (i = lastRow; i > 0; i--) {
		tbl.deleteRow(i);
	}
}

function removeLastRow(table_name)
{
	var tbl = document.getElementById(table_name), // table reference
	lastRow = tbl.rows.length - 2;
	if (lastRow > 1)
	{
		deleteRow(tbl, lastRow);
	}
}

function removeLastDataRow(table_name)
{
	var tbl = document.getElementById(table_name), // table reference
	lastRow = tbl.rows.length - 2; // deduct 2 for header and summary rows
	if (lastRow > 1) // do not remove first data row
	{
		deleteRow(tbl, lastRow);
	}
}

function deleteRow(tbl, idx)
{
	tbl.deleteRow(idx);
}

function goBack() {
    window.history.back();
}

function toggle_hideable_rows(toggleBtnId) {
	$('tr[name$="hideable-row"]').each(function(){
       if ($(this).css('display') == "none") {
           $(this).attr("style", "display: table-row");
		   $('#toggleTableBtn').text("Show Less");
       }
	   else
	   {
		   $(this).attr("style", "display: none");
		   $('#toggleTableBtn').text("Show More");
	   }
    });
}

function toggle_hideable_divs(divname, toggleBtnId) {
	$('div[name$="' + divname + '"]').each(function(){
       if ($(this).css('display') == "none") {
           $(this).attr("style", "display: initial");
		   $('#'+toggleBtnId).text("Hide");
       }
	   else
	   {
		   $(this).attr("style", "display: none");
		   $('#'+toggleBtnId).text("Show");
	   }
    });
}

function setMonthFilterEnable(year_id, month_id)
{
	year = $(year_id).val();
	if (year == "All")
	{
		$(month_id).val("All");
		$(month_id).prop('disabled', true);
	}
	else
	{
		$(month_id).prop('disabled', false);
	}
}
function enableSubmitButton(enableButtons, submitId)
{
	$(submitId).prop('disabled', !enableButtons);
}

String.prototype.replaceAll = function(search, replace) {
    if (replace === undefined) {
        return this.toString();
    }
    return this.split(search).join(replace);
}

var total_rel_funds = 0;
var total_overdue_rel_funds = 0;

function formatAllOverdueFunds()
{
	total_rel_funds = 0;
	total_overdue_rel_funds = 0;
	$('#released_funds_table tr.table-data').each(function() {
		rel_fund_date = $(this).find("td[name$='rel_fund_date']").text();
		rel_fund_status = $(this).find("td[name$='rel_fund_status']").text();
		rel_fund_variance = $(this).find("td[name$='variance']").text();
		var overdue_days = getOverdueInDays(rel_fund_date);

		total_rel_funds++;
		
		if ((overdue_days > 0) && (rel_fund_status != "Closed" && rel_fund_status != "Cancelled"))
		{
			if (parseFloat(rel_fund_variance) > 0.01)
			{
				$(this).attr("style", "background-color: #FF8F8F");
			}
			else
			{
				$(this).attr("style", "background-color: #F5CFCF");
			}
			total_overdue_rel_funds++;
		}
	 });
	 //summarizeOverdueFunds();
}

function formatAllDueChecks()
{
	$('#checks_table tr.table-data').each(function() {
		check_date = $(this).find("td[name$='check_date']").text();
		check_status = $(this).find("td[name$='check_status']").text();
		var days_to_due = getRemainingDaysToDue(check_date);
		
		if ((days_to_due  > 0) && check_status != "Paid" && check_status != "Cancelled")
		{
			$(this).attr("style", "background-color: #FFB1B1");
		}
	 });
}

function summarizeOverdueFunds()
{
	var total_liquidated = total_rel_funds - total_overdue_rel_funds;
	var total_liquidated_in_percentage = 0;
	
	if (total_rel_funds != 0)
	{
		total_liquidated_in_percentage = numberWithCommas(total_liquidated /  total_rel_funds * 100);
	}
	document.getElementById('liquidation_summary').innerHTML = "Liquidation Status: " + total_liquidated + "/" + total_rel_funds +" ( " + total_liquidated_in_percentage  + "%)";
}

function computeSummaryRowsCount(tbl_id, rows_count, with_filter_row)
{
	var rows_to_deduct = 3;
	if(with_filter_row == false)
	{
		rows_to_deduct = rows_to_deduct - 1;
	}
	var count = 0;
	
	if (document.getElementById(tbl_id))
	{
		count = document.getElementById(tbl_id).rows.length - rows_to_deduct;
		if (document.getElementById(rows_count))
		{
			document.getElementById(rows_count).innerHTML = "Total (" + count + ")";
		}
	}	
}

function getOverdueInDays(released_date)
{
	parsed_date = released_date.split(" ");
	parsed_month = parsed_date[0].substring(0, 3);
	parsed_day = parsed_date[1].replace(",","");
	parsed_year = parsed_date[2];

	var relfund_date = new Date(parsed_year, getDateNumFromMonthName(parsed_month)-1, parsed_day);
	var current_date = new Date();
	var timeDiff = current_date.getTime() - relfund_date.getTime();
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

	return diffDays - BUFFER_DAYS;
}

function parseDateFromServer(date_from_server)
{
	parsed_date = date_from_server.split(" ");
	parsed_month = parsed_date[0].substring(0, 3);
	parsed_day = parsed_date[1].replace(",","");
	parsed_year = parsed_date[2];

	var date_to_date_format = new Date(parsed_year, getDateNumFromMonthName(parsed_month)-1, parsed_day);
	return date_to_date_format;
	
}

function getRemainingDaysToDue(check_date)
{
	// ignore empty check dates
	if(check_date == "None")
	{
		return 0;
	}
	parsed_date = check_date.split(" ");
	parsed_month = parsed_date[0].substring(0, 3);
	parsed_day = parsed_date[1].replace(",","");
	parsed_year = parsed_date[2];

	var checkDateWarning = new Date(parsed_year, getDateNumFromMonthName(parsed_month)-1, parsed_day);
	var current_date = new Date();
	var timeDiff = current_date.getTime() - checkDateWarning.getTime();
	var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 

	return diffDays + CHECK_BUFFER_DAYS;
}

function getDateNumFromMonthName(month_name)
{
	if (month_name == "Jan")
	{
		return 1;
	}
	else if (month_name == "Feb")
	{
		return 2;
	}
	else if (month_name == "Mar")
	{
		return 3;
	}
	else if (month_name == "Apr")
	{
		return 4;
	}
	else if (month_name == "May")
	{
		return 5;
	}
	else if (month_name == "Jun")
	{
		return 6;
	}
	else if (month_name == "Jul")
	{
		return 7;
	}
	else if (month_name == "Aug")
	{
		return 8;
	}
	else if (month_name == "Sep")
	{
		return 9;
	}
	else if (month_name == "Oct")
	{
		return 10;
	}
	else if (month_name == "Nov")
	{
		return 11;
	}
	else if (month_name == "Dec")
	{
		return 12;
	}
	
	return -1;
}

function addListenerToFilterInput()
{
	$('input').keypress(function (e) {
		if (e.which == 13) {
			$("#submit_filter_btn").click();
		return false;
		}
	});
}

function addListenerToSearchInput()
{
	$('#search_data').keypress(function (e) {
		if (e.which == 13) {
			$("#search_data_btn").click();
		return false;
		}
	});
}