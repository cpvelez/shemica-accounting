//configs
var default_added_rate = 0.17647059
var default_driver_pay_rate = 0.1;
var default_helper_pay_rate = 0.05;
var default_extra_helper_pay_rate = 0.05;
//constants
var CHARTER_VOYAGE = 0;
var CHARTER_TIME = 1;
var PAY_METHOD_CASH = 0;
var PAY_METHOD_CHECK = 1;

function addCredit(payment_method) {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//compute total funds
	computeTotalFunds();
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		// temporarily disable checking of allocated funds
		/* check if there are allocated funds (total funds != 0)
		if (checkTotalFundsIfValid() == false)
		{
			displayErrorModal("No allocated funds.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;

		}
		*/
		var funds_entry;
		var data;
		if (payment_method == PAY_METHOD_CASH)
		{
			// create funds array from add cash form
			var funds_array = new Array();
			$('#budgets_table tr.table-data').each(function() {
				budget_key = $(this).find("input[name$='budget_key']").val();
				budget_name = $(this).find("input[name$='budget_name']").val();
				fund_amount = $(this).find("input[name$='fund_amount']").val();
				holder = $(this).find("input[name$='holder']").val();
				holder_name = $(this).find("input[name$='holder_name']").val();
				funds_entry = {'budget_key':budget_key, 'budget_name':budget_name, 'amount':fund_amount, 'holder':holder, 'holder_name':holder_name};
				funds_array.push(funds_entry);    
			 });
			 // create cash data
			 data = {
				'credit_date_issued': $( "#credit_date_issued" ).val(),
				'credit_amount': $( "#credit_amount" ).val(),
				'credit_check_no': $( "#check_no" ).val(),
				'credit_ref_no': $( "#ref_no" ).val(),
				'credit_payment_method': $( "#credit_payment_method" ).val(),
				'credit_notes': $( "#notes" ).val(),
				'check_vendor_id': "N/A",
				'check_vendor_name': "N/A",
				'check_date': $( "#credit_date_issued" ).val(),
				'funds': JSON.stringify(funds_array)
				};
		}
		else
		{
			// create funds array from add check form
			var funds_array = new Array();
			$('#budgets_table tr.table-data').each(function() {
				budget_key = $( "#budget_id" ).val();
				budget_name =  budgets[budget_key]['budget_name'],
				fund_amount = $( "#fund_amount" ).val();
				holder = budgets[budget_key]['holder'],
				holder_name = budgets[budget_key]['holder_name'],
				funds_entry = {'budget_key':budget_key, 'budget_name':budget_name, 'amount':fund_amount, 'holder':holder, 'holder_name':holder_name};
				funds_array.push(funds_entry);    
			 });
			 // create check data
			 vendor_key = $( "#vendor_id" ).val();
			 data = {
				'credit_date_issued': $( "#credit_date_issued" ).val(),
				'credit_amount': $( "#credit_amount" ).val(),
				'credit_check_no': $( "#check_no" ).val(),
				'credit_ref_no': $( "#ref_no" ).val(),
				'credit_payment_method': $( "#credit_payment_method" ).val(),
				'credit_notes': $( "#notes" ).val(),
				'check_vendor_id': vendor_key,
				'check_vendor_name': vendors[vendor_key]['name'],
				'check_date': $( "#check_date" ).val(),
				'funds': JSON.stringify(funds_array)
				};
		}
				
		 $.ajax({
			url: '/credit/add',
			type: 'POST',
			data: data,
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					if (payment_method == PAY_METHOD_CASH)
					{
						displaySuccessModal2Buttons("Data has been successfully added.");
					}
					else
					{
						displaySuccessModal("Data has been successfully added.");
					}
					return false;
				}
				else if(data=="duplicate") {
					displayErrorModal("Check already exists.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
		});
	  } else {
			displayErrorModal("Some required fields are missing.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
	  }
	  return false;
}

function editCredit() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//compute total funds
	computeTotalFunds();
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		credit_key = $( "#credit_edit_key" ).val();
		// create funds array
		var funds_array = new Array();
		$('#budgets_table tr.table-data').each(function() {
			fund_key = $(this).find("input[name$='fund_key']").val();
			budget_key = $(this).find("input[name$='budget_key']").val();
			budget_name = $(this).find("input[name$='budget_name']").val();
			fund_amount = $(this).find("input[name$='fund_amount']").val();
			holder = $(this).find("input[name$='holder']").val();
			holder_name = $(this).find("input[name$='holder_name']").val();
			var funds_entry = {'fund_key':fund_key, 'budget_key':budget_key, 'budget_name':budget_name, 'amount':fund_amount, 'holder':holder, 'holder_name':holder_name};
			funds_array.push(funds_entry);    
		 });
		 
		 
		 payment_method =  $( "#credit_payment_method" ).val();
		 if (payment_method == PAY_METHOD_CASH)
		 {
			 // create data for cash
			 data = {
				'credit_date_issued': $( "#credit_date_issued" ).val(),
				'credit_amount': $( "#credit_amount" ).val(),
				'credit_payment_method': $( "#credit_payment_method" ).val(),
				'credit_notes': $( "#notes" ).val(),
				'credit_check_no': "N/A",
				'credit_ref_no': "N/A",
				'check_vendor_id': "N/A",
				'check_vendor_name': "N/A",
				'check_date': $( "#credit_date_issued" ).val(),
				'check_status': $( "#real_check_status" ).val(),
				'funds': JSON.stringify(funds_array)
				};
		}
		else
		{
			// create data for check
			vendor_key = $( "#vendor_id" ).val();
			check_status = $( "#check_status" ).val();
			real_check_status = $( "#real_check_status" ).val();
			if (check_status == 1)
			{
				real_check_status = 3;
			}
			 data = {
				'credit_date_issued': $( "#credit_date_issued" ).val(),
				'credit_amount': $( "#credit_amount" ).val(),
				'credit_check_no': $( "#check_no" ).val(),
				'credit_ref_no': $( "#ref_no" ).val(),
				'credit_payment_method': $( "#credit_payment_method" ).val(),
				'credit_notes': $( "#notes" ).val(),
				'check_vendor_id': vendor_key,
				'check_vendor_name': vendors[vendor_key]['name'],
				'check_date': $( "#check_date" ).val(),
				'check_status': real_check_status,
				'funds': JSON.stringify(funds_array)
				};
		}
		 
		 $.ajax({
			url: '/credit/edit/' + credit_key,
			type: 'POST',
			data: data,
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
	}
	return false;
}

function addBudget() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
    //check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	if(empty == 0) {
		$.ajax({
			url: '/budget/add',
			type: 'POST',
			data: {
				'budget_name': $( "#budget_name" ).val(),
				'budget_amount': $( "#budget_amount" ).val(),
				'budget_holder': $( "#budget_holder" ).val(),
				'budget_holder_name': document.getElementById("budget_holder").options[document.getElementById("budget_holder").selectedIndex ].text,
				'budget_default_department': $( "#default_department" ).val(),
			},
			success: function(data) {
				console.log(data);
					if(data=="ok") {
						displaySuccessModal("Data has been successfully added.");
						return false;
					}else if(data=="duplicate") {
						displayErrorModal("Budget name has a duplicate entry.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
					}
					else {
						displayErrorModal("Data was not added due to some error.");
						//enable submit button
						enableSubmitButton(true, "#submit-form");
						return false;
					}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function editBudget() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
    if(empty == 0) {
		budget_key = $( "#budget_edit_key" ).val();
		$.ajax({
			url: '/budget/edit/'+budget_key,
			type: 'POST',
			data: {
				'budget_name': $( "#budget_name" ).val(),
				'budget_amount': $( "#budget_amount" ).val(),
				'budget_holder': $( "#budget_holder" ).val(),
				'budget_holder_name': document.getElementById("budget_holder").options[document.getElementById("budget_holder").selectedIndex ].text,
				'budget_default_department': $( "#default_department" ).val(),
				'budget_is_active': $( "#budget_is_active" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}else if(data=="duplicate") {
					displayErrorModal("Budget name has a duplicate entry.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
	  });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function transferBudgetFunds() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
    if(empty == 0) {
		budget_key = $( "#budget_transfer_key" ).val();
		amount_to_transfer = $( "#amount_to_transfer" ).val();
		if (amount_to_transfer == 0)
		{
			displayErrorModal("Funds to transfer is invalid.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
		}
		if ( $( "#budget_to" ).val() == budget_key)
		{
			displayErrorModal("Cannot transfer funds to same budget.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
		}
		$.ajax({
			url: '/budget/transfer/'+budget_key,
			type: 'POST',
			data: {
				'budget_from': budget_key,
				'amount_to_transfer': $( "#amount_to_transfer" ).val(),
				'budget_to': $( "#budget_to" ).val(),
				'notes': $( "#notes" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Funds have been successfully transferred.");
					return false;
				}else if(data=="insufficient") {
					displayErrorModal("Insufficient funds");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
				else {
					displayErrorModal("Funds were not transferred due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Funds were not transferred due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
	  });
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function addBudgetCash() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//compute total funds
	computeTotalFunds();
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		// temporarily disable checking of allocated funds
		/* check if there are allocated funds (total funds != 0)
		if (checkTotalFundsIfValid() == false)
		{
			displayErrorModal("No allocated funds.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;

		}
		*/
		var funds_entry;
		var data;

		// create funds array from add cash form
		var funds_array = new Array();
		$('#budgets_table tr.table-data').each(function() {
			budget_key = $(this).find("input[name$='budget_key']").val();
			budget_name = $(this).find("input[name$='budget_name']").val();
			fund_amount = $(this).find("input[name$='fund_amount']").val();
			holder = $(this).find("input[name$='holder']").val();
			holder_name = $(this).find("input[name$='holder_name']").val();
			funds_entry = {'budget_key':budget_key, 'budget_name':budget_name, 'amount':fund_amount, 'holder':holder, 'holder_name':holder_name};
			funds_array.push(funds_entry);    
		 });
		 // create cash data
		 data = {
			'credit_date_issued': $( "#credit_date_issued" ).val(),
			'credit_amount': $( "#credit_amount" ).val(),
			'credit_check_no': $( "#check_no" ).val(),
			'credit_ref_no': $( "#ref_no" ).val(),
			'credit_payment_method': $( "#credit_payment_method" ).val(),
			'credit_notes': $( "#notes" ).val(),
			'check_vendor_id': "N/A",
			'check_vendor_name': "N/A",
			'check_date': $( "#credit_date_issued" ).val(),
			'funds': JSON.stringify(funds_array)
			};
				
		 $.ajax({
			url: '/budget/cash/add/' + budget_key,
			type: 'POST',
			data: data,
			success: function(data) {
				console.log(data);
				if(data=="ok") {

						displaySuccessModal2Buttons("Data has been successfully added.");

					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
		});
	  } else {
			displayErrorModal("Some required fields are missing.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
	  }
	  return false;
}

function addReleasedFund() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

    //check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	if(empty == 0) {
		var source = $( "#released_fund_source" ).val();
		// check if there are allocated funds (total funds != 0)
		if (checkReleasedFundsIfValid() == false)
		{
			displayErrorModal("No released fund amount.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		check_key = $( "#check_id" ).val();
		// determine payment method (check or cash)
		payment_method = 0;
		if (check_key != "N/A")
		{
			payment_method = 1;
		}
		$.ajax({
			url: '/budget/releasedfund/add/' + source,
			type: 'POST',
			data: {
				'released_fund_source': source,
				'released_fund_date_released': $( "#released_fund_date_released" ).val(),
				'released_fund_source_name': $( "#released_fund_source_name" ).val(),
				'released_fund_source_def_department': $( "#department" ).val(),
				'released_fund_amount': $( "#released_fund_amount" ).val(),
				'released_fund_point_person': document.getElementById("released_fund_point_person_name").options[document.getElementById("released_fund_point_person_name").selectedIndex ].value,
				'released_fund_point_person_name': document.getElementById("released_fund_point_person_name").options[document.getElementById("released_fund_point_person_name").selectedIndex ].text,
				'released_fund_liquidator': document.getElementById("released_fund_liquidator_name").options[document.getElementById("released_fund_liquidator_name").selectedIndex ].value,
				'released_fund_liquidator_name': document.getElementById("released_fund_liquidator_name").options[document.getElementById("released_fund_liquidator_name").selectedIndex ].text,
				'released_fund_remarks': $( "#remarks" ).val(),
				'released_fund_notes': $( "#notes" ).val(),
				'payment_method': payment_method,
				'released_fund_check': check_key,
				'released_fund_check_no': checks[check_key]['check_no'],
				'released_fund_check_vendor': checks[check_key]['vendor_id'],
				'release_type': $( "#release_type" ).val()
			},
			success: function(data) {
				console.log(data);
				var decoded = data.split(":");
				if(decoded[0]=="ok") {
					displaySuccessModal2Buttons("Released Fund " + decoded[1] + " has been successfully added.");
					return false;
				}
				else if (decoded[0]=="insufficient")
				{
					displayErrorModal("Insufficient funds.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");
				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");
		return false;
	}
	return false;
}

function editReleasedFund() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

    //check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	if(empty == 0) {
		var released_fund_edit_key = $( "#released_fund_edit_key" ).val();
		var released_amount = parseFloat($( "#released_fund_amount" ).val()) + parseFloat($( "#amount_to_add" ).val());
		var actual_expense = parseFloat($( "#released_fund_actual_expense" ).val());
		var returned_amount = parseFloat($( "#returned_amount" ).val());
		var excess_amount = round(released_amount- actual_expense,2);
		
		console.log("returned_amount:" + returned_amount);
		console.log("released_amount - actual_expense:" + excess_amount);
		
		// check if returned amount is greater than excess amount
		if (returned_amount > excess_amount)
		{
			displayErrorModal("Returned amount exceeds remaining funds.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;

		}
		check_key = $( "#check_id" ).val();
		// determine payment method (check or cash)
		payment_method = 0;
		if (check_key != "N/A")
		{
			payment_method = 1;
		}
		$.ajax({
			url: '/budget/releasedfund/edit/' + released_fund_edit_key,
			type: 'POST',
			data: {
				'released_fund_source': $( "#released_fund_source" ).val(),
				'released_fund_source_name': $( "#released_fund_source_name" ).val(),
				'released_fund_source_def_department': $( "#department" ).val(),
				'released_fund_date_released': $( "#released_fund_date_released" ).val(),
				'released_fund_amount': released_amount,
				'amount_to_add': parseFloat($( "#amount_to_add" ).val()),
				'returned_amount': returned_amount,
				'released_fund_status': $( "#released_fund_status" ).val(),
				'released_fund_point_person': document.getElementById("released_fund_point_person_name").options[document.getElementById("released_fund_point_person_name").selectedIndex ].value,
				'released_fund_point_person_name': document.getElementById("released_fund_point_person_name").options[document.getElementById("released_fund_point_person_name").selectedIndex ].text,
				'released_fund_liquidator': document.getElementById("released_fund_liquidator_name").options[document.getElementById("released_fund_liquidator_name").selectedIndex ].value,
				'released_fund_liquidator_name': document.getElementById("released_fund_liquidator_name").options[document.getElementById("released_fund_liquidator_name").selectedIndex ].text,
				'released_fund_remarks': $( "#remarks" ).val(),
				'released_fund_notes': $( "#notes" ).val(),
				'payment_method': payment_method,
				'released_fund_check': check_key,
				'released_fund_check_no': checks[check_key]['check_no'],
				'released_fund_check_vendor': checks[check_key]['vendor_id']
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
				}
				else if (data=="insufficient")
				{
					displayErrorModal("Insufficient funds.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
				else if (data=="variance")
				{
					displayErrorModal("Excess Amount error. Please check if this is completely liquidated or excess amount is returned.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	}
	return false;
}

function addReleasedFundExpense() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	
	computeTotalExpenses();	
	computeWTax();
	computeInputTax();

	if(empty == 0) {
		var source = $( "#released_fund_id" ).val();
		/*
		skip checking to allow ignored expenses
		if(	checkTotalAmountIfValid() == false)
		{
			displayErrorModal("Expense amount is invalid.");
			return false;
		}
		*/
		// check if expense is Salary & Allowances, Gross Amount should be >= Total Amount
		expense_type = $("#expense_type option:selected").val();
		total_amount = $( "#total_amount" ).val();
		gross_amount = $( "#gross_amount" ).val();
		if (expense_type == 31 && total_amount > gross_amount) // 31 Salary & Allowances
		{
			console.info("expense_type: " + expense_type + " total_amount: " + total_amount + " gross_amount: " + gross_amount );
			displayErrorModal("Please input correct Gross Amount for payroll.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			$('div[name$="payroll_div"]').each(function(){
				if ($(this).css('display') == "none") {
					$(this).attr("style", "display: initial");
					$('#payroll_toggle_btn').text("Hide");
				
				}
			});
			
			return false;
		}
		
		// check if there is a selected vendor when there is OR number_filter
		OR_number = $( "#OR_number" ).val();
		selected_vendor = $( "#vendor_id" ).val();
		if (OR_number != "-" && OR_number != "N/A" && selected_vendor == "N/A")
		{
			displayErrorModal("Please select Vendor.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			
			return false;
		}
				
		if(isUploading)
		{
			displayErrorModal("Uploading of receipt is ongoing...");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		
		// create expense items array
		var expense_items_array = new Array();
		$('#expense_item_table tr.table-data').each(function() {
			item_name = $(this).find("input[name$='item_name']").val();
			//qty = $(this).find("input[name$='qty']").val();
			//units = $(this).find("select[name$='units']").val();
			qty = "1";
			units = "others";
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			withholding_tax = $(this).find("input[name$='withholding_tax']").val();
			input_tax = $(this).find("input[name$='input_tax']").val();
			var expense_item = {'item_name':item_name, 'qty':qty, 'units':units, 'expense_amount':expense_amount, 'input_tax':input_tax, 'withholding_tax':withholding_tax};
			expense_items_array.push(expense_item);
		 });
		 
		 // construct particulars
		particulars = constructParticulars();
		
		// get vendor key
		vendor_key = $("#vendor_id").val();
		
		// determine payment method
		check_no = $( "#check_no" ).val();
		payment_method = 0;
		if (check_no != "N/A" && check_no != "-")
		{
			payment_method  = 1;
		}
		
		$.ajax({
			url: '/budget/releasedfund/expense/add/' + source,
			type: 'POST',
			data: {
				'expense_date': $( "#expense_date" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'check_no': $( "#check_no" ).val(), 
				'OR_number': $( "#OR_number" ).val(), 
				'total_withholding_tax': $( "#total_withholding_tax" ).val(), 
				'total_input_tax': $( "#total_input_tax" ).val(), 
				'transaction_type': $( "#transaction_type option:selected" ).val(),
				'receipt': $("#expense_blob_id").val(),
				'vendor_id': vendor_key,
				'vendor_name': vendors[vendor_key]['name'],
				'vendor_short_name': vendors[vendor_key]['short_name'],
				'vendor_TIN': vendors[vendor_key]['TIN'],
				'vendor_address': vendors[vendor_key]['address'],
				'vendor_is_vat_registered': vendors[vendor_key]['is_registered'] == "True"? 1 : 0,
				'budget_id' : $( "#budget_id" ).val(), 
				'budget_name' : $( "#budget_name" ).val(),
				'expense_type' : $( "#expense_type option:selected" ).val(),
				'released_fund_id': source,
				'released_fund_remarks': $( "#released_fund_remarks" ).val(),
				'expense_items': JSON.stringify(expense_items_array),
				'particulars' : particulars,
				'charged_to_id': $("#charged_to_id").val(),
				'charged_to_name': $( "#charged_to_id option:selected" ).text(),
				'notes': $("#notes").val(),
				'charge_type': $("#charge_type").val(),
				'payment_method': payment_method,
				'check_received_by': $( "#check_received_by" ).val(),
				'department': $("#department").val(),
				'gross_amount': $( "#gross_amount" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal2Buttons("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addMultipleExpenses() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	if(empty == 0) {
		/*
		skip checking to allow ignored expenses
		if(	checkTotalAmountIfValid() == false)
		{
			displayErrorModal("Expense amount is invalid.");
			return false;
		}
		*/
		if(isUploading)
		{
			displayErrorModal("Uploading of receipt is ongoing...");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}		 
		 // create expense items array
		var expenses_array = new Array();
		released_fund_id = $( "#released_fund_id" ).val();
		var error_found = false;
		$('#expense_table tr.table-data').each(function() {
			
			// check if we need to kill the loop
			if(error_found)
			{
				return false;
			}
			item_name = $(this).find("input[name$='item_name']").val();
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			OR_number = $(this).find("input[name$='OR_number']").val();
			expense_type = $(this).find("select[name$='expense_type']").val();
			vendor_key = $(this).find("select[name$='vendor']").val();
		
			if (expense_amount != 0 && item_name != "" && OR_number != "" && expense_type != "" && vendor_key != "")
			{
				expense_date = $( "#expense_date" ).val();
				console.log("vendor key " + vendor_key);
				vendor_name = vendors[vendor_key]['name'];
				console.log("vendor name " + vendor_name);
				vendor_short_name = vendors[vendor_key]['short_name'];
				vendor_TIN = vendors[vendor_key]['TIN'];
				vendor_address = vendors[vendor_key]['address'];
				vendor_is_vat_registered = vendors[vendor_key]['is_registered'] == "True"? 1 : 0;
				withholding_tax = 0;
				input_tax = getInputTax(expense_amount, vendor_is_vat_registered);
				units = "others";
				qty ="1";
				check_no = "-";
				total_withholding_tax = 0;
				total_input_tax = input_tax;
				receipt = ""; //TBD
				budget_id = $( "#budget_id" ).val();
				budget_name = $( "#budget_name" ).val();
				released_fund_id = $( "#released_fund_id" ).val();
				released_fund_remarks = $( "#released_fund_remarks" ).val();
				transaction_type = getTransactionTypeFromExpenseType(expense_type);
				particulars = $(this).find("input[name$='item_name']").val();
				charged_to_id = "N/A";
				charged_to_name = "N/A";
				notes = "";
				charged_to_status = 0;
				department = $("#department").val();

				var expense_entry = {
					'expense_date':expense_date,
					'item_name':item_name,
					'vendor_id':vendor_key,
					'vendor_name': vendor_name,
					'vendor_short_name': vendor_short_name,
					'vendor_TIN': vendor_TIN,
					'vendor_address': vendor_address,
					'vendor_is_vat_registered': vendor_is_vat_registered,
					'qty':qty,
					'units':units,
					'expense_amount': expense_amount,
					'total_amount': expense_amount,
					'input_tax':input_tax,
					'withholding_tax':withholding_tax,
					'check_no':check_no,
					'OR_number': OR_number,
					'total_withholding_tax': total_withholding_tax, 
					'total_input_tax': total_input_tax, 
					'transaction_type': transaction_type,
					'receipt': receipt, 
					'budget_id': budget_id, 
					'budget_name':budget_name, 
					'expense_type': expense_type, 
					'released_fund_id': released_fund_id, 
					'invoice_number': "",
					'particulars': particulars,
					'charged_to_id': charged_to_id,
					'charged_to_name': charged_to_name,
					'notes': notes,
					'charged_to_status': charged_to_status,
					'department': department
				};
				expenses_array.push(expense_entry);
			}
		});
		
		if(error_found)
		{
			displayErrorModal("Please select Vendor.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
		}
		
		// construct submit link
		submit_link = '/budget/releasedfund/add_multiple_expenses/' + released_fund_id;
		$.ajax({
			url: submit_link,
			type: 'POST',
			data: {
				'released_fund_id': released_fund_id,
				'multiple_expenses_array': JSON.stringify(expenses_array)
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addMultipleExpensesSingleOR() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });
	
	computeTotalExpenses();	
	computeWTax();
	computeInputTax();

	if(empty == 0) {
		/*
		skip checking to allow ignored expenses
		if(	checkTotalAmountIfValid() == false)
		{
			displayErrorModal("Expense amount is invalid.");
			return false;
		}
		*/
		
		// check if there is a selected vendor when there is OR number_filter
		OR_number = $( "#OR_number" ).val();
		selected_vendor = $( "#vendor_id" ).val();
		if (OR_number != "-" && OR_number != "N/A" && selected_vendor == "N/A")
		{
			displayErrorModal("Please select Vendor.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			
			return false;
		}
	
		if(isUploading)
		{
			displayErrorModal("Uploading of receipt is ongoing...");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		
		// get vendor key
		vendor_key = $("#vendor_id").val();

		 // create expense items array
		var expenses_array = new Array();
		released_fund_id = $( "#released_fund_id" ).val();
		var error_found = false;
		$('#expense_table tr.table-data').each(function() {
			
			// check if we need to kill the loop
			if(error_found)
			{
				return false;
			}
			item_name = $(this).find("input[name$='item_name']").val();
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			expense_type = $(this).find("select[name$='expense_type']").val();
			input_tax = $(this).find("input[name$='input_tax']").val();
		
			if (expense_amount != 0 && item_name != "" && OR_number != "" && expense_type != "" && vendor_key != "")
			{
				expense_date = $( "#expense_date" ).val();
				withholding_tax = 0;
				units = "others";
				qty ="1";
				check_no = "-";
				total_withholding_tax = 0;
				total_input_tax = input_tax;
				receipt = $("#expense_blob_id").val();
				budget_id = $( "#budget_id" ).val();
				budget_name = $( "#budget_name" ).val();
				released_fund_id = $( "#released_fund_id" ).val();
				released_fund_remarks = $( "#released_fund_remarks" ).val();
				transaction_type = getTransactionTypeFromExpenseType(expense_type);
				particulars = $(this).find("input[name$='item_name']").val();
				charged_to_id = "N/A";
				charged_to_name = "N/A";
				notes = "";
				charged_to_status = 0;
				department = $("#department").val();

				var expense_entry = {
					'expense_date':expense_date,
					'item_name':item_name,
					'vendor_id': vendor_key,
					'vendor_name': vendors[vendor_key]['name'],
					'vendor_short_name': vendors[vendor_key]['short_name'],
					'vendor_TIN': vendors[vendor_key]['TIN'],
					'vendor_address': vendors[vendor_key]['address'],
					'vendor_is_vat_registered': vendors[vendor_key]['is_registered'] == "True"? 1 : 0,
					'qty':qty,
					'units':units,
					'expense_amount': expense_amount,
					'total_amount': expense_amount,
					'input_tax':input_tax,
					'withholding_tax':withholding_tax,
					'check_no':check_no,
					'OR_number': OR_number,
					'total_withholding_tax': total_withholding_tax, 
					'total_input_tax': total_input_tax, 
					'transaction_type': transaction_type,
					'receipt': receipt, 
					'budget_id': budget_id, 
					'budget_name':budget_name, 
					'expense_type': expense_type, 
					'released_fund_id': released_fund_id, 
					'invoice_number': "",
					'particulars': particulars,
					'charged_to_id': charged_to_id,
					'charged_to_name': charged_to_name,
					'notes': notes,
					'charged_to_status': charged_to_status,
					'department': department
				};
				expenses_array.push(expense_entry);
			}
		});
		
		if(error_found)
		{
			displayErrorModal("Please select Vendor.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			return false;
		}
		
		// construct submit link
		submit_link = '/budget/releasedfund/add_multiple_expenses_single_or/' + released_fund_id;
		$.ajax({
			url: submit_link,
			type: 'POST',
			data: {
				'released_fund_id': released_fund_id,
				'multiple_expenses_array': JSON.stringify(expenses_array)
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addCharge() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });

	if(empty == 0) {
		
		OR_number = $( "#OR_number" ).val();
		selected_vendor = $( "#vendor_id" ).val();
		
		// create expense items array
		var expense_items_array = new Array();
		$('#charge_item_table tr.table-data').each(function() {
			item_name = $(this).find("input[name$='item_name']").val();
			qty = 1;
			units = "others";
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			withholding_tax = 0;
			input_tax = 0;
			var expense_item = {'item_name':item_name, 'qty':qty, 'units':units, 'expense_amount':expense_amount, 'input_tax':input_tax, 'withholding_tax':withholding_tax};
			expense_items_array.push(expense_item);
		 });
		 
		 // construct particulars
		particulars = constructParticulars();
		
		// get employee id
		employee_id = $("#charged_to_id").val()
		
		$.ajax({
			url: '/cashadvance/add/' + employee_id,
			type: 'POST',
			data: {
				'expense_date': $( "#charge_date" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'check_no': "N/A", 
				'OR_number': "-", 
				'total_withholding_tax': 0, 
				'total_input_tax': 0, 
				'transaction_type': 1,
				'receipt': "",
				'vendor_id': "N/A",
				'vendor_name': "N/A",
				'vendor_short_name': "N/A",
				'vendor_TIN': "N/A",
				'vendor_address': "N/A",
				'vendor_is_vat_registered': 0,
				'budget_id' : "N/A", 
				'budget_name' : "N/A",
				'expense_type' : 6,
				'released_fund_id': "N/A",
				'released_fund_remarks': "N/A",
				'expense_items': JSON.stringify(expense_items_array),
				//'waybill_id' : "", 
				//'waybill_number' : "",
				'invoice_number' : "",
				'particulars' : particulars,
				'charged_to_id': $("#charged_to_id").val(),
				'charged_to_name': $( "#charged_to_id option:selected" ).text(),
				'notes': $("#notes").val(),
				'status': 0,
				'payment_method': 0,
				'check_received_by': "N/A",
				'department': $("#department").val(),
				'charge_type': $("#charge_type").val(),
				'is_charge_billed': $("#is_charge_billed").val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addCACharge() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });

	if(empty == 0) {
		
		OR_number = $( "#OR_number" ).val();
		selected_vendor = $( "#vendor_id" ).val();
		
		// create expense items array
		var expense_items_array = new Array();
		$('#charge_item_table tr.table-data').each(function() {
			item_name = $(this).find("input[name$='item_name']").val();
			qty = 1;
			units = "others";
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			withholding_tax = 0;
			input_tax = 0;
			var expense_item = {'item_name':item_name, 'qty':qty, 'units':units, 'expense_amount':expense_amount, 'input_tax':input_tax, 'withholding_tax':withholding_tax};
			expense_items_array.push(expense_item);
		 });
		 
		 // construct particulars
		particulars = constructParticulars();
		
		// get released fund id
		released_fund_id = $("#released_fund_id").val()
		
		$.ajax({
			url: '/budget/releasedfund/expense/addCACharge/' + released_fund_id,
			type: 'POST',
			data: {
				'expense_date': $( "#charge_date" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'check_no': "N/A", 
				'OR_number': "-", 
				'total_withholding_tax': 0, 
				'total_input_tax': 0, 
				'transaction_type': 1,
				'receipt': "",
				'vendor_id': "N/A",
				'vendor_name': "N/A",
				'vendor_short_name': "N/A",
				'vendor_TIN': "N/A",
				'vendor_address': "N/A",
				'vendor_is_vat_registered': 0,
				'budget_id' : $( "#budget_id" ).val(), 
				'budget_name' : $( "#budget_name" ).val(),
				'expense_type' : 3,
				'released_fund_id': $( "#released_fund_id" ).val(),
				'released_fund_remarks': $( "#released_fund_remarks" ).val(),
				'expense_items': JSON.stringify(expense_items_array),
				'particulars' : particulars,
				'charged_to_id': $("#charged_to_id").val(),
				'charged_to_name': $( "#charged_to_id option:selected" ).text(),
				'notes': $("#notes").val(),
				'status': 0,
				'payment_method': 0,
				'check_received_by': "N/A",
				'department': $("#department").val(),
				'charge_type': $("#charge_type").val(),
				'is_charge_billed': $("#is_charge_billed").val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addMultipleCharges() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });

	if(empty == 0) {
		
		// create charges array
		var charges_array = new Array();
		$('#charges_table tr.table-data').each(function() {
			
			expense_amount = $(this).find("input[name$='charge_amount']").val();
			charged_to_id = $(this).find("select[name$='charged_to_id']").val();
			department =  $(this).find("select[name$='department']").val();
			
			if (expense_amount != 0 && charged_to_id != "" && department != "")
			{
				qty = 1;
				units = "others";
				vendor_key = "N/A";
				expense_date = $( "#charge_date" ).val();
				vendor_name = "N/A";
				vendor_short_name = "N/A";
				vendor_TIN = "N/A";
				vendor_address = "N/A";
				vendor_is_vat_registered = 0;
				check_no = "N/A";
				OR_number = "N/A";
				total_withholding_tax = 0;
				total_input_tax = 0;
				receipt = "";
				budget_id = "N/A";
				budget_name = "N/A";
				released_fund_id = "N/A";
				released_fund_remarks = "N/A";
				expense_type = 6;
				transaction_type = 1;
				item_name = $("#item_name").val();
				particulars = $("#item_name").val();
				charged_to_name = $(this).find("select[name$='charged_to_id'] option:selected").text();
				notes = $( "#notes" ).val();
				charge_type = $("#charge_type").val();
				is_charge_billed = 1;

				var charge = {
					'expense_date':expense_date,
					'item_name':item_name,
					'vendor_id':vendor_key,
					'vendor_name': vendor_name,
					'vendor_short_name': vendor_short_name,
					'vendor_TIN': vendor_TIN,
					'vendor_address': vendor_address,
					'vendor_is_vat_registered': vendor_is_vat_registered,
					'qty':qty,
					'units':units,
					'expense_amount': expense_amount,
					'total_amount': expense_amount,
					'input_tax':total_input_tax,
					'withholding_tax':total_withholding_tax,
					'check_no':check_no,
					'OR_number': OR_number,
					'total_withholding_tax': total_withholding_tax, 
					'total_input_tax': total_input_tax, 
					'transaction_type': transaction_type,
					'receipt': receipt, 
					'budget_id': budget_id, 
					'budget_name':budget_name, 
					'expense_type': expense_type, 
					'released_fund_id': released_fund_id,
					'released_fund_remarks': released_fund_remarks,
					'particulars': particulars,
					'charged_to_id': charged_to_id,
					'charged_to_name': charged_to_name,
					'notes': notes,
					'department': department,
					'charge_type': charge_type,
					'is_charge_billed': is_charge_billed
				};
				charges_array.push(charge);
			}
		});
		
		$.ajax({
			url: '/cashadvance/add_charges/',
			type: 'POST',
			data: {
				'charges_array': JSON.stringify(charges_array)
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	  
	return false;
}

function addSettlement() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });

	if(empty == 0) {
		
		employee_id = $( "#credit_to_id" ).val();
		$.ajax({
			url: '/cashadvance/add_settlement/' + employee_id,
			type: 'POST',
			data: {
				'settlement_date': $( "#settlement_date" ).val(),
				'credit_to_id': $( "#credit_to_id" ).val(),
				'credit_to_name': $( "#credit_to_id option:selected" ).text(),
				'department': $( "#department" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'remarks': $( "#remarks" ).val(),
				'notes': $( "#notes" ).val(),
				'settlement_type': $("#settlement_type").val(),
				'settled_charge_type': $("#settled_charge_type").val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }
	return false;
}

function addMultipleSettlements() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");
	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    });

	if(empty == 0) {
		
		// create settlements array
		var settlements_array = new Array();
		$('#settlements_table tr.table-data').each(function() {
			
			credit_to_id = $(this).find("select[name$='credit_to_id']").val();
			department = $(this).find("select[name$='department']").val();
			settled_amount = $(this).find("input[name$='settled_amount']").val();
			
			if (credit_to_id != "" && department != "" && settled_amount != 0)
			{
				settlement_date = $( "#settlement_date" ).val();
				credit_to_name = $(this).find("select[name$='credit_to_id'] option:selected").text();
				notes = $( "#notes" ).val();
				settlement_type = $("#settlement_type").val();
				settled_charge_type = $("#settled_charge_type").val();
				remarks = $("#remarks").val();

				var settlement = {
					'credit_to_id': credit_to_id,
					'department': department,
					'total_amount': settled_amount,
					'remarks': remarks,
					'settlement_date': settlement_date,
					'credit_to_name': credit_to_name,
					'notes': notes,
					'settlement_type': settlement_type,
					'settled_charge_type': settled_charge_type
				};
				settlements_array.push(settlement);
			}
		});

		$.ajax({
			url: '/cashadvance/add_settlements/',
			type: 'POST',
			data: {
				'settlements_array': JSON.stringify(settlements_array)
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully added.");
					return false;
				}
				else {
					displayErrorModal("Data was not added due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");
					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not added due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	  } else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	  }

	return false;
}

function editReleasedFundExpense() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	computeTotalExpenses();
	computeWTax();
	computeInputTax();	
	if(empty == 0) {
		/*
		skip checking to allow ignored expenses
		if(	checkTotalAmountIfValid() == false)
		{
			displayErrorModal("Expense amount is invalid.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		*/
		// check if expense is Salary & Allowances, Gross Amount should be >= Total Amount
		expense_type = $("#expense_type option:selected").val();
		total_amount = $( "#total_amount" ).val();
		gross_amount = $( "#gross_amount" ).val();
		if (expense_type == 31 && total_amount > gross_amount) // 31 Salary & Allowances
		{
			console.info("expense_type: " + expense_type + " total_amount: " + total_amount + " gross_amount: " + gross_amount );
			displayErrorModal("Please input correct Gross Amount for payroll.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			$('div[name$="payroll_div"]').each(function(){
				if ($(this).css('display') == "none") {
					$(this).attr("style", "display: initial");
					$('#payroll_toggle_btn').text("Hide");
				
				}
			});
			
			return false;
		}
		
		// check if there is a selected vendor when there is OR number_filter
		OR_number = $( "#OR_number" ).val();
		selected_vendor = $( "#vendor_id" ).val();
		if (OR_number != "-" && OR_number != "N/A" && selected_vendor == "N/A")
		{
			displayErrorModal("Please select Vendor.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");
			
			return false;
		}
		
		if(isUploading)
		{
			displayErrorModal("Uploading of receipt is ongoing...");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		// create expense items array
		var expense_items_array = new Array();
		$('#expense_item_table tr.table-data').each(function() {
			expense_item_key = $(this).find("input[name$='expense_item_key']").val() == undefined ? "" : $(this).find("input[name$='expense_item_key']").val();
			item_name = $(this).find("input[name$='item_name']").val();
			//qty = $(this).find("input[name$='qty']").val();
			//units = $(this).find("select[name$='units']").val();
			qty = "1";
			units = "others";
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			withholding_tax = $(this).find("input[name$='withholding_tax']").val();
			input_tax = $(this).find("input[name$='input_tax']").val();
			var expense_item = {'expense_item_key':expense_item_key, 'item_name':item_name, 'qty':qty, 'units':units, 'expense_amount':expense_amount, 'input_tax':input_tax, 'withholding_tax':withholding_tax};
			expense_items_array.push(expense_item);    
		 });
		var expense_key = $( "#expense_edit_key" ).val();
		// construct particulars
		particulars = constructParticulars();
		// get vendor key
		vendor_key = document.getElementById("vendor_id").options[document.getElementById("vendor_id").selectedIndex ].value
		// determine payment method
		check_no = $( "#check_no" ).val();
		payment_method = 0;
		if (check_no != "N/A" && check_no != "-")
		{
			payment_method  = 1;
		}
		$.ajax({
			url: '/budget/releasedfund/expense/edit/'+expense_key,
			type: 'POST',
			data: {
				'expense_date': $( "#expense_date" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'check_no': check_no, 
				'OR_number': $( "#OR_number" ).val(), 
				'total_withholding_tax': $( "#total_withholding_tax" ).val(), 
				'total_input_tax': $( "#total_input_tax" ).val(), 
				'transaction_type': $( "#transaction_type option:selected" ).val(),
				'receipt': $("#expense_blob_id").val(),
				'vendor_id': vendor_key,
				'vendor_name': vendors[vendor_key]['name'],
				'vendor_short_name': vendors[vendor_key]['short_name'],
				'vendor_TIN': vendors[vendor_key]['TIN'],
				'vendor_address': vendors[vendor_key]['address'],
				'vendor_is_vat_registered': vendors[vendor_key]['is_registered'] == "True"? 1 : 0,
				'expense_type' : $("#expense_type option:selected").val(),
				'expense_items': JSON.stringify(expense_items_array),
				'particulars' : particulars,
				'charged_to_id': $("#charged_to_id").val(),
				'charged_to_name': $("#charged_to_id option:selected").text(),
				'paid_amount': $( "#paid_amount" ).val(),
				'notes': $("#notes").val(),
				'charge_type': $("#charge_type").val(),
				'check_received_by': $( "#check_received_by" ).val(),
				'department': $("#department").val(),
				'gross_amount': $( "#gross_amount" ).val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	}
	return false;
}

function editCharge() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	computeTotalExpenses();
	if(empty == 0) {
		/*
		skip checking to allow ignored expenses
		if(	checkTotalAmountIfValid() == false)
		{
			displayErrorModal("Expense amount is invalid.");
			//enable submit button
			enableSubmitButton(true, "#submit-form");

			return false;
		}
		*/
		
		// create expense items array
		var expense_items_array = new Array();
		$('#charge_item_table tr.table-data').each(function() {
			charge_item_key = $(this).find("input[name$='charge_item_key']").val() == undefined ? "" : $(this).find("input[name$='charge_item_key']").val();
			item_name = $(this).find("input[name$='item_name']").val();
			qty = 1;
			units = "others";
			expense_amount = $(this).find("input[name$='expense_amount']").val();
			withholding_tax = "0";
			input_tax = "0";
			var expense_item = {'charge_item_key':charge_item_key, 'item_name':item_name, 'qty':qty, 'units':units, 'expense_amount':expense_amount, 'input_tax':input_tax, 'withholding_tax':withholding_tax};
			expense_items_array.push(expense_item);    
		 });
		var charge_key = $( "#charge_edit_key" ).val();
		// construct particulars
		particulars = constructParticulars();
		
		$.ajax({
			url: '/cashadvance/edit/'+charge_key,
			type: 'POST',
			data: {
				'charge_date': $( "#charge_date" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'charge_items': JSON.stringify(expense_items_array),
				'particulars' : particulars,
				'charged_to_id': $("#charged_to_id").val(),
				'charged_to_name': $( "#charged_to_id option:selected" ).text(),
				'notes': $("#notes").val(),
				'department': $("#department").val(),
				'charge_type': $("#charge_type").val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	}
	return false;
}

function editSettlement() {
	//disable submit button
	enableSubmitButton(false, "#submit-form");

	//check required fields
	var empty = 0;
    $('input[required]').each(function(){
       if (this.value == "") {
           empty++;
       } 
    })
	
	if(empty == 0) {

		// get key
		var settlement_key = $( "#settlement_edit_key" ).val();
		
		$.ajax({
			url: '/cashadvance/edit_settlement/'+settlement_key,
			type: 'POST',
			data: {
				'settlement_date': $( "#settlement_date" ).val(),
				'credit_to_id': $( "#credit_to_id" ).val(),
				'credit_to_name': $( "#credit_to_id option:selected" ).text(),
				'department': $( "#department" ).val(),
				'total_amount': $( "#total_amount" ).val(),
				'remarks': $( "#remarks" ).val(),
				'notes': $( "#notes" ).val(),
				'settlement_type': $("#settlement_type").val(),
				'settled_charge_type': $("#settled_charge_type").val()
			},
			success: function(data) {
				console.log(data);
				if(data=="ok") {
					displaySuccessModal("Data has been successfully updated.");
					return false;
				}
				else {
					displayErrorModal("Data was not updated due to some error.");
					//enable submit button
					enableSubmitButton(true, "#submit-form");

					return false;
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log(errorThrown);
				console.log(textStatus);
				displayErrorModal("Data was not updated due to some error.");
				//enable submit button
				enableSubmitButton(true, "#submit-form");

				return false;
            }
		});
	} else {
		displayErrorModal("Some required fields are missing.");
		//enable submit button
		enableSubmitButton(true, "#submit-form");

		return false;
	}
	return false;
}

function creditFilter() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/credit/filter',
		type: 'GET',
		data: {
		  'credit_year_filter' : $( "#credit_year_filter" ).val(),
		  'credit_month_filter' : $( "#credit_month_filter").val()
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function fundsFilter() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/fund/filter',
		type: 'GET',
		data: {
		  'funds_year_filter' : $( "#funds_year_filter" ).val(),
		  'funds_month_filter' : $( "#funds_month_filter").val(),
		  'funds_budget_filter' : $( "#funds_budget_filter").val(),
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function expenseFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/expense/filter',
		type: 'GET',
		data: {
		  'expense_year_filter' : $( "#expense_year_filter" ).val(),
		  'expense_month_filter' : $( "#expense_month_filter").val(),
		  'expense_type_filter' : $( "#expense_type_filter").val(),
		  'expense_budget_id_filter' : $( "#expense_budget_id_filter").val(),
		  'expense_number_filter' : $( "#expense_number_filter").val(),
		  'expense_status_filter' : $( "#expense_status_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}

function checkFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/check/filter',
		type: 'GET',
		data: {
		  'check_year_filter' : $( "#check_year_filter" ).val(),
		  'check_month_filter' : $( "#check_month_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}

function cashFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/cash/filter',
		type: 'GET',
		data: {
		  'cash_year_filter' : $( "#cash_year_filter" ).val(),
		  'cash_month_filter' : $( "#cash_month_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}

function cashadvanceFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/cashadvance/filter',
		type: 'GET',
		data: {
		  'expense_year_filter' : $( "#expense_year_filter" ).val(),
		  'expense_month_filter' : $( "#expense_month_filter").val(),
		  'expense_charged_to_filter' : $( "#expense_charged_to_filter").val(),
		  'expense_number_filter' : $( "#expense_number_filter").val(),
		  'expense_status_filter' : $( "#expense_status_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		}
	});
}

function releaseFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/releasedfund/filter',
		type: 'GET',
		data: {
		  'released_funds_year_filter' : $( "#release_year_filter" ).val(),
		  'released_funds_month_filter' : $( "#release_month_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}

function budgetYearFilter(budget_key) {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/budget/filter/' + budget_key,
		type: 'GET',
		data: {
		  'budget_details_year_filter' : $( "#year_filter" ).val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}


function viewReleasesOfDate() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/releases/date',
		type: 'GET',
		data: {
		  'currentIdx' : $( "#release_date_range" ).val(),
		  'release_start_date_year' : $( "#release_start_date_year" ).val(),
		  'release_start_date_month' : $( "#release_start_date_month" ).val(),
		  'release_start_date_day' : $( "#release_start_date_day" ).val(),
		  'release_end_date_year' : $( "#release_end_date_year" ).val(),
		  'release_end_date_month' : $( "#release_end_date_month" ).val(),
		  'release_end_date_day' : $( "#release_end_date_day" ).val()
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function viewChargesOfDate() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/charges_history/date',
		type: 'GET',
		data: {
		  'currentIdx' : $( "#charge_date_range" ).val(),
		  'charge_start_date_year' : $( "#charge_start_date_year" ).val(),
		  'charge_start_date_month' : $( "#charge_start_date_month" ).val(),
		  'charge_start_date_day' : $( "#charge_start_date_day" ).val(),
		  'charge_end_date_year' : $( "#charge_end_date_year" ).val(),
		  'charge_end_date_month' : $( "#charge_end_date_month" ).val(),
		  'charge_end_date_day' : $( "#charge_end_date_day" ).val()
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function viewSettlementsOfDate() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/settlements_history/date',
		type: 'GET',
		data: {
		  'currentIdx' : $( "#settlement_date_range" ).val(),
		  'settlement_start_date_year' : $( "#settlement_start_date_year" ).val(),
		  'settlement_start_date_month' : $( "#settlement_start_date_month" ).val(),
		  'settlement_start_date_day' : $( "#settlement_start_date_day" ).val(),
		  'settlement_end_date_year' : $( "#settlement_end_date_year" ).val(),
		  'settlement_end_date_month' : $( "#settlement_end_date_month" ).val(),
		  'settlement_end_date_day' : $( "#settlement_end_date_day" ).val()
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function viewExpensesOfDate() {
	$( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/expenses_history/date',
		type: 'GET',
		data: {
		  'currentIdx' : $( "#expense_date_range" ).val(),
		  'expense_start_date_year' : $( "#expense_start_date_year" ).val(),
		  'expense_start_date_month' : $( "#expense_start_date_month" ).val(),
		  'expense_start_date_day' : $( "#expense_start_date_day" ).val(),
		  'expense_end_date_year' : $( "#expense_end_date_year" ).val(),
		  'expense_end_date_month' : $( "#expense_end_date_month" ).val(),
		  'expense_end_date_day' : $( "#expense_end_date_day" ).val()
		},
		success: function(data) {
		  $('#page-wrapper').html(data);
		  $( "#loader" ).attr("style", "visibility: hidden");
		}
	});
}

function expenseReport() {

	// get saved year and month
	if (date_year != '')
	{
		year = date_year;
		month = date_month;
	}
	else
	{
		year = $( "#expense_year_filter" ).val();
		month = $( "#expense_month_filter").val();
	}
	
	location.replace('/expense/report?expense_year_filter='+year+'&expense_month_filter='+month);
}

function expenseReportThisYear() {
	
	// set current year
	currentDate = new Date();
	currentYear = currentDate.getFullYear();
	month = "All";
	location.replace('/expense/report?expense_year_filter='+currentYear+'&expense_month_filter='+month);
}

function expenseReportPreviousYear() {
	
	// set current year
	currentDate = new Date();
	currentYear = currentDate.getFullYear() - 1;
	month = "All";
	location.replace('/expense/report?expense_year_filter='+currentYear+'&expense_month_filter='+month);
}

function expenseReportFilter() {
  $( "#loader" ).attr("style", "visibility: visible");
	$.ajax({
		url: '/expense/report/filter',
		type: 'GET',
		data: {
		  'expense_year_filter' : $( "#expense_year_filter" ).val(),
		  'expense_month_filter' : $( "#expense_month_filter").val()
		},
		success: function(data) {
			$( "#loader" ).attr("style", "visibility: hidden");
			$('#page-wrapper').html(data);
		  
		},
		async: true,
		timeout: 120000, //2 minutes
	});
}

function filterExpenseReportByType(expenseType) {
  console.log("Filter " + expenseType);
  $("#selectcolumn5").val(expenseType);
  $("#selectcolumn5").val(expenseType).change();
  $("#expenses_table input:first").focus();
}

function openModal() {
	$('#addBudgetReleasedFundModal').modal('show');
}

function computeTotalFunds()
{
	totalFunds = 0;
	$( "input[name$='fund_amount']" ).each(function(){
		totalFunds += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#credit_amount').val(totalFunds);
}

function checkTotalFundsIfValid()
{
	totalFunds = $('#credit_amount').val();
	if (totalFunds == 0)
	{
		return false;
	}
	return true;
}

function computeTotalExpenses()
{
	totalExpenses = 0;
	$( "input[name$='expense_amount']" ).each(function(){
		totalExpenses += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#total_amount').val(totalExpenses);
}

function computeTotalCharges()
{
	totalCharges = 0;
	$( "input[name$='charge_amount']" ).each(function(){
		totalCharges += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#total_charged_amount').val(totalCharges);
}

function computeTotalSettlements()
{
	totalSettlements = 0;
	$( "input[name$='settled_amount']" ).each(function(){
		totalSettlements += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#total_settled_amount').val(totalSettlements);
}

function computeWTax()
{
	totalWTax = 0;
	$( "input[name$='withholding_tax']" ).each(function(){
		totalWTax += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#total_withholding_tax').val(totalWTax);
}

function computeInputTax()
{
	totalInputTax = 0;
	$( "input[name$='input_tax']" ).each(function(){
		totalInputTax += parseFloat((this.value == "") ? 0 : this.value);
	});
	$('#total_input_tax').val(totalInputTax);
}

function checkTotalAmountIfValid()
{
	totalAmount = $('#total_amount').val();
	if (totalAmount == 0)
	{
		return false;
	}
	return true;
}

function checkReleasedFundsIfValid()
{
	totalFunds = $('#released_fund_amount').val();
	if (totalFunds == 0)
	{
		return false;
	}
	return true;
}

var isUploading = false;

function uploadImage() {
	if(isUploading)
	{
		displayErrorModal("Uploading of receipt is ongoing...");
		return false;
	}
	var imagefile = document.getElementsByName("upload_file")[0].files;
    var imageData = new FormData()
    imageData.append('image', imagefile[0])
	$( "#loader" ).attr("style", "visibility: visible");
	isUploading = true;
	$.ajax({
        type: "POST",
        url: '/expense_upload/',
        data: imageData,
		cache: false,
        contentType: false,
        processData: false,
        success: function(key) {
          if(key == "error") {
			$( "#receipt_image" ).val("");
          } else {
          	$("#expense_blob_id").val(key);
			$("#receipt-status").text("(Receipt attached)");
          }
        },
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log(errorThrown);
			console.log(textStatus);
			displayErrorModal("Error in uploading receipt.");
			$( "#receipt_image" ).val("");
			return false;
		},
		complete: function (XMLHttpRequest, textStatus) {
			console.log(textStatus);
			$( "#loader" ).attr("style", "visibility: hidden");
			isUploading = false
			return false;
		}
  });	
}

function updateCheckNo(paymentMethodSelect, checkNo)
{
	if ($("#"+paymentMethodSelect).val() == "1")
	{
		$("#"+checkNo).val("");
		$("#"+checkNo).prop('disabled', false);
	}
	else
	{
		$("#"+checkNo).val("N/A");
		$("#"+checkNo).prop('disabled', true);
	}
}

function constructParticulars()
{
	particulars = "";
	i = 0;
	$( "input[name$='item_name']" ).each(function(){
		if ( i == 0)
		{
			particulars += this.value;
		}
		else
		{
			particulars += ", " + this.value;
		}
		i++;
	});
	return particulars;
}

function setAddedRate()
{
	if ($('input[name="apply_additional_rate"]:checked').val() == true)
	{
		$("#added_rate").val(default_added_rate);
		$("#added_rate").prop('disabled', false);
	}
	else
	{
		$("#added_rate").val(0.0);
		$("#added_rate").prop('disabled', true);
	}
}

function detectIfORExists()
{
	vendor = document.getElementById("vendor_id").value;
	if (vendor == "N/A")
	{
		document.getElementById("OR_number").value = "-";
	}
}

// constructor for wtax keywords
function addKeyList(keyCode, keyPercent, keyWords)
{
	this.keyCode  = keyCode;
	this.keyPercent = keyPercent;
    this.keyWords = keyWords;
}

var wtax_key_list = [
    new addKeyList( 0, 0.02, "omniserve rcm safeforce omniserv manpower" ),
	new addKeyList( 1, 0.05, "rentals rent rental" ),
	new addKeyList( 2, 0.15, "professional prof accrual retainer consultation lawyer consultant consult accountant" )
];

function computeItemWithholdingTax(idx)
{
	var wtax = 0.0;
	var is_wtax_vatable = false;
	var wtax_percentage = 0;
	// get expense keywords
	var keywords = getExpenseKeywords();
	keywords = keywords.toLowerCase();
	var keywords_arr = keywords.split(" ");
	var classificationArray = [];
	
	// check if expense is witholding tax vatable based on keywords match	
    // loop through user input and search for a match
    for ( var j = 0; j < wtax_key_list.length; j++ )
    {
        for( var i = 0; i < keywords_arr.length; i++ )
        {
			keywords_list =  wtax_key_list[j].keyWords.split(" ");
			for (var k = 0; k < keywords_list.length; k++)
			{
				if (keywords_arr[i].toLowerCase().search(keywords_list[k]) == 0 )
				{
					//add classification in an array
					classificationArray.push(wtax_key_list[j].keyCode);
				}
			}
        }
    }

	// get result
	var frequency = {};  // array of frequency.
	var max = 0;  // holds the max frequency.
	var result = -1;   // holds the max frequency element.
	for(var v in classificationArray) {
		frequency[classificationArray[v]]=(frequency[classificationArray[v]] || 0)+1; // increment frequency.
		if(frequency[classificationArray[v]] > max) { // is this frequency > max so far ?
				max = frequency[classificationArray[v]];  // update max.
				result = classificationArray[v];          // update result.
		}
	}
	
	// get percentage if wtax taxable
	if (result != -1)
	{
		wtax_percentage = parseFloat(wtax_key_list[result].keyPercent);
	}
	
	// if no, return 0;
	if (is_wtax_vatable)
	{
		wtax = 0;
	}
	else
	{
		// if yes, compute and return wtax_percentage
		amount = $("#expense_amount"+idx).val();
		if (amount)
		{
			if ( isVendorVatRegistered())
			{
				wtax = parseFloat(amount) / 1.12 * wtax_percentage;
			}
			else
			{
				wtax = parseFloat(amount) * wtax_percentage;
			}
		}
	}

	$("#withholding_tax"+idx).val(round(wtax, 2));
	computeWTax();
}

function getExpenseKeywords()
{
	keywords = "";
	// get vendor
	vendor = "";
	vendorSelect = document.getElementById('vendor_id');
	if (vendorSelect)
	{
		vendor = vendorSelect.options[ vendorSelect.selectedIndex ].text;
	}
	
	// get particulars
	particulars = "";
	i = 0;
	$( "input[name$='item_name']" ).each(function(){
		if ( i == 0)
		{
			particulars += this.value;
		}
		else
		{
			particulars += " " + this.value;
		}
		i++;
	});
	
	keywords = vendor + " " + particulars;
	
	return keywords;
}



function computeItemInputTax(idx)
{
	var input_tax = 0.0;
	if ( isVendorVatRegistered())
	{
		amount = $("#expense_amount"+idx).val();
		if (amount)
		{
			input_tax = parseFloat(amount) / 1.12 * 0.12;
		}
	}
	else
	{
		input_tax = 0.0;
	}
	
	$("#input_tax"+idx).val(round(input_tax, 2));
	computeInputTax();
}

function computeRowInputTax(idx)
{
	vendor_key = $("#vendor"+idx).val();

	if (vendor_key != "")
	{
		is_vat_registered = vendors[vendor_key]["is_registered"];
		expense_amount = $("#expense_amount"+idx).val();
		amount = $("#expense_amount"+idx).val();
		if (amount != "" && is_vat_registered == "True")
		{
			input_tax = parseFloat(amount) / 1.12 * 0.12;
		}
		else
		{
			input_tax = "0.0";
		}
	}
	else
	{
		input_tax = "0.0";
	}
	$("#input_tax"+idx).val(input_tax);
}

function getInputTax(expense_amount, is_vat_registered)
{
	if (is_vat_registered)
	{
		input_tax = parseFloat(expense_amount) / 1.12 * 0.12;
	}
	else
	{
		input_tax = 0.0;
	}
	
	return input_tax;
}

function isVendorVatRegistered()
{
	vendor_key = document.getElementById("vendor_id").options[document.getElementById("vendor_id").selectedIndex ].value;
	if(vendor_key != "")
	{
		if (vendors[vendor_key]['is_registered'] == "True")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function computeInputTaxesOfEachItem(table_name) {
	var tbl = document.getElementById(table_name); // table reference
	var lastRow = tbl.rows.length - 1;            // set the last row index
	var i;
	// delete rows with index greater then 0
	for (i = 0; i < lastRow; i++) {
		computeItemInputTax(i);
	}
}

function computeWTaxesOfEachItem() {
	var tbl = document.getElementById('expense_item_table'); // table reference
	var lastRow = tbl.rows.length - 1;            // set the last row index
	var i;
	// delete rows with index greater then 0
	for (i = 0; i < lastRow; i++) {
		computeItemWithholdingTax(i);
	}
}

function computeTotalOfRows(row_name, cell_total)
{
	totalAmount = 0;
	$( "td[name$='" + row_name + "']" ).each(function(){
		if(!isNaN(this.innerHTML) && this.innerHTML != "" )
		{
			totalAmount += parseFloat(this.innerHTML);
		}
	});
	if(document.getElementById(cell_total))
	{
		document.getElementById(cell_total).innerHTML = totalAmount;
	}
}

function setCheckInfo()
{
	check_key = $( "#check_id" ).val();
	if (check_key == "" || check_key == "N/A")
	{
		// Reset fund amount and remarks
		$("#released_fund_amount").val(0);
		$("#released_fund_amount").prop('disabled', false);
		$("#remarks").val("");
		$("#remarks").prop('disabled', false);
	}
	else
	{
		// Set fund amount and remarks
		fund_value = checks[check_key]['check_amount']
		remarks = checks[check_key]['notes']
		$("#released_fund_amount").val(fund_value); 
		$("#released_fund_amount").prop('disabled', true);
		$("#remarks").val(remarks);
		$("#remarks").prop('disabled', true);
	}
}

function searchData() {
	// format search data
	var search_data = $( "#search_data" ).val();
	var formatted_search_data = search_data.replace(/ /g, "+");	
	
	// check validity
	if (formatted_search_data == "")
	{
		return;
	}
	
	// perform search
	var url = '/search?search_data=' + formatted_search_data;
    window.location.href = url;
	 
	return false;
}

function updateExpenseReportTable()
{
	var reporttbl = document.getElementById('expense_report_table'); // report table
	var expensetbl = document.getElementById('expenses_table'); // expense table
	var i;
	
	console.log("reporttbl len: " + reporttbl.rows.length);
	//read expense type per row. total all values with the current expense type
	for (i = 0; i < reporttbl.rows.length - 1; i++) {
		
		// get expense type name
		var expensetype = reporttbl.rows.cells[0].innerHTML;
		var totalexpense = 0.0;
		console.log("current expense type: " + expensetype + " expense table len: " + (expensetbl.rows.length - 1));
		if (expensetype != "")
		{
			for( var j = 0; j < expensetbl.rows.length - 1; j++ )
			{
				
				//check if expense type is same
				console.log("row expense type: " + expensetbl.rows[j].cells[5].innerHTML);
				if (expensetype == expensetbl.rows[j].cells[5])
				{
					totalexpense += parseFloat(expensetbl.rows[j].cells[2].innerHTML);
					console.log("row expense type: " + expensetbl.rows[j].cells[5].innerHTML + " : " + expensetbl.rows[j].cells[2].innerHTML);
				}
			}
			
			reporttbl.rows.cells[1].innerHTML = totalexpense;
			console.log("current expense type totalexpense: " + totalexpense);
		}
	}
}