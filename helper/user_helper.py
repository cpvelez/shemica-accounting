from google.appengine.api import users
from google.appengine.ext import ndb
from array import array
from employee.models import Employee

from django.http import HttpResponse


import logging

# Constants
PERMISSION_ADMIN_ONLY = 0
PERMISSION_NORMAL_USERS = 1
PERMISSION_ALL_USERS = 2

TEMPLATE_BASE = 'budget/base.html'
TEMPLATE_BASE2 = 'budget/base2.html'
TEMPLATE_BLANK = 'budget/blank.html'
PAGE_UNAUTH = "budget/unathorizedAccessPage.html"

# Configs
ADMIN_USER_EMAILS = ['cpcvelez', 'miguelantoniovelez']

def get_user_access_type():
	current_user = users.get_current_user()
	if is_user_admin(current_user):
		return 1
	user = ndb.Key(Employee, current_user.email()).get()
	return user.access_type

def get_current_user_email_address():

	return users.get_current_user()

def is_user_admin(current_user):
	return str(current_user) in ADMIN_USER_EMAILS

def is_user_permitted(permission=None):
	current_user = users.get_current_user()
	if is_user_admin(current_user):
		return True
	else:
		user = ndb.Key(Employee, current_user.email()).get()

	# Check if user is listed
	if not user:
		return False

	# If user has access type = 1(Admin), return true
	if user.access_type == 1:
		return True
	# If user has access type = 2(Normal) and permission is for at least normal users, return true
	elif user.access_type == 2 and permission >= PERMISSION_NORMAL_USERS:
		return True
	# If user has access type = 2(Limited) and permission is for all users, return true
	elif user.access_type == 3 and permission == PERMISSION_ALL_USERS:
		return True
	# If user has access type = 0(No Access), return false
	elif user.access_type == 0:
		return False
	else:
		return False

def getBaseOfAccessType(access_type):
	if access_type == 1 or access_type == 2:
		return TEMPLATE_BASE
	elif access_type == 3:
		return TEMPLATE_BASE2
	else:
		return PAGE_UNAUTH