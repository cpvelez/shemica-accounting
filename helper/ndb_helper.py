import logging
import datetime
from helper import user_helper

def make_ndb_return_data_json_serializable(data):
    """Build a new dict so that the data can be JSON serializable"""

    result = data.to_dict()
    record = {}

    # Populate the new dict with JSON serializiable values
    for key in result.iterkeys():
        if isinstance(result[key], datetime.datetime):
            record[key] = result[key].isoformat()
            continue
        record[key] = result[key]
    
    # Add the key so that we have a reference to the record
    record['key'] = data.key.urlsafe()

    # sandy_key = ndb.Key(urlsafe=url_string)
    # sandy = sandy_key.get()

    return record


def filter_results(qry):
    """
    Send NDB query result to serialize function if single result, 
    else loop through the query result and serialize records one by one
    """

    result = []

    # check if qry is a list (multiple records) or not (single record)
    if type(qry) != list:
        record = make_ndb_return_data_json_serializable(qry)
        return record

    for q in qry:
        result.append(make_ndb_return_data_json_serializable(q))

    return result

def update_meta_data(object, is_creation):
	if is_creation:
		object.created_by = user_helper.get_current_user_email_address().email()
		object.created_date = datetime.datetime.now()
		object.modified_by = None
		object.modified_date = None
	else:
		object.modified_by = user_helper.get_current_user_email_address().email()
		object.modified_date = datetime.datetime.now()

def convertToCurrentTZ(datetime_to_format):
	if datetime_to_format:
		add_8 = datetime.timedelta(hours=+8)
		currentDT = datetime_to_format + add_8
		return currentDT.strftime("%b %d, %Y, %I:%M %p")
	else:
		return datetime_to_format