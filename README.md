**Set-up environment**

Make virtual environment para di magulo ang python packages

You can refer to this http://roundhere.net/journal/virtualenv-ubuntu-12-10/

Or follow my own tutorial:

To install pip (pip is python package manager)

```
#!bash

sudo apt-get install python-pip
```


To install virtualenv using pip

```
#!bash

sudo pip install virtualenv
```


Make your virtualenv directory on your home folder (this will serve as your storage for your virtual envs)

```
#!bash

mkdir ~/.virtualenvs
```


Also install virtualenvwrapper to have more commands to edit your virtual environment

```
#!bash

sudo pip install virtualenvwrapper
```


Add the following lines on your .bashrc file so virtual env and virtualenv wrapper’s commands will be globally available on your bash

On your home directory, run

```
#!bash

sudo nano .bashrc
```

add the following lines at the end of the file

```
#!line

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
```


press ctrl+o to save the edited file and ctrl+x to exit
restart your bash window (close and open again)

Create your own virtual env



```
#!bash

mkvirtualenv desiredprojectname
```


To work on your virtual environment, run the following command

```
#!bash

workon desiredprojectname
```


Install App Engine SDK - https://cloud.google.com/appengine/downloads?hl=en

Install Google Cloud SDK - https://cloud.google.com/sdk/