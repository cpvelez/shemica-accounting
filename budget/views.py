from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import logout
from django.db import transaction

from calendar import monthrange
from cStringIO import StringIO

from google.appengine.api import users
from google.appengine.api import memcache
from google.appengine.ext import deferred
from google.appengine.api import images
from google.appengine.ext import ndb

from budget.models import *
from employee.models import *

from helper import ndb_helper, user_helper

from google.appengine.api import mail

import urllib
import logging
import datetime
import StringIO
import csv

#Constants
Payment_Method_Cash = 0
Payment_Method_Check = 1

Check_Status_Ready = 0
Check_Status_Released = 1
Check_Status_Paid = 2
Check_Status_Returned = 3

Generic_Status_Active = 0
Generic_Status_Cancelled = 1

Rel_Fund_Status_Open = 0
Rel_Fund_Status_Closed = 1
Rel_Fund_Status_Cancelled = 2

Search_Type_Released_Fund = 0
Search_Type_Expense = 1

#Configs
#Email Configs
EMAIL_SENDER = "SHEMICA Accounting System <system@shemica-accounting.appspotmail.com>"
EMAIL_SENDER_NAME = "Shemica Inc Accounting System"

# Released Funds Email Config
REL_FUND_EMAIL_RECIPIENTS = "cpvelez@yahoo.com"
#REL_FUND_EMAIL_RECIPIENTS = "cpcvelez@gmail.com"
SITE_LINK_TO_REL_FUNDS = "https://shemica-accounting.appspot.com/budget/releasedfund/expenses/"

# Add Check Email Config
ADD_CHECK_EMAIL_RECIPIENTS = "cpvelez@yahoo.com"
#ADD_CHECK_EMAIL_RECIPIENTS = "cpcvelez@gmail.com"
SITE_LINK_TO_CHECKS = "https://shemica-accounting.appspot.com/check/"

# Newly Closed Funds Email Config
LIQUIDATED_EMAIL_RECIPIENTS = "cpvelez@yahoo.com"

# Site Home Link
SITE_HOME_LINK = "https://shemica-accounting.appspot.com"

def main(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
	return render(request, 'budget/index.html', {"first_name": user_helper.get_current_user_email_address().nickname(), 'base_template': base_template}, content_type="text/html")

@ensure_csrf_cookie
def create_credit(request, payment_method=""):

	if request.method == 'GET':
		# Retrieve active budgets
		budget_objects = Budget.query(Budget.is_active == True).order(Budget.name).fetch(projection=[Budget.name,Budget.remaining_amount, Budget.maintaining_amount])
		budgets = ndb_helper.filter_results(budget_objects)

		vendors = getVendors()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		if payment_method == 'cash':
			credit_page = 'budget/addCash.html'
		else:
			credit_page = 'budget/addCheck.html'
		return render(request, credit_page, {'budgets' : budgets, 'vendors': vendors, 'base_template': base_template},
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		# If check, check if duplicate check number
		if int(request.POST['credit_payment_method']) == 1:
			check_no = request.POST['credit_check_no']
			if check_check_number(request, check_no):
				return HttpResponse('duplicate')
		response = create_credit_post(request)
		list_credit_fund(request)
		return response

@ensure_csrf_cookie
def create_check(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	response = create_credit(request, "check")
	return response

@ensure_csrf_cookie
def create_cash(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	response = create_credit(request, "cash")
	return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def create_credit_post(request):
	#Create credit
	credit = Credit()
	date = request.POST['credit_date_issued']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	credit.date_issued = date_
	credit.amount = float(request.POST['credit_amount'])
	credit.payment_method = int(request.POST['credit_payment_method'])
	credit.check_no = request.POST['credit_check_no']
	credit.check_ref_no = request.POST['credit_ref_no']
	credit.notes = request.POST['credit_notes']
	credit.check_vendor_id = request.POST['check_vendor_id']
	credit.check_vendor_name = request.POST['check_vendor_name']
	check_date = request.POST['check_date']
	check_date_ = datetime.datetime.strptime(check_date, "%Y-%m-%d")
	credit.check_date = check_date_
	ndb_helper.update_meta_data(credit, True)
	credit_key = credit.put()

	#Create Funds
	funds = simplejson.loads(request.POST['funds'])
	for item in funds:
		if float(item["amount"]) != 0 :
			fund = Fund(parent=credit_key)
			fund.date_funded = date_
			credit_fund_id = IdCounter.record('credit_fund_number')
			current_year = datetime.datetime.now().year
			fund.fund_number = 'C' + str(current_year) + '' + str(credit_fund_id)
			fund.amount = float(item["amount"])
			fund.credit_id = credit_key.urlsafe()
			fund.credit_notes = request.POST['credit_notes']
			fund.target_id = item["budget_key"]
			fund.target_name = item["budget_name"]
            
			# Get Budget to get point person name
			budget_id = fund.target_id
			budget_key = ndb.Key(urlsafe=item["budget_key"])
			budget = budget_key.get()
			fund.point_person_name = budget.holder_name
			ndb_helper.update_meta_data(fund, True)
			fund.put()
			# Update budget and cash on hand if cash
			if credit.payment_method == Payment_Method_Cash:
				#Update affected budget
				budget.remaining_amount = budget.remaining_amount + fund.amount
				budget.put()
				#Update Cash on Hand of affected employee
				employee_key = ndb.Key(urlsafe=budget.holder)
				employee = employee_key.get()
				employee.cash_on_hand = employee.cash_on_hand + fund.amount
				employee.put()
	
	# Disabled adding of check notification
	# If check, email notification that a check was added
	#if credit.payment_method == 1:
	#	mail.send_mail(sender=EMAIL_SENDER,to=ADD_CHECK_EMAIL_RECIPIENTS,subject="Check Added! P" + str(credit.amount) + " | Check No. " + credit.check_no + " | " + credit.notes,
		
#body="""Dear Sheila, Miguel and Carlo:

#A check was added with the ff. details:

#Check No: """ + str(credit.check_no) + """
#Amount: P""" + str(credit.amount) + """
#Notes: """ + str(credit.notes) + """
#Vendor: """ + str(credit.check_vendor_name) + """
#Check Date: """ + str(credit.check_date) + """
#Ref. No: """ + str(credit.check_ref_no) + """

#For more details, click the link below:
#""" + SITE_LINK_TO_CHECKS + credit.key.urlsafe() + """

#This is an automatically generated email, please do not reply.

#Regards,
#""" + EMAIL_SENDER_NAME)
	return HttpResponse('ok')

@ensure_csrf_cookie
def update_credit(request, credit_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	if request.method == 'GET':
		credit_key = ndb.Key(urlsafe=credit_id)
		credit = credit_key.get()
		credit_key = credit.key.urlsafe()
		funds = getFunds(credit_id)
		vendors = getVendors()
		check_date = ""
		if credit.check_date:
			check_date = credit.check_date.strftime("%Y-%m-%d")

		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'credit' : credit,
			'credit_date' : credit.date_issued.strftime("%Y-%m-%d"),
			'check_date' : check_date,
			'credit_key' : credit_key,
			'funds' : funds,
			'vendors': vendors,
			'base_template': base_template
		}
		return render(request, 'budget/editCredit.html', context,
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = update_credit_post(request, credit_id)
		list_credit_fund(request)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_credit_post(request, credit_id):
	credit_key = ndb.Key(urlsafe=credit_id)
	credit = credit_key.get()
	date = request.POST['credit_date_issued']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	credit.date_issued = date_
	credit.amount = float(request.POST['credit_amount'])
	credit.payment_method = int(request.POST['credit_payment_method'])
	credit.check_no = request.POST['credit_check_no']
	credit.check_ref_no = request.POST['credit_ref_no']
	credit.notes = request.POST['credit_notes']
	credit.check_vendor_id = request.POST['check_vendor_id']
	credit.check_vendor_name = request.POST['check_vendor_name']
	check_date = request.POST['check_date']
	check_date_ = datetime.datetime.strptime(check_date, "%Y-%m-%d")
	credit.check_date = check_date_
	credit.check_status = int(request.POST['check_status'])
	ndb_helper.update_meta_data(credit, False)
	credit.put()

	#Update Funds
	funds = simplejson.loads(request.POST['funds'])
	for item in funds:
		fund_key = ndb.Key(urlsafe=item["fund_key"])
		fund = fund_key.get()
		old_amount = fund.amount
		
		fund.date_funded = date_
		fund.amount = float(item["amount"])
		fund.credit_id = credit_key.urlsafe()
		fund.target_id = item["budget_key"]
		fund.credit_notes = request.POST['credit_notes']
		fund.target_name = item["budget_name"]
		ndb_helper.update_meta_data(fund, False)
		fund.put()
		# Update budget and cash on hand if cash
		if credit.payment_method == Payment_Method_Cash:
			#Update affected budget
			budget_id = fund.target_id
			budget_key = ndb.Key(urlsafe=budget_id)
			budget = budget_key.get()
			if old_amount < fund.amount:
				offset = fund.amount - old_amount
				budget.remaining_amount = budget.remaining_amount + offset
			else:
				offset = old_amount - fund.amount
				budget.remaining_amount = budget.remaining_amount - offset
			budget.put()
			#Update Cash on Hand of affected employee
			employee_key = ndb.Key(urlsafe=budget.holder)
			employee = employee_key.get()
			if old_amount < fund.amount:
				offset = fund.amount - old_amount
				employee.cash_on_hand = employee.cash_on_hand + offset
			else:
				offset = old_amount - fund.amount
				employee.cash_on_hand = employee.cash_on_hand - offset
			employee.put()
	
	# If check is cancelled, update status of released fund and expense
	if credit.payment_method == Payment_Method_Check:
		# Get released fund and update
		released_fund_id = credit.check_rel_fund
		if released_fund_id:
			released_fund_key = ndb.Key(urlsafe=released_fund_id)
			released_fund = released_fund_key.get()
			released_fund.status = Rel_Fund_Status_Cancelled;
			released_fund.put()
		# Get expense and update
		expense_id = credit.check_expense
		if expense_id:
			expense_key = ndb.Key(urlsafe=expense_id)
			expense = expense_key.get()
			expense.released_fund_status = Rel_Fund_Status_Cancelled;
			expense.active_status = Generic_Status_Cancelled;
			expense.put()
	
	return HttpResponse('ok')


def list_credit(request, payment_method=-1):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Get date today
		now = date.today()
		date_month = now.month
		date_year = now.year

		month_year = datetime.datetime(int(date_year),int(date_month),1)
		days = monthrange(int(date_year), int(date_month))
		days_ = days[1]
        
        
		
		query = Credit.query(ndb.AND(Credit.date_issued >= month_year, Credit.date_issued <= month_year + datetime.timedelta(days=days_-1)))
		if payment_method == Payment_Method_Cash:
			query = query.filter(Credit.payment_method == Payment_Method_Cash)
			credit_objects = query.order(-Credit.date_issued).fetch()
		elif payment_method == Payment_Method_Check:
			query = query.filter(Credit.payment_method == Payment_Method_Check)
			credit_objects = query.order(-Credit.date_issued).fetch()
		else:
			credit_objects = Credit.query().order(-Credit.date_issued).fetch()
		credits_dict = ndb_helper.filter_results(credit_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		paginator = Paginator(credits_dict, 1000)
		page = request.GET.get('page')
		paged = request.GET.get('paged')

		pageTemplate = 'budget/viewCredits.html'
		if payment_method == Payment_Method_Cash:
			pageTemplate = 'budget/viewCash.html'
		elif payment_method == Payment_Method_Check:
			pageTemplate = 'budget/viewCheck.html'
		else:
			pageTemplate = 'budget/viewCredits.html'

		try:
		    credits_dict = paginator.page(page)
		except PageNotAnInteger:
		    # If page is not an integer, deliver first page.
		    credits_dict = paginator.page(1)
		except EmptyPage:
		    # If page is out of range (e.g. 9999), deliver last page of results.
		    credits_dict = paginator.page(paginator.num_pages)
		if paged:
			base_template = user_helper.TEMPLATE_BLANK
		return render(request, pageTemplate, {"credits": credits_dict, 'date_year': date_year, 'date_month': date_month, 'user_access_type': user_access_type, 'base_template': base_template})

def list_cash(request):
	response = list_credit(request, Payment_Method_Cash)
	return response

def list_check(request):
	response = list_credit(request, Payment_Method_Check)
	return response

def filter_credit(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	if request.method == 'GET':
		date_year = request.GET['credit_year_filter']
		date_month = request.GET['credit_month_filter']
		payment_method = request.GET['credit_payment_filter']

		query = Credit.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = Credit.query(ndb.AND(Credit.date_issued >= month_year, Credit.date_issued <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = Credit.query(ndb.AND(Credit.date_issued >= month_year_start, Credit.date_issued <= month_year_end))

		if payment_method != 'All':
			query = query.filter(Credit.payment_method == int(payment_method))

		credit_objects = query.order(-Credit.date_issued).fetch()
		logging.info(credit_objects)
		credits_dict = ndb_helper.filter_results(credit_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.TEMPLATE_BLANK
		paginator = Paginator(credits_dict, 10000)
		page = request.GET.get('page')
		paged = request.GET.get('paged')
		pageTemplate = 'budget/viewCredits.html'
		if payment_method == str(Payment_Method_Cash):
			pageTemplate = 'budget/viewCash.html'
		elif payment_method == str(Payment_Method_Check):
			pageTemplate = 'budget/viewCheck.html'
		else:
			pageTemplate = 'budget/viewCredits.html'

		try:
		    credits_dict = paginator.page(page)
		except PageNotAnInteger:
		    # If page is not an integer, deliver first page.
		    credits_dict = paginator.page(1)
		except EmptyPage:
		    # If page is out of range (e.g. 9999), deliver last page of results.
		    credits_dict = paginator.page(paginator.num_pages)

		return render(request, pageTemplate, {"credits": credits_dict, "date_year": date_year, "date_month": date_month, "payment_method":payment_method, 'user_access_type': user_access_type, 'base_template': base_template})

@ensure_csrf_cookie
def list_check_details(request, credit_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		credit_key = ndb.Key(urlsafe=credit_id)
		credit = credit_key.get()
		credit_key = credit.key.urlsafe()
		credit_created_date = ndb_helper.convertToCurrentTZ(credit.created_date)
		credit_modified_date = ndb_helper.convertToCurrentTZ(credit.modified_date)
		released_fund = None
		released_fund_key = None
		if credit.check_rel_fund:
			released_fund_key = ndb.Key(urlsafe=credit.check_rel_fund)
			released_fund = released_fund_key.get()
			released_fund_key = released_fund.key.urlsafe()
		expense = None
		expense_key = None
		if credit.check_expense:
			expense_key = ndb.Key(urlsafe=credit.check_expense)
			expense = expense_key.get()
			expense_key = expense.key.urlsafe()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'credit': credit,
			'credit_key': credit_key,
			'released_fund': released_fund,
			'released_fund_key': released_fund_key,
			'expense' : expense,
			'expense_key': expense_key,
			'credit_created_date': credit_created_date,
			'credit_modified_date': credit_modified_date,
			'base_template': base_template
		}
		return render(request, 'budget/viewCheckDetails.html', context, content_type="text/html")

def filter_funds(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	if request.method == 'GET':
		date_year = request.GET['funds_year_filter']
		date_month = request.GET['funds_month_filter']

		query = Fund.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = Fund.query(ndb.AND(Fund.date_funded >= month_year, Fund.date_funded <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = Fund.query(ndb.AND(Fund.date_funded >= month_year_start, Fund.date_funded <= month_year_end))

		fund_objects = query.order(-Fund.date_funded).fetch()
		funds_dict = ndb_helper.filter_results(fund_objects)
		user_access_type = user_helper.get_user_access_type()
		budgets = list_budget_expense(request)
		base_template = user_helper.TEMPLATE_BLANK
		context = {
			"funds": funds_dict,
			"date_year": date_year,
			"date_month": date_month,
			'base_template': base_template
		}
		return render(request, 'budget/viewFunds.html', context)

def filter_released_funds(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	if request.method == 'GET':
		date_year = request.GET['released_funds_year_filter']
		date_month = request.GET['released_funds_month_filter']
		
		query = ReleasedFund.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = ReleasedFund.query(ndb.AND(ReleasedFund.date_released >= month_year, ReleasedFund.date_released <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = ReleasedFund.query(ndb.AND(ReleasedFund.date_released >= month_year_start, ReleasedFund.date_released <= month_year_end))

		released_funds_objects = query.order(-ReleasedFund.date_released).fetch()
		logging.info(released_funds_objects)
		released_funds_dict = ndb_helper.filter_results(released_funds_objects)
		user_access_type = user_helper.get_user_access_type()

		base_template = user_helper.TEMPLATE_BLANK
	
		context = {
			"released_funds": released_funds_dict,
			"date_year": date_year,
			"date_month": date_month,
			"user_access_type": user_access_type,
			"base_template": base_template
		}
		return render(request, 'budget/viewReleasedFunds.html', context)

@ensure_csrf_cookie
def create_budget(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	if request.method == 'GET':
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		return render(request, 'budget/addBudget.html', {'employees' : employees, 'base_template': base_template},
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		name = request.POST['budget_name']
		if check_budget_name(request, name):
			return HttpResponse('duplicate')
		response = create_budget_post(request)
		list_budget_expense(request, 'update')
		return response

@ndb.transactional(retries=3)
@ensure_csrf_cookie
def create_budget_post(request):
	employee_urlsafe = request.POST['budget_holder']
	employee_key = ndb.Key(urlsafe=employee_urlsafe)
	employee = employee_key.get()
	budget = Budget(parent=employee_key)
	budget.maintaining_amount = float(request.POST['budget_amount'])
	budget.name = request.POST['budget_name']
	budget.holder = employee.key.urlsafe()
	budget.holder_name = request.POST['budget_holder_name']
	budget.default_department = int(request.POST['budget_default_department'])
	ndb_helper.update_meta_data(budget, True)
	budget.put()
	return HttpResponse('ok')

def update_budget_names(budget_key, name):
	logging.info('runninng update_budget name')
	objects = ReleasedFund.query(ReleasedFund.source_id == budget_key)
	for obj in objects:
		obj.source_name = name
		obj.put()

@ensure_csrf_cookie
def update_budget(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		budget_key = budget.key.urlsafe()
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'budget' : budget,
			'budget_key' : budget_key,
			'employees' : employees,
			'base_template': base_template
		}
		return render(request, 'budget/editBudget.html', context,
			content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		budget_name = request.POST['budget_name']
		if check_budget_name(request, budget_name, budget_id):
			return HttpResponse('duplicate')
		response = update_budget_post(request, budget_id)
		list_budget_expense(request, 'update')
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		update_budget_names(budget.key.urlsafe(), budget.name)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_budget_post(request, budget_id): 
	budget_key = ndb.Key(urlsafe=budget_id)
	budget = budget_key.get()
	logging.info(dir(budget))
	if budget.maintaining_amount != float(request.POST['budget_amount']):
		budget.maintaining_amount = float(request.POST['budget_amount'])

	if budget.name != request.POST['budget_name']:
		budget.name = request.POST['budget_name']
	
	if budget.default_department != int(request.POST['budget_default_department']):
		budget.default_department = int(request.POST['budget_default_department'])
		
	if budget.is_active != bool(int(request.POST['budget_is_active'])):
		budget.is_active = bool(int(request.POST['budget_is_active']))

	#Store old and new holders
	old_holder = budget.holder
	new_holder = request.POST['budget_holder']

	if old_holder != new_holder:
		budget.holder = request.POST['budget_holder']
		budget.holder_name = request.POST['budget_holder_name']
	ndb_helper.update_meta_data(budget, False)
	budget.put()

	if old_holder != new_holder:
		# Transfer funds from old holder to next holder
		# Get old holder
		from_employee_key = ndb.Key(urlsafe=old_holder)
		from_employee = from_employee_key.get()
		# Compute amount to be transferred
		transfer_amount = budget.remaining_amount
		# Deduct amount to be transferred from old holder
		from_employee.cash_on_hand -= transfer_amount
		from_employee.put()
		# Get new holder
		to_employee_key = ndb.Key(urlsafe=new_holder)
		to_employee = to_employee_key.get()
		# Transfer amount to new holder
		to_employee.cash_on_hand += transfer_amount
		to_employee.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def transfer_budget_funds(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Retrieve active budgets
		budget_objects = Budget.query(Budget.is_active == True).order(-Budget.name).fetch(projection=[Budget.name,Budget.remaining_amount, Budget.maintaining_amount])
		budgets = ndb_helper.filter_results(budget_objects)

		# Retrieve active budget
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		budget_key = budget.key.urlsafe()

		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'budgets' : budgets,
			'budget' : budget,
			'budget_key' : budget_key,
			'base_template': base_template
		}
		return render(request, 'budget/transferBudgetFunds.html', context,
			content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = transfer_budget_funds_post(request, budget_id)

		return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def create_budget_cash_post(request, budget_id):
	#Create budget cash
	credit = Credit()
	date = request.POST['credit_date_issued']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	credit.date_issued = date_
	credit.amount = float(request.POST['credit_amount'])
	credit.payment_method = int(request.POST['credit_payment_method'])
	credit.check_no = request.POST['credit_check_no']
	credit.check_ref_no = request.POST['credit_ref_no']
	credit.notes = request.POST['credit_notes']
	credit.check_vendor_id = request.POST['check_vendor_id']
	credit.check_vendor_name = request.POST['check_vendor_name']
	check_date = request.POST['check_date']
	check_date_ = datetime.datetime.strptime(check_date, "%Y-%m-%d")
	credit.check_date = check_date_
	ndb_helper.update_meta_data(credit, True)
	credit_key = credit.put()

    #Create Funds
	funds = simplejson.loads(request.POST['funds'])
	for item in funds:
		if float(item["amount"]) != 0 :

			#Update affected budget
			budget_key = ndb.Key(urlsafe=item["budget_key"])
			budget = budget_key.get()

			fund = Fund(parent=credit_key)
			fund.date_funded = date_
			credit_fund_id = IdCounter.record('credit_fund_number')
			current_year = datetime.datetime.now().year
			fund.fund_number = 'C' + str(current_year) + '' + str(credit_fund_id)
			fund.amount = float(item["amount"])
			fund.credit_notes = request.POST['credit_notes']
			fund.credit_id = credit_key.urlsafe()
			fund.target_id = item["budget_key"]
			fund.target_name = item["budget_name"]
			fund.point_person_name = budget.holder_name
			ndb_helper.update_meta_data(fund, True)
			fund.put()
        
			# Update budget and cash on hand if cash
			budget.remaining_amount = budget.remaining_amount + fund.amount
			budget.put()
			#Update Cash on Hand of affected employee
			employee_key = ndb.Key(urlsafe=budget.holder)
			employee = employee_key.get()
			employee.cash_on_hand = employee.cash_on_hand + fund.amount
			employee.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def create_budget_cash(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		budget_key = budget.key.urlsafe()

		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'budget' : budget,
			'budget_key' : budget_key,
			'base_template': base_template
		}
		return render(request, 'budget/addBudgetCash.html', context,
			content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_budget_cash_post(request, budget_id)

		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def transfer_budget_funds_post(request, budget_id): 
	# Check if budget from has enough funds to transfer
	budget_key = ndb.Key(urlsafe=budget_id)
	budget = budget_key.get()
	if float(request.POST['amount_to_transfer']) > budget.remaining_amount:
		return HttpResponse('insufficient')
	
	amount_to_transfer = float(request.POST['amount_to_transfer'])
	budget_to_id = request.POST['budget_to']
	
	# Get budget from
	budget_from_key = ndb.Key(urlsafe=budget_id)
	budget_from = budget_from_key.get()
	# Get budget to
	budget_to_key = ndb.Key(urlsafe=budget_to_id)
	budget_to = budget_to_key.get()
	
	# Create credit
	credit = Credit()
	today = datetime.datetime.now()
	date_transferred = str(today.year) + '-' + str(today.month) + '-' + str(today.day)
	date_ = datetime.datetime.strptime(str(date_transferred), "%Y-%m-%d")
	credit.date_issued = date_
	credit.amount = 0
	credit.payment_method = 0
	credit.check_no = "N/A"
	credit.notes = request.POST['notes']
	ndb_helper.update_meta_data(credit, True)
	credit_key = credit.put()
	
	# Create funds to add
	fund_to = Fund(parent=credit_key)
	fund_to.date_funded = date_
	credit_fund_id = IdCounter.record('credit_fund_number')
	current_year = datetime.datetime.now().year
	fund_to.fund_number = 'C' + str(current_year) + '' + str(credit_fund_id)
	fund_to.amount = amount_to_transfer
	fund_to.credit_id = credit_key.urlsafe()
	fund_to.target_id = budget_to_id
	fund_to.target_name = budget_to.name
	fund_to.point_person_name = budget_to.holder_name
	fund_to.credit_notes = request.POST['notes']
	ndb_helper.update_meta_data(fund_to, True)
	fund_to.put()
	
	#Update budget to
	budget_to.remaining_amount = budget_to.remaining_amount + fund_to.amount
	budget_to.put()
	
	#Update Cash on Hand of affected employee (budget to)
	employee_key = ndb.Key(urlsafe=budget_to.holder)
	employee = employee_key.get()
	employee.cash_on_hand = employee.cash_on_hand + fund_to.amount
	employee.put()
	
	# Create funds to deduct
	fund_from = Fund(parent=credit_key)
	fund_from.date_funded = date_
	credit_fund_id = IdCounter.record('credit_fund_number')
	current_year = datetime.datetime.now().year
	fund_from.fund_number = 'C' + str(current_year) + '' + str(credit_fund_id)
	fund_from.amount = amount_to_transfer * -1
	fund_from.credit_id = credit_key.urlsafe()
	fund_from.target_id = budget_id
	fund_from.target_name = budget_from.name
	fund_from.point_person_name = budget_from.holder_name
	fund_from.credit_notes = request.POST['notes']
	ndb_helper.update_meta_data(fund_from, True)
	fund_from.put()
	
	#Update budget from
	budget_from.remaining_amount = budget.remaining_amount + fund_from.amount
	budget_from.put()
	
	#Update Cash on Hand of affected employee (budget from)
	employee_key = ndb.Key(urlsafe=budget_from.holder)
	employee = employee_key.get()
	employee.cash_on_hand = employee.cash_on_hand + fund_from.amount
	employee.put()
	
	return HttpResponse('ok')


def list_budget(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
	    budget_active_objects = Budget.query(Budget.is_active == True).order(Budget.name).fetch()
	    budgets_active_dict = ndb_helper.filter_results(budget_active_objects)
	    budget_inactive_objects = Budget.query(Budget.is_active == False).order(Budget.name).fetch()
	    budgets_inactive_dict = ndb_helper.filter_results(budget_inactive_objects)
	    user_access_type = user_helper.get_user_access_type()
	    base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

	    return render(request, 'budget/viewBudgets.html', {"active_budgets": budgets_active_dict, "inactive_budgets": budgets_inactive_dict, 'user_access_type': user_access_type, 'base_template': base_template})

@ensure_csrf_cookie
def list_fund(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
	    # Get date today
	    now = date.today()
	    date_month = now.month
	    date_year = now.year
		
	    month_year = datetime.datetime(int(date_year),int(date_month),1)
	    days = monthrange(int(date_year), int(date_month))
	    days_ = days[1]

	    query = Fund.query(ndb.AND(Fund.date_funded >= month_year, Fund.date_funded <= month_year + datetime.timedelta(days=days_-1)))
	    fund_objects = query.order(-Fund.date_funded).fetch()
	    funds_dict = ndb_helper.filter_results(fund_objects)
	    base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
	    context = {
			"funds": funds_dict,
			"date_month": date_month,
			"date_year": date_year,
			'base_template': base_template
		}
	    return render(request, 'budget/viewFunds.html', context)

@ensure_csrf_cookie
def create_released_fund(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	# TODO: Check if user is admin or budget holder
	
	if request.method == 'GET':
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		employees = getEmployees()
		checks = getChecksOfStatus(Check_Status_Ready)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		release_type = request.GET.get('release_type')
		context = {
			'budget':budget,
			'budget_key':budget_id,
			'employees':employees,
			'checks': checks,
			'release_type': release_type,
			'base_template': base_template
		}
		return render(request, 'budget/addReleasedFund.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_release_fund_post(request)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_release_fund_post(request): 
	budget_id = request.POST['released_fund_source']
	budget_key = ndb.Key(urlsafe=budget_id)
	budget = budget_key.get()
	payment_method = int(request.POST['payment_method'])
	# Check if there is sufficient funds to release
	if payment_method == 0 and float(request.POST['released_fund_amount']) > budget.remaining_amount:
		return HttpResponse('insufficient')

	# Create Release Fund object
	released_fund = ReleasedFund()
	rel_id = IdCounter.record('fund_number')
	current_year = datetime.datetime.now().year
	released_fund.fund_number = 'RF' + str(current_year) + '' + str(rel_id)
	released_fund.source_id = request.POST['released_fund_source']
	released_fund.source_name = request.POST['released_fund_source_name']
	released_fund.source_department = int(request.POST['released_fund_source_def_department'])
	date = request.POST['released_fund_date_released']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	released_fund.date_released = date_
	released_fund.released_amount = float(request.POST['released_fund_amount'])
	released_fund.amount = float(request.POST['released_fund_amount'])
	released_fund.actual_expense = 0;
	released_fund.variance = released_fund.amount - released_fund.actual_expense
	released_fund.point_person = request.POST['released_fund_point_person']
	released_fund.point_person_name = request.POST['released_fund_point_person_name']
	released_fund.default_liquidator = request.POST['released_fund_liquidator']
	released_fund.default_liquidator_name = request.POST['released_fund_liquidator_name']
	released_fund.remarks = request.POST['released_fund_remarks']
	released_fund.notes = request.POST['released_fund_notes']
	released_fund.check_no = request.POST['released_fund_check_no']
	released_fund.check_credit_id = request.POST['released_fund_check']
	released_fund.check_vendor_id = request.POST['released_fund_check_vendor']
	released_fund.payment_method = payment_method
	released_fund.release_type = int(request.POST['release_type'])
	ndb_helper.update_meta_data(released_fund, True)
	released_fund_key = released_fund.put()
	
	# Update check info if a check is released
	if payment_method == Payment_Method_Check:
		credit_id = released_fund.check_credit_id
		credit_key = ndb.Key(urlsafe=credit_id)
		credit = credit_key.get()
		credit.check_status = Check_Status_Released
		credit.check_rel_fund = released_fund_key.urlsafe()
		credit.put()
	else:
		# Deduct released funds from affected budget
		budget_id = released_fund.source_id
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		budget.remaining_amount = float(float(budget.remaining_amount) - float(released_fund.amount))
		budget.put()
		
		# Get amount to be transferred
		transfer_amount = released_fund.released_amount
		# Deduct released amount from credit on hand of budget holder
		from_employee_key = ndb.Key(urlsafe=budget.holder)
		from_employee = from_employee_key.get()
		from_employee.cash_on_hand -= transfer_amount
		from_employee.put()
		# Add released funds on hand of point person
		to_employee_key = ndb.Key(urlsafe=released_fund.point_person)
		to_employee = to_employee_key.get()
		to_employee.released_funds_on_hand += transfer_amount
		to_employee.put()
	# Send email
	if payment_method == Payment_Method_Cash:
		email_subj_intro = "Cash Released!"
	elif payment_method == Payment_Method_Check:
		email_subj_intro = "Check Released!"
	else:
		email_subj_intro = "Funds Released!"

	mail.send_mail(sender=EMAIL_SENDER,to="carlovelez4@gmail.com",subject=email_subj_intro + " P" + str(released_fund.released_amount) + " | " + released_fund.source_name + " | " + released_fund.remarks,
		body="""Dear Miguel:

This is to notify you that funds were released with the ff. details:

Amount: P""" + str(released_fund.released_amount) + """
Budget: """ + released_fund.source_name + """
Purpose: """ + released_fund.remarks + """
Released Fund No.: """ + released_fund.fund_number + """
Released By: """ + released_fund.created_by + """
Released To: """ + str(released_fund.point_person_name) + """
Released Date: """ + str(released_fund.date_released) + """

For more details, click the link below:
""" + SITE_LINK_TO_REL_FUNDS + released_fund.key.urlsafe() + """

This is an automatically generated email, please do not reply.

Regards,
""" + EMAIL_SENDER_NAME)
			
	return HttpResponse(['ok:', released_fund.fund_number])

@ensure_csrf_cookie
def update_released_fund(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		released_fund_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_fund_key.get()
		released_fund_key = released_fund.key.urlsafe()
		employees = getEmployees()
		checks = getChecksOfStatus(Check_Status_Ready, released_fund.check_no)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		closed_date = ''		
		try:
			closed_date = released_fund.date_closed.strftime("%Y-%m-%d")
		except:
			pass
		context = {
			'released_fund': released_fund, 
			'released_fund_key':released_fund_key,
			'released_fund_date_closed': closed_date if closed_date else None,
			'released_fund_date_released':released_fund.date_released.strftime("%Y-%m-%d"),
			'employees': employees,
			'checks': checks,
			'user_access_type': user_access_type,
			'base_template': base_template
		}

		return render(request, 'budget/editReleasedFund.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = update_release_fund_post(request, released_fund_id)

		# update released fund expenses (NOT IMPLEMENTED DUE TO TOO MANY ENTITIES)
		# get released fund expenses
		# rel_fund_expenses = getReleasedFundExpenses(released_fund_id)
		# response = update_release_fund_expenses_post(request, released_fund_id, rel_fund_expenses)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_release_fund_post(request, released_fund_id): 
	budget_id = request.POST['released_fund_source']
	budget_key = ndb.Key(urlsafe=budget_id)
	budget = budget_key.get()
	payment_method = int(request.POST['payment_method'])
	if payment_method == 0 and float(request.POST['amount_to_add']) > budget.remaining_amount:
		return HttpResponse('insufficient')

	#Get released fund
	released_fund_id = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_id.get()
	#Store current values
	old_amount = released_fund.amount
	old_released_amount = released_fund.released_amount
	old_returned_amount = released_fund.returned_amount
	old_point_person = released_fund.point_person
	old_credit_id = released_fund.check_credit_id
	old_payment_method = released_fund.payment_method
	#Set new values
	date = request.POST['released_fund_date_released']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	released_fund.date_released = date_
	released_fund.source_id = request.POST['released_fund_source']
	released_fund.source_name = request.POST['released_fund_source_name']
	released_fund.source_department = int(request.POST['released_fund_source_def_department'])
	released_fund.released_amount = float(request.POST['released_fund_amount'])
	released_fund.returned_amount = float(request.POST['returned_amount'])
	released_fund.default_liquidator = request.POST['released_fund_liquidator']
	released_fund.default_liquidator_name = request.POST['released_fund_liquidator_name']
	#Assumption: Released Amount is always >= Returned Amount
	# released_fund.amount is actual amount decreased from budget
	released_fund.amount = float(released_fund.released_amount - released_fund.returned_amount)
	released_fund.remarks = request.POST['released_fund_remarks']
	released_fund.notes = request.POST['released_fund_notes']
	released_fund.variance = float(released_fund.amount - released_fund.actual_expense)
	edit_status = int(request.POST['released_fund_status'])
	#if status is set to closed, check if actual expense == released fund. set closing date 
	just_closed = False
	if released_fund.status == 0 and edit_status == 1:
		if abs(round(released_fund.variance, 2)) > 0.01:
			# Return variance error if expense amount and released amount have variance
			return HttpResponse('variance')
		else:
			logging.info(datetime.datetime.now())
			today = datetime.datetime.now()
			date_closed = str(today.year) + '-' + str(today.month) + '-' + str(today.day)
			released_fund.date_closed = datetime.datetime.strptime(str(date_closed), "%Y-%m-%d")
			just_closed = True
	released_fund.status = edit_status;
	released_fund.point_person = request.POST['released_fund_point_person']
	released_fund.point_person_name = request.POST['released_fund_point_person_name']
	released_fund.check_no = request.POST['released_fund_check_no']
	released_fund.check_credit_id = request.POST['released_fund_check']
	released_fund.check_vendor_id = request.POST['released_fund_check_vendor']
	released_fund.payment_method = payment_method
	ndb_helper.update_meta_data(released_fund, False)
	released_fund.put()
	
	# If check, update check statuses
	if payment_method == Payment_Method_Check:
		new_credit_id = request.POST['released_fund_check']
		if old_credit_id != new_credit_id:
			old_check_key = ndb.Key(urlsafe=old_credit_id)
			old_check = old_check_key.get()
			old_check.check_status = Check_Status_Ready
			old_check.check_rel_fund = None
			old_check.put()
			
			new_check_key = ndb.Key(urlsafe=new_credit_id)
			new_check = new_check_key.get()
			new_check.check_status = Check_Status_Released
			new_check.check_rel_fund = released_fund_id.urlsafe()
			new_check.put()
	elif payment_method == Payment_Method_Cash:
		# If payment method is previously for check, update check status and old_amount (old amount should be 0 since no funds is released in reality)
		if old_payment_method == Payment_Method_Check:
			# Set previous check status to Ready
			old_check_key = ndb.Key(urlsafe=old_credit_id)
			old_check = old_check_key.get()
			old_check.check_status = Check_Status_Ready
			old_check.check_rel_fund = None
			old_check.put()
			# Adjust old amount if pay method is previously a check. old amount should be 0 since no funds is released in reality
			old_amount = 0
		# Update affected budget's amount
		# Get budget
		budget_id = released_fund.source_id
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		if old_amount < released_fund.amount:
			offset = float(released_fund.amount - old_amount)
			budget.remaining_amount -= offset
		elif old_amount > released_fund.amount:
			offset = float(old_amount - released_fund.amount)
			budget.remaining_amount += offset
		budget.put()

		# Update budget holder's credit on hand (still with old holder)
		# Get budget holder
		from_employee_key = ndb.Key(urlsafe=budget.holder)
		from_employee = from_employee_key.get()
		if old_amount < released_fund.amount:
			offset = float(released_fund.amount - old_amount)
			from_employee.cash_on_hand -= offset
		elif old_amount > released_fund.amount:
			offset = float(old_amount - released_fund.amount)
			from_employee.cash_on_hand += offset
		from_employee.put()

		# Update point person's funds on hand (still with old point person)
		to_employee_key = ndb.Key(urlsafe=old_point_person)
		to_employee = to_employee_key.get()
		if old_amount < released_fund.amount:
			offset = float(released_fund.amount - old_amount)
			to_employee.released_funds_on_hand = float(to_employee.released_funds_on_hand + offset)
		elif old_amount > released_fund.amount:
			offset = float(old_amount - released_fund.amount)
			to_employee.released_funds_on_hand = to_employee.released_funds_on_hand - offset
		to_employee.put()
		
		# Transfer funds from old to new point person (if changed)
		if old_point_person != released_fund.point_person:
			# Transfer funds from old point person to next point person
			# Get old point person
			old_employee_key = ndb.Key(urlsafe=old_point_person)
			old_employee = old_employee_key.get()
			# Get amount to be transferred
			transfer_amount = released_fund.amount - released_fund.actual_expense
			# Subtract transfer amount from old point person
			old_employee.released_funds_on_hand -= transfer_amount
			old_employee.put()
			# Get new point person
			new_employee_key = ndb.Key(urlsafe=released_fund.point_person)
			new_employee = new_employee_key.get()
			# Add transfer amount to new point person
			new_employee.released_funds_on_hand += transfer_amount
			new_employee.put()

	# Notify newly liquidated funds DISABLED!
	# if just_closed:
	# 	notify_newly_closed_fund(released_fund)
	return HttpResponse('ok')

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_release_fund_expenses_post(request, released_fund_id, rel_fund_expenses):
	#Get released fund
	released_fund_id = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_id.get()

	# Update remarks and status of expenses under this released fund
	for item in rel_fund_expenses:
		expense_key = ndb.Key(urlsafe=item['key'])
		expense = expense_key.get()
		expense.released_fund_remarks = released_fund.remarks
		expense.released_fund_status = released_fund.status
		expense.put()

	return HttpResponse('ok')	

@ensure_csrf_cookie
def list_released_fund(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	#Todo Pagination
	if request.method == 'GET':
    
		# Get Budget Entity
		budget_id = budget_id.split("/", 1)[0] # remove other params after budget ID (ex. paged, page)
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()

		# Get Employee Entity of Budget Holder 
		employee_key = ndb.Key(urlsafe=budget.holder)
		employee_holder = employee_key.get()
		# Check if user is manager of account
		user_email = user_helper.get_current_user_email_address().email()
		if user_email != employee_holder.email and user_helper.get_user_access_type() == 2:
			return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

		budget_key = budget.key.urlsafe()
		budget_created_date = ndb_helper.convertToCurrentTZ(budget.created_date)
		budget_modified_date = ndb_helper.convertToCurrentTZ(budget.modified_date)
		user_access_type = user_helper.get_user_access_type()
		
		# Get date today
		now = date.today()
		date_year = now.year

		year_start = datetime.datetime(int(date_year),1,1)
		year_end = datetime.datetime(int(date_year),12,31)
		
		# query released fund
		query = ReleasedFund.query(ndb.AND(ReleasedFund.source_id == budget_id))
		query = query.filter(ndb.AND(ReleasedFund.date_released >= year_start, ReleasedFund.date_released <= year_end))
		released_funds_objects = query.order().fetch()
		released_funds_dict = ndb_helper.filter_results(released_funds_objects)

		# query funds
		query = Fund.query()
		query = Fund.query(ndb.AND(Fund.date_funded >= year_start, Fund.date_funded <= year_end, Fund.target_id == budget_id))

		fund_objects = query.order(-Fund.date_funded).fetch()
		funds_dict = ndb_helper.filter_results(fund_objects)
		
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			"released_funds": released_funds_dict,
			"funds": funds_dict,
			"budget": budget,
			"budget_key" : budget_key,
			"budget_created_date": budget_created_date,
			'budget_modified_date': budget_modified_date,
			'user_access_type': user_access_type,
			'date_year': date_year,
			'base_template': base_template
		}
		
		return render(request, 'budget/viewBudgetReleasedFunds.html', context)

@ensure_csrf_cookie
def list_expenses_report(request):
		# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Get date today
		now = date.today()
		date_month = now.month
		date_year = now.year

		month_year = datetime.datetime(int(date_year),int(date_month),1)
		days = monthrange(int(date_year), int(date_month))
		days_ = days[1]
		# Retrieve actual expenses. Do not included advances of employees
		query = Expense.query(Expense.charged_to_id == "N/A")
		query = query.filter(ndb.AND(Expense.date >= month_year, Expense.date <= month_year + datetime.timedelta(days=days_-1), Expense.active_status == Generic_Status_Active))
		expense_objects = query.order().fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'budget/viewReportExpenses.html', {"expenses": expenses_dict, 'date_year': date_year, 'date_month': date_month, 'base_template': base_template})

def report_filter_expense(request, is_csv=False):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_year = request.GET['expense_year_filter']
		date_month = request.GET['expense_month_filter']

		# Retrieve actual expenses. Do not included advances of employees
		query = Expense.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = query.filter(ndb.AND(Expense.date >= month_year, Expense.date <= month_year + datetime.timedelta(days=days_-1), Expense.active_status == Generic_Status_Active))
		elif date_year != 'All' and date_month == 'All':
			logging.info('check')
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = query.filter(ndb.AND(Expense.date >= month_year_start, Expense.date <= month_year_end, Expense.active_status == Generic_Status_Active))
		
		expense_objects = query.order().fetch()
		if is_csv:
			return expense_objects
		expenses_dict = ndb_helper.filter_results(expense_objects)
		base_template = user_helper.TEMPLATE_BLANK
		
		return render(request, 'budget/viewReportExpenses.html', {"expenses": expenses_dict, "date_year": date_year, "date_month": date_month, 'base_template': base_template})

@ensure_csrf_cookie
def list_expense(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Get date today
		now = date.today()
		date_month = now.month
		date_year = now.year

		month_year = datetime.datetime(int(date_year),int(date_month),1)
		days = monthrange(int(date_year), int(date_month))
		days_ = days[1]
		# Retrieve actual expenses. Do not included advances of employees
		query = Expense.query(Expense.charged_to_id == "N/A")
		query = query.filter(ndb.AND(Expense.date >= month_year, Expense.date <= month_year + datetime.timedelta(days=days_-1), Expense.active_status == Generic_Status_Active))
		expense_objects = query.order().fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'budget/viewExpenses.html', {"expenses": expenses_dict, 'date_year': date_year, 'date_month': date_month, 'base_template': base_template})

@ensure_csrf_cookie
def list_cash_advances(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_objects = Employee.query().order(Employee.first_name).fetch()
		employees_dict = ndb_helper.filter_results(employee_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'budget/viewCashAdvances.html', {'employees':employees_dict, 'base_template': base_template})

@ensure_csrf_cookie
def list_employee_cash_advances(request, employee_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_key = ndb.Key(urlsafe=employee_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		# Retrieve charges
		expense_objects = Expense.query(Expense.charged_to_id == employee_key).order(-Expense.date).fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)
		# Retrieve settlements
		settlement_objects = Settlement.query(Settlement.credit_to_id == employee_key).order(-Settlement.date).fetch()
		settlement_dict = ndb_helper.filter_results(settlement_objects)
		# Retrieve Source
		source = request.GET.get('source')
		
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'budget/viewCashAdvancesEmployee.html', {"employee" : employee, "employee_key" : employee_key, "expenses": expenses_dict, "settlements": settlement_dict, 'source': source, 'base_template': base_template})

@ensure_csrf_cookie
def list_expense_details(request, expense_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		expense_key = ndb.Key(urlsafe=expense_id)
		expense = expense_key.get()
		expense_key = expense.key.urlsafe()
		expense_created_date = ndb_helper.convertToCurrentTZ(expense.created_date)
		expense_modified_date = ndb_helper.convertToCurrentTZ(expense.modified_date)
		expense_items = getExpenseItems(expense_id)
		released_fund_key = ndb.Key(urlsafe=expense.released_fund_id)
		released_fund = released_fund_key.get()
		released_fund_key = released_fund_key.urlsafe()
		source = request.GET.get('source')
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'source' : source,
			'expense' : expense,
			'released_fund': released_fund,
			'released_fund_key': released_fund_key,
			'expense_date' : str(expense.date),
			'expense_key' : expense_id,
			'transaction_type' : expense.transaction_type,
			'expense_type' : expense.expense_type,
			'total_amount' : expense.total_amount,
			'vendor_name' : expense.vendor_name,
			'check_no' : expense.check_no,
			'OR_number' : expense.OR_number,
			#'expense_items_key' : expense_items.expense_id,
			#'expense_items_qty' : expense_items.qty,
			#'expense_items_units' : expense_items.units,
			#'expense_items_name' : expense_items.item_name,
			#'expense_items_witholding_tax' : expense_items.withholding_tax,
			#'expense_items_input_tax' : expense_items.input_tax,
			#'expense_items_amount' : expense_items.amount
			'expense_items' : expense_items,
			'expense_created_date': expense_created_date,
			'expense_modified_date': expense_modified_date,
			'base_template': base_template
		}
		return render(request, 'budget/viewExpenseDetails.html', context, content_type="text/html")

@ensure_csrf_cookie
def list_charge_details(request, charge_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		expense_key = ndb.Key(urlsafe=charge_id)
		expense = expense_key.get()
		charge_key = expense.key.urlsafe()
		charge_items = getExpenseItems(charge_key)
		charge_created_date = ndb_helper.convertToCurrentTZ(expense.created_date)
		charge_modified_date = ndb_helper.convertToCurrentTZ(expense.modified_date)
		employee_key = ndb.Key(urlsafe=expense.charged_to_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'expense' : expense,
			'expense_date' : str(expense.date),
			'expense_key' : charge_id,
			'expense_items': charge_items,
			'employee': employee,
			'employee_key': employee_key,
			'expense_created_date': charge_created_date,
			'expense_modified_date': charge_modified_date,
			'base_template': base_template
		}
		return render(request, 'budget/viewChargeDetails.html', context, content_type="text/html")

@ensure_csrf_cookie
def list_settlement_details(request, settlement_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		settlement_key = ndb.Key(urlsafe=settlement_id)
		settlement = settlement_key.get()
		settlement_key = settlement.key.urlsafe()
		settlement_created_date = ndb_helper.convertToCurrentTZ(settlement.created_date)
		settlement_modified_date = ndb_helper.convertToCurrentTZ(settlement.modified_date)
		employee_key = ndb.Key(urlsafe=settlement.credit_to_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'settlement' : settlement,
			'settlement_date' : str(settlement.date),
			'settlement_key' : settlement_id,
			'employee': employee,
			'employee_key': employee_key,
			'settlement_created_date': settlement_created_date,
			'settlement_modified_date': settlement_modified_date,
			'base_template': base_template
		}
		return render(request, 'budget/viewSettlementDetails.html', context, content_type="text/html")

def filter_expense(request, is_csv=False):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_year = request.GET['expense_year_filter']
		date_month = request.GET['expense_month_filter']

		# Retrieve actual expenses. Do not included advances of employees
		query = Expense.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = query.filter(ndb.AND(Expense.date >= month_year, Expense.date <= month_year + datetime.timedelta(days=days_-1), Expense.active_status == Generic_Status_Active))
		elif date_year != 'All' and date_month == 'All':
			logging.info('check')
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = query.filter(ndb.AND(Expense.date >= month_year_start, Expense.date <= month_year_end, Expense.active_status == Generic_Status_Active))
		
		expense_objects = query.order().fetch()
		if is_csv:
			return expense_objects
		expenses_dict = ndb_helper.filter_results(expense_objects)
		base_template = user_helper.TEMPLATE_BLANK
		
		return render(request, 'budget/viewExpenses.html', {"expenses": expenses_dict, "date_year": date_year, "date_month": date_month, 'base_template': base_template})
def filter_check(request, is_csv=False):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_year = request.GET['check_year_filter']
		date_month = request.GET['check_month_filter']

		# Retrieve Checks
		query = Credit.query(Credit.payment_method == Payment_Method_Check)
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = query.filter(ndb.AND(Credit.date_issued >= month_year, Credit.date_issued <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = query.filter(ndb.AND(Credit.date_issued >= month_year_start, Credit.date_issued <= month_year_end))

		checks_objects = query.order().fetch()
		if is_csv:
			return checks_objects
		checks_dict = ndb_helper.filter_results(checks_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.TEMPLATE_BLANK
		context = {
			"credits": checks_dict,
			"date_year": date_year,
			"date_month": date_month,
			"user_access_type": user_access_type,
			'base_template': base_template
		}
		return render(request, 'budget/viewCheck.html', context)

def filter_cash(request, is_csv=False):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_year = request.GET['cash_year_filter']
		date_month = request.GET['cash_month_filter']

		# Retrieve Cash
		query = Credit.query(Credit.payment_method == Payment_Method_Cash)
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = query.filter(ndb.AND(Credit.date_issued >= month_year, Credit.date_issued <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = query.filter(ndb.AND(Credit.date_issued >= month_year_start, Credit.date_issued <= month_year_end))

		cash_objects = query.order().fetch()
		if is_csv:
			return cash_objects
		cash_dict = ndb_helper.filter_results(cash_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.TEMPLATE_BLANK
		context = {
			"credits": cash_dict,
			"date_year": date_year,
			"date_month": date_month,
			"user_access_type": user_access_type,
			'base_template': base_template
		}
		return render(request, 'budget/viewCash.html', context)

def filter_cashadvance(request, is_csv=False):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_year = request.GET['expense_year_filter']
		date_month = request.GET['expense_month_filter']
		charged_to_id = request.GET['expense_charged_to_filter']
		apv_number = request.GET['expense_number_filter']
		status = request.GET['expense_status_filter']
		isChargedToFiltered = False

		query = Expense.query()
		if date_year != 'All' and date_month != 'All':
			month_year = datetime.datetime(int(date_year),int(date_month),1)
			days = monthrange(int(date_year), int(date_month))
			days_ = days[1]
			query = Expense.query(ndb.AND(Expense.date >= month_year, Expense.date <= month_year + datetime.timedelta(days=days_-1)))
		elif date_year != 'All' and date_month == 'All':
			logging.info('check')
			month_year_start = datetime.datetime(int(date_year),1,1)
			month_year_end = datetime.datetime(int(date_year),12,31)
			query = Expense.query(ndb.AND(Expense.date >= month_year_start, Expense.date <= month_year_end))

		if charged_to_id != 'All':
			query = query.filter(Expense.charged_to_id == charged_to_id)
			isChargedToFiltered = True
		else:
			query = Expense.query(Expense.charged_to_id != "N/A")

		if status != 'All':
			query = query.filter(Expense.status == int(status))

		if apv_number != '':
			query = query.filter(Expense.apv_number == apv_number)
		
		if not isChargedToFiltered:
			expense_objects = query.order(Expense.charged_to_id, -Expense.date).fetch()
		else:
			expense_objects = query.order(-Expense.date).fetch()

		if is_csv:
			return expense_objects
		expenses_dict = ndb_helper.filter_results(expense_objects)
		employees = getEmployees()
		base_template = user_helper.TEMPLATE_BLANK
		paginator = Paginator(expenses_dict, 10000)
		page = request.GET.get('page')
		paged = request.GET.get('paged')
		budgets = list_budget_expense(request)

		try:
		    expenses_dict = paginator.page(page)
		except PageNotAnInteger:
		    # If page is not an integer, deliver first page.
		    expenses_dict = paginator.page(1)
		except EmptyPage:
		    # If page is out of range (e.g. 9999), deliver last page of results.
		    expenses_dict = paginator.page(paginator.num_pages)
		return render(request, 'budget/viewCashAdvances.html', {"expenses": expenses_dict, "budgets": budgets, "date_year": date_year, "date_month": date_month, "charged_to":charged_to_id, 'apv_number': apv_number, 'status': status, 'employees':employees, 'base_template': base_template})

@ensure_csrf_cookie
def filter_budget_details(request, budget_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	#Todo Pagination
	if request.method == 'GET':
		budget_id = budget_id.split("/", 1)[0] # remove other params after budget ID (ex. paged, page)
		budget_key = ndb.Key(urlsafe=budget_id)
		budget = budget_key.get()
		budget_key = budget.key.urlsafe()
		budget_created_date = ndb_helper.convertToCurrentTZ(budget.created_date)
		budget_modified_date = ndb_helper.convertToCurrentTZ(budget.modified_date)
		user_access_type = user_helper.get_user_access_type()
		
		# Get date today
		now = date.today()
		date_year = request.GET['budget_details_year_filter']

		year_start = datetime.datetime(int(date_year),1,1)
		year_end = datetime.datetime(int(date_year),12,31)
		
		#query released funds
		query = ReleasedFund.query(ndb.AND(ReleasedFund.source_id == budget_id))
		query = query.filter(ndb.AND(ReleasedFund.date_released >= year_start, ReleasedFund.date_released <= year_end))
		released_funds_objects = query.order().fetch()
		released_funds_dict = ndb_helper.filter_results(released_funds_objects)

		# query funds
		query = Fund.query()
		query = Fund.query(ndb.AND(Fund.date_funded >= year_start, Fund.date_funded <= year_end, Fund.target_id == budget_id))

		fund_objects = query.order(-Fund.date_funded).fetch()
		funds_dict = ndb_helper.filter_results(fund_objects)
		
		#base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		base_template = user_helper.TEMPLATE_BLANK
		context = {
			"released_funds": released_funds_dict,
			"funds": funds_dict,
			"budget": budget,
			"budget_key" : budget_key,
			"budget_created_date": budget_created_date,
			'budget_modified_date': budget_modified_date,
			'user_access_type': user_access_type,
			'date_year': date_year,
			'base_template': base_template
		}
		
		return render(request, 'budget/viewBudgetReleasedFunds.html', context)

def search_data(request):
	if request.method == 'GET':
	    base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		
		# Get search item
	    search_data = request.GET.get('search_data')
		
		# Check search type
	    if search_data[0] == "A":
	    	# Search Type is for Expense
	    	search_type = Search_Type_Expense
	    else:
	    	# Search Type is for Released Fund
	    	search_type = Search_Type_Released_Fund
	    
	    # Search Expense
	    if search_type == Search_Type_Expense:
	    	expense_object = Expense.query(Expense.apv_number == search_data).get()
	    	if expense_object:
				expense_key = expense_object.key
				expense_id = expense_object.key.urlsafe()
				expense = expense_key.get()
				expense_key = expense.key.urlsafe()
				expense_created_date = ndb_helper.convertToCurrentTZ(expense.created_date)
				expense_modified_date = ndb_helper.convertToCurrentTZ(expense.modified_date)
				expense_items = getExpenseItems(expense_id)
				
				# get released fund if not N/A
				if expense.released_fund_id != "N/A":
					released_fund_key = ndb.Key(urlsafe=expense.released_fund_id)
					released_fund = released_fund_key.get()
					released_fund_key = released_fund_key.urlsafe()
				else:
					released_fund = "N/A"
					released_fund_key = "N/A"
				# retrieved charge to employee if not N/A
				if expense.charged_to_id != "N/A":
					employee_key = ndb.Key(urlsafe=expense.charged_to_id)
					employee = employee_key.get() 
					employee_key = employee_key.urlsafe()
				else:
					employee = "N/A"
					employee_key = "N/A"
				
				base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
				context = {
					'expense' : expense,
					'released_fund': released_fund,
					'released_fund_key': released_fund_key,
					'expense_date' : str(expense.date),
					'expense_key' : expense_id,
					'transaction_type' : expense.transaction_type,
					'expense_type' : expense.expense_type,
					'total_amount' : expense.total_amount,
					'vendor_name' : expense.vendor_name,
					'check_no' : expense.check_no,
					'OR_number' : expense.OR_number,
					'expense_items' : expense_items,
					'expense_created_date': expense_created_date,
					'expense_modified_date': expense_modified_date,
					'base_template': base_template,
					'search_data': search_data,
					'employee': employee,
					'employee_key': employee_key
				}
				page = 'budget/viewExpenseDetails.html'
				
				# Use Charge Details Page if this expense is a billed charge
				if expense.is_charge_billed:
					page = 'budget/viewChargeDetails.html'
				return render(request, page, context, content_type="text/html")
	    	else:
	    		return render(request, 'budget/itemNotFound.html', {'base_template': base_template,'search_data': search_data}, content_type="text/html")
	    # Search Released Fund
	    elif search_type == Search_Type_Released_Fund:
	    	released_fund_object = ReleasedFund.query(ReleasedFund.fund_number == search_data).get()
	    	if released_fund_object:
	    		released_fund_key = released_fund_object.key
	    		released_fund_id = released_fund_object.key.urlsafe()
	    		released_fund = released_fund_key.get()
	    		released_fund_key = released_fund.key.urlsafe()
	    		expense_objects = Expense.query(Expense.released_fund_id == released_fund_id).order(-Expense.date).fetch()
	    		expenses_dict = ndb_helper.filter_results(expense_objects)
	    		relfund_created_date = ndb_helper.convertToCurrentTZ(released_fund.created_date)
	    		relfund_modified_date = ndb_helper.convertToCurrentTZ(released_fund.modified_date)
	    		user_email = user_helper.get_current_user_email_address().email()
	    		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
	    		paginator = Paginator(expenses_dict, 20)
	    		page = request.GET.get('page')
	    		paged = request.GET.get('paged')
	    		try:
	    			expenses_dict = paginator.page(page)
	    		except PageNotAnInteger:
	    			# If page is not an integer, deliverfirst page.
	    			expenses_dict = paginator.page(1)
	    		except EmptyPage:
	    			# If page is out of range (e.g. 9999), deliver last page of results.
	    			expenses_dict = paginator.page(paginator.num_pages)
	    		if paged:
	    			base_template = user_helper.TEMPLATE_BLANK
	    		return render(request, 'budget/viewReleasedFundExpenses.html', {"expenses": expenses_dict, "released_fund":released_fund, "released_fund_key":released_fund_key, 'relfund_created_date': relfund_created_date, 'relfund_modified_date': relfund_modified_date, 'user_email': user_email, 'base_template': base_template,'search_data': search_data})

	    	else:
	    		return render(request, 'budget/itemNotFound.html', {'base_template': base_template,'search_data': search_data}, content_type="text/html")
	    else:
	    	return render(request, 'budget/itemNotFound.html', {'base_template': base_template,'search_data': search_data}, content_type="text/html")
	return render(request, 'budget/itemNotFound.html', {'base_template': base_template,'search_data': search_data}, content_type="text/html")

def report_expense(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	out = StringIO.StringIO()
	fields = ['Date', 'APV Number', 'Paid Amount', 'Gross Amount', 'Check No', 'Total Withholding Tax',
		'Total Input Tax', 'Transaction Type', 'OR Number', 'Vendor Name', 'Vendor TIN', 
		'Vendor Address', 'Vendor Is VAT Registered?', 'Budget Name', 'Department', 'Expense Type', 'Waybill Number', 'Invoice Number', 'Particulars', 'Purpose', 'Charged To', 'Status', 'Notes', 'Created By', 'Creation Date','Modified By','Modified Date']
	writer = csv.DictWriter(out, fieldnames=fields)
	writer.writeheader()
	items = filter_expense(request, True)
	for item in items:
		expense_name = getExpenseTypeName(item.expense_type)
		if item.vendor_is_vat_registered == True:
			is_vat_reg = "Yes"
		else:
			is_vat_reg = "No"
		if item.transaction_type == 0:
			trans_type = "Goods"
		elif item.transaction_type == 1:
			trans_type = "Services"
		elif item.transaction_type == -1:
			trans_type = "Unclassified"
		else:
			trans_type = "?"
		
		if item.department == 0:
			department = "Unspecified"
		elif item.department == 1:
			department = "General Admin"
		elif item.department == 2:
			department = "Sales"
		elif item.department == 3:
			department = "Marketing"
		elif item.department == 4:
			department = "R&D"
		elif item.department == 5:
			department = "Storage"
		else:
			department = "?"

		if item.active_status == 0:
			active_status = "Active"
		elif item.active_status == 1:
			active_status = "Cancelled"
		else:
			active_status = "?"

		data = {
			'Date': item.date,
			'APV Number': item.apv_number,
			'Paid Amount': item.total_amount,
			'Gross Amount': item.gross_amount,
			'Check No': encodeStringToUTF(item.check_no),
			'Total Withholding Tax': item.total_withholding_tax,
			'Total Input Tax': item.total_input_tax,
			'Transaction Type': trans_type,
			'OR Number': encodeStringToUTF(item.OR_number),
			'Vendor Name': encodeStringToUTF(item.vendor_name),
			'Vendor TIN': encodeStringToUTF(item.vendor_tin),
			'Vendor Address': encodeStringToUTF(item.vendor_address),
			'Vendor Is VAT Registered?': is_vat_reg,
			'Budget Name': encodeStringToUTF(item.budget_name),
			'Department': encodeStringToUTF(department),
			'Expense Type': encodeStringToUTF(expense_name),
			'Particulars': encodeStringToUTF(item.particulars),
			'Purpose': encodeStringToUTF(item.released_fund_remarks),
			'Charged To': encodeStringToUTF(item.charged_to_name),
			'Status': active_status,
			'Notes': encodeStringToUTF(item.notes),
			'Created By': encodeStringToUTF(item.created_by),
			'Creation Date': str(item.created_date),
			'Modified By': encodeStringToUTF(item.modified_by),
			'Modified Date': str(item.modified_date)
		}
		writer.writerow(data)
	response = HttpResponse(content_type='text/csv')
	response['Content-Transfer-Encoding'] = 'Binary'
	response['Content-disposition'] = 'attachment; filename="SHEMICA EXPENSES-%s.csv"' % datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S")
	response.write(out.getvalue())
	logging.info(out.getvalue())
	out.close()

	return response

def encodeStringToUTF(string):
	if string is None:
		return None
	return string.encode('utf-8').strip().rstrip().replace("\n", " ")

def getExpenseTypeName(expense_type):
	expense_name = ""
	if expense_type == 0:
		expense_name = "13th Month Pay"
	elif expense_type == 1:
		expense_name = "Brokerage Fee"
	elif expense_type == 2:
		expense_name = "Business Registration"
	elif expense_type == 3:
		expense_name = "Cash Advance"
	elif expense_type == 4:
		expense_name = "Communication"
	elif expense_type == 5:
		expense_name = "Construction"
	elif expense_type == 6:
		expense_name = "Listing Fee"
	elif expense_type == 7:
		expense_name = "Donation"
	elif expense_type == 8:
		expense_name = "Gas, Fuel and Oil"
	elif expense_type == 9:
		expense_name = "Import Fees"
	elif expense_type == 10:
		expense_name = "Income Tax"
	elif expense_type == 11:
		expense_name = "Insurance"
	elif expense_type == 12:
		expense_name = "Inventory - Goods"
	elif expense_type == 13:
		expense_name = "Machineries & Equipment"
	elif expense_type == 14:
		expense_name = "Marketing"
	elif expense_type == 15:
		expense_name = "Meals"
	elif expense_type == 16:
		expense_name = "Meeting, Seminars and Teambuilding"
	elif expense_type == 17:
		expense_name = "Minor Properties"
	elif expense_type == 18:
		expense_name = "Miscellaneous"
	elif expense_type == 19:
		expense_name = "Office Supplies"
	elif expense_type == 20:
		expense_name = "Other Current Assets"
	elif expense_type == 21:
		expense_name = "Other Employee Benefits"
	elif expense_type == 22:
		expense_name = "Pag-ibig EE"
	elif expense_type == 23:
		expense_name = "Pag-ibig ER"
	elif expense_type == 24:
		expense_name = "Pantry Supplies"
	elif expense_type == 25:
		expense_name = "Philhealth EE"
	elif expense_type == 26:
		expense_name = "Philhealth ER"
	elif expense_type == 27:
		expense_name = "Postage and Courier Charges"
	elif expense_type == 28:
		expense_name = "Professional Fees"
	elif expense_type == 29:
		expense_name = "Rent"
	elif expense_type == 30:
		expense_name = "Repairs & Maintenance"
	elif expense_type == 31:
		expense_name = "Salaries & Wages"
	elif expense_type == 32:
		expense_name = "Security Services"
	elif expense_type == 33:
		expense_name = "SSS EE"
	elif expense_type == 34:
		expense_name = "SSS ER"
	elif expense_type == 35:
		expense_name = "Taxes & Licences"
	elif expense_type == 36:
		expense_name = "Transportation Equipment"
	elif expense_type == 37:
		expense_name = "Transportation, Travel, Parking & Toll"
	elif expense_type == 38:
		expense_name = "Utilities"
	elif expense_type == 39:
		expense_name = "VAT"
	elif expense_type == 40:
		expense_name = "Warehouse Equipment"
	elif expense_type == 41:
		expense_name = "Warehouse Supplies"
	elif expense_type == 42:
		expense_name = "Withholding - compensation"
	elif expense_type == 43:
		expense_name = "Withholding - expanded"
	elif expense_type == 44:
		expense_name = "Loan Interest"
	elif expense_type == 45:
		expense_name = "Office Equipment"
	elif expense_type == 46:
		expense_name = "Bank Charges"
	elif expense_type == 47:
		expense_name = "Shipment and Forwarding Fees"
	elif expense_type == 48:
		expense_name = "IT Fees"
	elif expense_type == 49:
		expense_name = "Custom Tax"
	elif expense_type == 50:
		expense_name = "Marketing Supplies"
	elif expense_type == 51:
		expense_name = "Research & Development"
	elif expense_type == 52:
		expense_name = "Loan Payment"
	elif expense_type == 53:
		expense_name = "Marketing - Key Visual"
	elif expense_type == 54:
		expense_name = "Marketing - Merchandising"
	elif expense_type == 55:
		expense_name = "Marketing - Merchandising (Payola)"
	elif expense_type == 56:
		expense_name = "Marketing - Digital"
	elif expense_type == 57:
		expense_name = "Marketing - Outlet Support"
	elif expense_type == 58:
		expense_name = "Logistics - Delivery"
	elif expense_type == 59:
		expense_name = "Non-recurring Professional Fees"
	elif expense_type == 60:
		expense_name = "Logistics - Transfer"
	elif expense_type == 61:
		expense_name = "Final Pay"
	elif expense_type == -1:
		expense_name = "Unclassified"
	else:
		expense_name = "?"
	return expense_name

def expense_upload(request):
	image = request.FILES['image']
	blob = BlobStorage()
	obj = image.file.getvalue()
	img = images.Image(obj)
	img.resize(width=500, height=700)
	img.im_feeling_lucky()
	thumbnail = img.execute_transforms(output_encoding=images.JPEG)
	blob.obj = thumbnail
	if blob.obj:
		blob.put()
		return HttpResponse(blob.key.urlsafe())
	return HttpResponse('error')

def get_expense_image(request, blob_id):
	blob_key = ndb.Key(urlsafe=blob_id)
	blob = blob_key.get()
	
	return HttpResponse(blob.obj, mimetype="image/jpg")

@ensure_csrf_cookie
def list_released_funds(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Get date today
		now = date.today()
		date_month = now.month
		date_year = now.year
		
		month_year = datetime.datetime(int(date_year),int(date_month),1)
		days = monthrange(int(date_year), int(date_month))
		days_ = days[1]
		
		query = ReleasedFund.query(ndb.AND(ReleasedFund.date_released >= month_year, ReleasedFund.date_released <= month_year + datetime.timedelta(days=days_-1)))
		released_fund_objects = query.order(-ReleasedFund.date_released).fetch()
		released_funds_dict = ndb_helper.filter_results(released_fund_objects)
		
		budgets = list_budget_expense(request)
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			"released_funds": released_funds_dict,
			'employees': employees,
			'budgets': budgets,
			'date_month': date_month,
			'date_year': date_year,
			'base_template': base_template
		}

		return render(request, 'budget/viewReleasedFunds.html', context)


@ensure_csrf_cookie
def create_released_fund_expense(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	# Check if expense submitter is same as released fund creator and not an admin
	released_fund_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_key.get()
	user_email = user_helper.get_current_user_email_address().email()
	user_access_type = user_helper.get_user_access_type()
	if str(user_email) == str(released_fund.created_by) and user_access_type != 1:
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		released_fund_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_fund_key.get()
		clients = getClients()
		vendors = getVendors()
		employees = getEmployees()
		# Retrieve Source
		expense_type = request.GET.get('expense_type')
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'released_fund' : released_fund,
			'released_fund_key' : released_fund_id,
			'released_fund_date_released' : released_fund.date_released.strftime("%Y-%m-%d"),
			'clients' : clients,
			'vendors' : vendors,
			'employees': employees,
			'expense_type': expense_type,
			'base_template': base_template
		}
		return render(request, 'budget/addExpense.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_expense_post(request)		
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_expense_post(request):
	# Create expense
	expense = Expense()
	date = request.POST['expense_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	expense.date = date_
	apv_id = IdCounter.record('apv_number')
	current_date = datetime.datetime.now()
	current_year = current_date.year
	current_month = current_date.month
	expense.apv_number = 'A' + str('%04d' % current_year) + '' + str('%02d' % current_month) + '' + str(apv_id)
	expense.total_amount = float(request.POST['total_amount'])
	expense.check_no = request.POST['check_no']
	expense.total_withholding_tax = float(request.POST['total_withholding_tax'])
	expense.total_input_tax = float(request.POST['total_input_tax'])
	expense.transaction_type = int(request.POST['transaction_type'])
	expense.receipt = request.POST['receipt']
	expense.OR_number = request.POST['OR_number']
	expense.vendor_id = request.POST['vendor_id']
	expense.vendor_name = request.POST['vendor_name']
	expense.vendor_short_name = request.POST['vendor_short_name']
	expense.vendor_tin = request.POST['vendor_TIN']
	expense.vendor_address = request.POST['vendor_address']
	expense.vendor_is_vat_registered = bool(int(request.POST['vendor_is_vat_registered']))
	expense.budget_name = request.POST['budget_name']
	expense.budget_id = request.POST['budget_id']
	expense.expense_type = int(request.POST['expense_type'])
	expense.released_fund_id = request.POST['released_fund_id']
	expense.released_fund_remarks = request.POST['released_fund_remarks']
	expense.particulars = request.POST['particulars']
	expense.charged_to_id = request.POST['charged_to_id']
	expense.charged_to_name = request.POST['charged_to_name']
	expense.paid_amount = 0
	expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
	expense.charge_type = int(request.POST['charge_type'])
	expense.notes = request.POST['notes']
	payment_method = int(request.POST['payment_method'])
	expense.payment_method = payment_method
	expense.department = int(request.POST['department'])
	expense.gross_amount = float(request.POST['gross_amount'])

	# Add Expense
	ndb_helper.update_meta_data(expense, True)
	expense_key = expense.put()

	# Create Expense Items
	expense_items = simplejson.loads(request.POST['expense_items'])
	for item in expense_items:
		expense_item = ExpenseItem()
		expense_item.qty  = float(item["qty"])
		expense_item.units  = item["units"]
		expense_item.item_name  = item["item_name"]
		expense_item.withholding_tax  = float(item["withholding_tax"])
		expense_item.input_tax  = float(item["input_tax"])
		expense_item.amount  = float(item["expense_amount"])
		expense_item.expense_id = expense_key.urlsafe()
		ndb_helper.update_meta_data(expense_item, True)
		expense_item.put()

	# Update Released Funds Actual Expense and Variance
	released_fund_id = expense.released_fund_id
	released_funds_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_funds_key.get()
	released_fund.actual_expense = released_fund.actual_expense + expense.total_amount
	released_fund.variance = released_fund.amount - released_fund.actual_expense
	released_fund.put()
	
	# Update check info if payment_method is check
	if payment_method == Payment_Method_Check:
		released_fund_id = expense.released_fund_id
		released_funds_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_funds_key.get()
		credit_id = released_fund.check_credit_id
		credit_key = ndb.Key(urlsafe=credit_id)
		credit = credit_key.get()
		credit.check_status = Check_Status_Paid
		credit.check_expense = expense_key.urlsafe()
		credit.check_received_by = request.POST['check_received_by']
		credit.put()
	else:
		# Update point person's funds on hand if cash is spent
		point_person_key = ndb.Key(urlsafe=released_fund.point_person)
		point_person = point_person_key.get()
		point_person.released_funds_on_hand = point_person.released_funds_on_hand - expense.total_amount
		point_person.put()

	# If cash advance/charge
	if expense.charged_to_id != "N/A":
		charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
		charged_person = charged_person_key.get()
		charged_person.total_balance += expense.total_amount
		charged_person.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def create_multiple_expenses(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	# Check if expense submitter is same as released fund creator
	released_fund_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_key.get()
	user_email = user_helper.get_current_user_email_address().email()
	user_access_type = user_helper.get_user_access_type()
	if str(user_email) == str(released_fund.created_by) and user_access_type != 1:
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# retrieve released fund
		released_fund_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_fund_key.get()
		# retrieve vendors
		vendors = getVendors()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'released_fund' : released_fund,
			'released_fund_key' : released_fund_id,
			'released_fund_date_released' : released_fund.date_released.strftime("%Y-%m-%d"),
			'vendors' : vendors,
			'base_template': base_template
		}
		return render(request, 'budget/addMultipleExpenses.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		# retrieve released fund
		released_fund_id = request.POST['released_fund_id']
		released_funds_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_funds_key.get()
		response = create_multiple_expenses_post(request, released_fund)
		return response

@ensure_csrf_cookie
def create_multiple_expenses_single_or(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	# Check if expense submitter is same as released fund creator
	released_fund_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_key.get()
	user_email = user_helper.get_current_user_email_address().email()
	user_access_type = user_helper.get_user_access_type()
	if str(user_email) == str(released_fund.created_by) and user_access_type != 1:
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# retrieve released fund
		released_fund_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_fund_key.get()
		# retrieve vendors
		vendors = getVendors()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'released_fund' : released_fund,
			'released_fund_key' : released_fund_id,
			'released_fund_date_released' : released_fund.date_released.strftime("%Y-%m-%d"),
			'vendors' : vendors,
			'base_template': base_template
		}
		return render(request, 'budget/addMultipleExpensesSingleOR.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		# retrieve released fund
		released_fund_id = request.POST['released_fund_id']
		released_funds_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_funds_key.get()
		response = create_multiple_expenses_post(request, released_fund)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_multiple_expenses_post(request, released_fund):
	# Create Multiple Expenses
	multiple_expenses = simplejson.loads(request.POST['multiple_expenses_array'])
	for item in multiple_expenses:
		# Create expense
		expense = Expense()
		date = item['expense_date']
		date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
		expense.date = date_
		# Generate APV ID
		apv_id = IdCounter.record('apv_number')
		current_date = datetime.datetime.now()
		current_year = current_date.year
		current_month = current_date.month
		# Set values
		expense.apv_number = 'A' + str('%04d' % current_year) + '' + str('%02d' % current_month) + '' + str(apv_id)
		expense.total_amount = float(item['total_amount'])
		expense.check_no = item['check_no']
		expense.total_withholding_tax = float(item['total_withholding_tax'])
		expense.total_input_tax = float(item['total_input_tax'])
		expense.transaction_type = int(item['transaction_type'])
		expense.receipt = item['receipt']
		expense.OR_number = item['OR_number']
		expense.vendor_id = item['vendor_id']
		expense.vendor_name = item['vendor_name']
		expense.vendor_short_name = item['vendor_short_name']
		expense.vendor_tin = item['vendor_TIN']
		expense.vendor_address = item['vendor_address']
		expense.vendor_is_vat_registered = bool(int(item['vendor_is_vat_registered']))
		expense.budget_name = item['budget_name']
		expense.budget_id = item['budget_id']
		expense.expense_type = int(item['expense_type'])
		expense.released_fund_id = item['released_fund_id']
		expense.released_fund_remarks = released_fund.remarks
		expense.invoice_number = item['invoice_number']
		expense.particulars = item['particulars']
		expense.charged_to_id = item['charged_to_id']
		expense.charged_to_name = item['charged_to_name']
		expense.paid_amount = 0
		expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
		expense.status = int(item['charged_to_status'])
		expense.notes = item['notes']
		expense.department = int(item['department'])

		# Add Expense
		ndb_helper.update_meta_data(expense, True)
		expense_key = expense.put()

		# Create Expense Items
		expense_item = ExpenseItem()
		expense_item.qty  = float(item["qty"])
		expense_item.units  = item["units"]
		expense_item.item_name  = item["item_name"]
		expense_item.withholding_tax  = float(item["withholding_tax"])
		expense_item.input_tax  = float(item["input_tax"])
		expense_item.amount  = float(item["expense_amount"])
		expense_item.expense_id = expense_key.urlsafe()
		ndb_helper.update_meta_data(expense_item, True)
		expense_item.put()

		# Update Released Funds Actual Expense and Variance
		released_fund.actual_expense = released_fund.actual_expense + expense.total_amount
		released_fund.variance = released_fund.amount - released_fund.actual_expense
		released_fund.put()

		# Update point person's funds on hand
		point_person_key = ndb.Key(urlsafe=released_fund.point_person)
		point_person = point_person_key.get()
		point_person.released_funds_on_hand = point_person.released_funds_on_hand - expense.total_amount
		point_person.put()

	return HttpResponse('ok')

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_charge_post(request):
	# Create expense
	expense = Expense()
	date = request.POST['expense_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	expense.date = date_
	apv_id = IdCounter.record('apv_number')
	current_date = datetime.datetime.now()
	current_year = current_date.year
	current_month = current_date.month
	expense.apv_number = 'A' + str('%04d' % current_year) + '' + str('%02d' % current_month) + '' + str(apv_id)
	expense.total_amount = float(request.POST['total_amount'])
	expense.check_no = request.POST['check_no']
	expense.total_withholding_tax = float(request.POST['total_withholding_tax'])
	expense.total_input_tax = float(request.POST['total_input_tax'])
	expense.transaction_type = int(request.POST['transaction_type'])
	expense.receipt = request.POST['receipt']
	expense.OR_number = request.POST['OR_number']
	expense.vendor_id = request.POST['vendor_id']
	expense.vendor_name = request.POST['vendor_name']
	expense.vendor_short_name = request.POST['vendor_short_name']
	expense.vendor_tin = request.POST['vendor_TIN']
	expense.vendor_address = request.POST['vendor_address']
	expense.vendor_is_vat_registered = bool(int(request.POST['vendor_is_vat_registered']))
	expense.budget_name = request.POST['budget_name']
	expense.budget_id = request.POST['budget_id']
	expense.expense_type = int(request.POST['expense_type'])
	expense.released_fund_id = request.POST['released_fund_id']
	expense.released_fund_remarks = request.POST['released_fund_remarks']
	expense.particulars = request.POST['particulars']
	expense.charged_to_id = request.POST['charged_to_id']
	expense.charged_to_name = request.POST['charged_to_name']
	expense.paid_amount = 0
	expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
	expense.status = int(request.POST['status'])
	expense.notes = request.POST['notes']
	payment_method = int(request.POST['payment_method'])
	expense.payment_method = payment_method
	expense.department = int(request.POST['department'])
	expense.charge_type = int(request.POST['charge_type'])
	expense.is_charge_billed = bool(int(request.POST['is_charge_billed']))

	# Add Charge
	ndb_helper.update_meta_data(expense, True)
	expense_key = expense.put()

	# Create Charge Items
	expense_items = simplejson.loads(request.POST['expense_items'])
	for item in expense_items:
		expense_item = ExpenseItem()
		expense_item.qty  = float(item["qty"])
		expense_item.units  = item["units"]
		expense_item.item_name  = item["item_name"]
		expense_item.withholding_tax  = float(item["withholding_tax"])
		expense_item.input_tax  = float(item["input_tax"])
		expense_item.amount  = float(item["expense_amount"])
		expense_item.expense_id = expense_key.urlsafe()
		ndb_helper.update_meta_data(expense_item, True)
		expense_item.put()

	# Update Total Advances of affected employee
	charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
	charged_person = charged_person_key.get()
	charged_person.total_balance += expense.total_amount
	charged_person.put()

	return HttpResponse('ok')

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_cashadvance_charge_post(request):
	# Create expense
	expense = Expense()
	date = request.POST['expense_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	expense.date = date_
	apv_id = IdCounter.record('apv_number')
	current_date = datetime.datetime.now()
	current_year = current_date.year
	current_month = current_date.month
	expense.apv_number = 'A' + str('%04d' % current_year) + '' + str('%02d' % current_month) + '' + str(apv_id)
	expense.total_amount = float(request.POST['total_amount'])
	expense.check_no = request.POST['check_no']
	expense.total_withholding_tax = float(request.POST['total_withholding_tax'])
	expense.total_input_tax = float(request.POST['total_input_tax'])
	expense.transaction_type = int(request.POST['transaction_type'])
	expense.receipt = request.POST['receipt']
	expense.OR_number = request.POST['OR_number']
	expense.vendor_id = request.POST['vendor_id']
	expense.vendor_name = request.POST['vendor_name']
	expense.vendor_short_name = request.POST['vendor_short_name']
	expense.vendor_tin = request.POST['vendor_TIN']
	expense.vendor_address = request.POST['vendor_address']
	expense.vendor_is_vat_registered = bool(int(request.POST['vendor_is_vat_registered']))
	expense.budget_name = request.POST['budget_name']
	expense.budget_id = request.POST['budget_id']
	expense.expense_type = int(request.POST['expense_type'])
	expense.released_fund_id = request.POST['released_fund_id']
	expense.released_fund_remarks = request.POST['released_fund_remarks']
	#expense.invoice_number = request.POST['invoice_number']
	expense.particulars = request.POST['particulars']
	expense.charged_to_id = request.POST['charged_to_id']
	expense.charged_to_name = request.POST['charged_to_name']
	expense.paid_amount = 0
	expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
	expense.status = int(request.POST['status'])
	expense.notes = request.POST['notes']
	payment_method = int(request.POST['payment_method'])
	expense.payment_method = payment_method
	expense.department = int(request.POST['department'])
	expense.charge_type = int(request.POST['charge_type'])
	expense.is_charge_billed = bool(int(request.POST['is_charge_billed']))

	# Add Charge
	ndb_helper.update_meta_data(expense, True)
	expense_key = expense.put()

	# Create Charge Items
	expense_items = simplejson.loads(request.POST['expense_items'])
	for item in expense_items:
		expense_item = ExpenseItem()
		expense_item.qty  = float(item["qty"])
		expense_item.units  = item["units"]
		expense_item.item_name  = item["item_name"]
		expense_item.withholding_tax  = float(item["withholding_tax"])
		expense_item.input_tax  = float(item["input_tax"])
		expense_item.amount  = float(item["expense_amount"])
		expense_item.expense_id = expense_key.urlsafe()
		ndb_helper.update_meta_data(expense_item, True)
		expense_item.put()

	# Update Total Advances of affected employee
	charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
	charged_person = charged_person_key.get()
	charged_person.total_balance += expense.total_amount
	charged_person.put()

	# Update Released Funds Actual Expense and Variance
	released_fund_id = expense.released_fund_id
	released_funds_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_funds_key.get()
	released_fund.actual_expense = released_fund.actual_expense + expense.total_amount
	released_fund.variance = released_fund.amount - released_fund.actual_expense
	released_fund.put()
	
	# Update point person's funds on hand if cash is spent
	point_person_key = ndb.Key(urlsafe=released_fund.point_person)
	point_person = point_person_key.get()
	point_person.released_funds_on_hand = point_person.released_funds_on_hand - expense.total_amount
	point_person.put()

	return HttpResponse('ok')

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_settlement_post(request):
	# Create Settlement
	settlement = Settlement()
	date = request.POST['settlement_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	settlement.date = date_
	settlement.settlement_number = "STL" + str(IdCounter.record('settlement'))
	settlement.total_amount = float(request.POST['total_amount'])
	settlement.remarks = request.POST['remarks']
	settlement.notes = request.POST['notes']
	settlement.department = int(request.POST['department'])
	settlement.settlement_type = int(request.POST['settlement_type'])
	settlement.settled_charge_type = int(request.POST['settled_charge_type'])
	settlement.credit_to_id = request.POST['credit_to_id']
	settlement.credit_to_name = request.POST['credit_to_name']
	
	# Add Settlement
	ndb_helper.update_meta_data(settlement, True)
	settlement_key = settlement.put()

	# Update Current Balance of affected employee
	credited_person_key = ndb.Key(urlsafe=settlement.credit_to_id)
	credited_person = credited_person_key.get()
	credited_person.total_balance -= settlement.total_amount
	credited_person.put()

	return HttpResponse('ok')
@ensure_csrf_cookie
def update_released_fund_expense(request, expense_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	# retrieve expense
	expense_key = ndb.Key(urlsafe=expense_id)
	expense = expense_key.get()
	# retrieve released fund
	released_fund_key = ndb.Key(urlsafe=expense.released_fund_id)
	released_fund = released_fund_key.get()
	# Check if expense updater is same as released fund creator
	user_email = user_helper.get_current_user_email_address().email()
	user_access_type = user_helper.get_user_access_type()
	if user_access_type != 1 and str(user_email) == str(released_fund.created_by):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		vendors = getVendors()
		employees = getEmployees()
		expense_items = getExpenseItems(expense_id)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'expense' : expense,
			'expense_date' : expense.date.strftime("%Y-%m-%d"),
			'expense_key' : expense_id,
			'released_fund' : released_fund,
			'expense_items' : expense_items,
			'vendors' : vendors,
			'employees': employees,
			'base_template': base_template
		}
		return render(request, 'budget/editExpense.html', context, content_type="text/html")			
	elif request.method == 'POST' and request.is_ajax:
		response = update_expense_post(request, expense_id)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_expense_post(request, expense_id):
	# Get Expense to update
	expense_key = ndb.Key(urlsafe=expense_id)
	expense = expense_key.get()
	# Store current expense info
	old_expense = expense.total_amount
	old_expense_type = expense.expense_type 
	old_charged_to = expense.charged_to_id 
	# Update expense info
	date = request.POST['expense_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	expense.date = date_
	expense.total_amount = float(request.POST['total_amount'])
	expense.check_no = request.POST['check_no']
	expense.total_withholding_tax = float(request.POST['total_withholding_tax'])
	expense.total_input_tax = float(request.POST['total_input_tax'])
	expense.transaction_type = int(request.POST['transaction_type'])
	expense.receipt = request.POST['receipt']
	expense.OR_number = request.POST['OR_number']
	expense.vendor_id = request.POST['vendor_id']
	expense.vendor_name = request.POST['vendor_name']
	expense.vendor_short_name = request.POST['vendor_short_name']
	expense.vendor_tin = request.POST['vendor_TIN']
	expense.vendor_address = request.POST['vendor_address']
	expense.vendor_is_vat_registered = bool(int(request.POST['vendor_is_vat_registered']))
	expense.expense_type = int(request.POST['expense_type'])
	#expense.released_fund_id = request.POST['released_fund_id']
	expense.particulars = request.POST['particulars']
	expense.charged_to_id = request.POST['charged_to_id']
	expense.charged_to_name = request.POST['charged_to_name']
	expense.paid_amount = float(request.POST['paid_amount'])
	expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
	expense.charge_type = int(request.POST['charge_type'])
	expense.notes = request.POST['notes']
	expense.department = int(request.POST['department'])
	expense.gross_amount = float(request.POST['gross_amount'])
	# Update Expense
	ndb_helper.update_meta_data(expense, False)
	expense.put()

	# Update/Create Expense Items
	expense_items = simplejson.loads(request.POST['expense_items'])
	for item in expense_items:
		expense_item_id = item["expense_item_key"]
		if expense_item_id == "":
			# Create new expense item if new
			expense_item = ExpenseItem()
			expense_item.qty  = float(item["qty"])
			expense_item.units  = item["units"]
			expense_item.item_name  = item["item_name"]
			expense_item.withholding_tax  = float(item["withholding_tax"])
			expense_item.input_tax  = float(item["input_tax"])
			expense_item.amount  = float(item["expense_amount"])
			expense_item.expense_id = expense_key.urlsafe()
			ndb_helper.update_meta_data(expense_item, True)
			expense_item.put()
		else:
			# Update expense item if existing
			expense_item_key = ndb.Key(urlsafe=expense_item_id)
			expense_item = expense_item_key.get()
			expense_item.qty = float(item["qty"])
			expense_item.units  = item["units"]
			expense_item.item_name  = item["item_name"]
			expense_item.withholding_tax  = float(item["withholding_tax"])
			expense_item.input_tax  = float(item["input_tax"])
			expense_item.amount  = float(item["expense_amount"])
			ndb_helper.update_meta_data(expense_item, False)
			expense_item.put()

	# Update Released Funds Actual Expense and Variance
	# Get Released Fund
	released_fund_id = expense.released_fund_id
	released_funds_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_funds_key.get()
	if old_expense < expense.total_amount:
		offset = expense.total_amount - old_expense
		released_fund.actual_expense += offset
	else:
		offset = old_expense - expense.total_amount
		released_fund.actual_expense -= offset
	released_fund.variance = released_fund.amount - released_fund.actual_expense
	released_fund.put()
	
	# Update point person's funds on hand
	point_person_key = ndb.Key(urlsafe=released_fund.point_person)
	point_person = point_person_key.get()
	if old_expense < expense.total_amount:
		offset = expense.total_amount - old_expense
		point_person.released_funds_on_hand -= offset
	elif old_expense > expense.total_amount:
		offset = old_expense - expense.total_amount
		point_person.released_funds_on_hand += offset
	point_person.put()
	# If there is an update in charged_to
	if old_charged_to != expense.charged_to_id:
		# if newly charged_person
		if old_charged_to == "N/A":
			# add current expense total to charged person
			charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
			charged_person = charged_person_key.get()
			charged_person.total_balance += expense.total_amount
			charged_person.put()
		# if charge is removed
		elif expense.charged_to_id == "N/A":
			# deduct old amount from current total advances
			charged_person_key = ndb.Key(urlsafe=old_charged_to)
			charged_person = charged_person_key.get()
			charged_person.total_balance -= old_expense
			charged_person.put()
		# if charge is transferred from old to new charged person
		elif old_charged_to != "N/A" and expense.charged_to_id != "N/A":
			old_charged_person_key = ndb.Key(urlsafe=old_charged_to)
			old_charged_person = old_charged_person_key.get()
			charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
			charged_person = charged_person_key.get()
			# Deduct old amount from old charged person
			old_charged_person.total_balance -= old_expense
			old_charged_person.put()
			# Add new amount to new charged person
			charged_person.total_balance += expense.total_amount
			charged_person.put()
	# If same charged to and not N/A
	elif expense.charged_to_id != "N/A":
		# Update charged to amount
		charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
		charged_person = charged_person_key.get()
		if old_expense < expense.total_amount:
			offset = expense.total_amount - old_expense
			charged_person.total_balance += offset
		elif old_expense > expense.total_amount:
			offset = old_expense - expense.total_amount
			charged_person.total_balance -= offset
		charged_person.put()

	return HttpResponse('ok')

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_charge_post(request, charge_id):
	# Get Charge to update
	charge_key = ndb.Key(urlsafe=charge_id)
	charge = charge_key.get()
	# Store current charge info
	old_charge = charge.total_amount
	old_charged_to = charge.charged_to_id 
	# Update charge info
	date = request.POST['charge_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	charge.date = date_
	charge.total_amount = float(request.POST['total_amount'])
	
	charge.particulars = request.POST['particulars']
	charge.charged_to_id = request.POST['charged_to_id']
	charge.charged_to_name = request.POST['charged_to_name']
	charge.notes = request.POST['notes']
	charge.department = int(request.POST['department'])
	charge.charge_type = int(request.POST['charge_type'])
	# Update Charge
	ndb_helper.update_meta_data(charge, False)
	charge.put()

	# Update/Create Charge Items
	charge_items = simplejson.loads(request.POST['charge_items'])
	for item in charge_items:
		charge_item_id = item["charge_item_key"]
		if charge_item_id == "":
			# Create new expense item if new
			expense_item = ExpenseItem()
			expense_item.qty  = float(item["qty"])
			expense_item.units  = item["units"]
			expense_item.item_name  = item["item_name"]
			expense_item.withholding_tax  = float(item["withholding_tax"])
			expense_item.input_tax  = float(item["input_tax"])
			expense_item.amount  = float(item["expense_amount"])
			expense_item.expense_id = charge_key.urlsafe()
			ndb_helper.update_meta_data(expense_item, True)
			expense_item.put()
		else:
			# Update expense item if existing
			expense_item_key = ndb.Key(urlsafe=charge_item_id)
			expense_item = expense_item_key.get()
			expense_item.qty = float(item["qty"])
			expense_item.units  = item["units"]
			expense_item.item_name  = item["item_name"]
			expense_item.withholding_tax  = float(item["withholding_tax"])
			expense_item.input_tax  = float(item["input_tax"])
			expense_item.amount  = float(item["expense_amount"])
			ndb_helper.update_meta_data(expense_item, False)
			expense_item.put()

	# If there is an update in charged_to
	if old_charged_to != charge.charged_to_id:
		# if newly charged_person
		if old_charged_to == "N/A":
			# add current charge total to charged person
			charged_person_key = ndb.Key(urlsafe=charge.charged_to_id)
			charged_person = charged_person_key.get()
			charged_person.total_balance += charge.total_amount
			charged_person.put()
		# if charge is removed
		elif charge.charged_to_id == "N/A":
			# deduct old amount from current total advances
			charged_person_key = ndb.Key(urlsafe=old_charged_to)
			charged_person = charged_person_key.get()
			charged_person.total_balance -= old_charge
			charged_person.put()
		# if charge is transferred from old to new charged person
		elif old_charged_to != "N/A" and charge.charged_to_id != "N/A":
			old_charged_person_key = ndb.Key(urlsafe=old_charged_to)
			old_charged_person = old_charged_person_key.get()
			charged_person_key = ndb.Key(urlsafe=charge.charged_to_id)
			charged_person = charged_person_key.get()
			# Deduct old amount from old charged person
			old_charged_person.total_balance -= old_charge
			old_charged_person.put()
			# Add new amount to new charged person
			charged_person.total_balance += charge.total_amount
			charged_person.put()
	# If same charged to and not N/A
	elif charge.charged_to_id != "N/A":
		# Update charged to amount
		charged_person_key = ndb.Key(urlsafe=charge.charged_to_id)
		charged_person = charged_person_key.get()
		if old_charge < charge.total_amount:
			offset = charge.total_amount - old_charge
			charged_person.total_balance += offset
		elif old_charge > charge.total_amount:
			offset = old_charge - charge.total_amount
			charged_person.total_balance -= offset
		charged_person.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def update_charge(request, charge_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':	
		# retrieve charge
		charge_key = ndb.Key(urlsafe=charge_id)
		charge = charge_key.get()
		employee_key  = ndb.Key(urlsafe=charge.charged_to_id)
		employee = employee_key.get()
		employees = getEmployees()
		employee_key = employee.key.urlsafe()
		charge_items = getExpenseItems(charge_id)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'charge' : charge,
			'charge_date' : charge.date.strftime("%Y-%m-%d"),
			'charge_key' : charge_id,
			'charge_items' : charge_items,
			'employees': employees,
			'employee': employee,
			'employee_key': employee_key,
			'base_template': base_template
		}
		return render(request, 'budget/editCharge.html', context, content_type="text/html")			
	elif request.method == 'POST' and request.is_ajax:
		response = update_charge_post(request, charge_id)
		return response

@ensure_csrf_cookie
def update_settlement(request, settlement_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':	
		# retrieve charge
		settlement_key = ndb.Key(urlsafe=settlement_id)
		settlement = settlement_key.get()
		employee_key  = ndb.Key(urlsafe=settlement.credit_to_id)
		employee = employee_key.get()
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'settlement' : settlement,
			'settlement_date' : settlement.date.strftime("%Y-%m-%d"),
			'settlement_key' : settlement_id,
			'employees': employees,
			'employee': employee,
			'employee_key': settlement.credit_to_id,
			'base_template': base_template
		}
		return render(request, 'budget/editSettlement.html', context, content_type="text/html")			
	elif request.method == 'POST' and request.is_ajax:
		response = update_settlement_post(request, settlement_id)
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def update_settlement_post(request, settlement_id):
	# Get Settlement to update
	settlement_key = ndb.Key(urlsafe=settlement_id)
	settlement = settlement_key.get()
	# Store current settlement info
	old_settlement = settlement.total_amount
	old_credit_to = settlement.credit_to_id 

	# Update settlement info	
	date = request.POST['settlement_date']
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	settlement.date = date_
	settlement.total_amount = float(request.POST['total_amount'])
	settlement.remarks = request.POST['remarks']
	settlement.notes = request.POST['notes']
	settlement.department = int(request.POST['department'])
	settlement.settlement_type = int(request.POST['settlement_type'])
	settlement.settled_charge_type = int(request.POST['settled_charge_type'])
	settlement.credit_to_id = request.POST['credit_to_id']
	settlement.credit_to_name = request.POST['credit_to_name']
	# Update Settlement
	ndb_helper.update_meta_data(settlement, False)
	settlement.put()

	# If there is an update in credit to
	if old_credit_to != settlement.credit_to_id:
		old_charged_person_key = ndb.Key(urlsafe=old_credit_to)
		old_charged_person = old_charged_person_key.get()
		charged_person_key = ndb.Key(urlsafe=settlement.credit_to_id)
		charged_person = charged_person_key.get()
		# Deduct old amount from old credited person
		old_charged_person.total_balance += old_settlement
		old_charged_person.put()
		# Add new amount to new credited person
		charged_person.total_balance -= settlement.total_amount
		charged_person.put()
	# If same credited to and not N/A
	elif settlement.credit_to_id != "N/A":
		# Update credited to amount
		charged_person_key = ndb.Key(urlsafe=settlement.credit_to_id)
		charged_person = charged_person_key.get()
		if old_settlement < settlement.total_amount:
			offset = settlement.total_amount - old_settlement
			charged_person.total_balance -= offset
		elif old_settlement > settlement.total_amount:
			offset = old_settlement - settlement.total_amount
			charged_person.total_balance += offset
		charged_person.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def list_released_fund_expense(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
	    released_fund_id = released_fund_id.split("/", 1)[0] # remove other params after budget ID (ex. paged, page)
	    released_fund_key = ndb.Key(urlsafe=released_fund_id)
	    released_fund = released_fund_key.get()
	    released_fund_key = released_fund.key.urlsafe()
	    expense_objects = Expense.query(Expense.released_fund_id == released_fund_id).order(Expense.apv_number).fetch()
	    expenses_dict = ndb_helper.filter_results(expense_objects)
	    relfund_created_date = ndb_helper.convertToCurrentTZ(released_fund.created_date)
	    relfund_modified_date = ndb_helper.convertToCurrentTZ(released_fund.modified_date)
	    user_email = user_helper.get_current_user_email_address().email()
	    user_access_type = user_helper.get_user_access_type()
	    base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

    	return render(request, 'budget/viewReleasedFundExpenses.html', {"expenses": expenses_dict, "released_fund":released_fund, "released_fund_key":released_fund_key, 'relfund_created_date': relfund_created_date, 'relfund_modified_date': relfund_modified_date, 'user_email': user_email, 'user_access_type': user_access_type, 'base_template': base_template})

def list_cash_on_hand(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_objects = Employee.query().order(Employee.first_name).fetch()
		employees_dict = ndb_helper.filter_results(employee_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'budget/viewCashOnHand.html', {"employees": employees_dict, 'base_template': base_template})


def view_newsfeed(request):

	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		date_today = datetime.datetime.now() + datetime.timedelta(hours=+8)
		date_today = date_today.replace(hour=0, minute=0)
		date_tomorrow = date_today + datetime.timedelta(hours=+24)

		# Retrieve released cash today
		released_cash_today_objects = ReleasedFund.query(ndb.AND(ReleasedFund.payment_method == Payment_Method_Cash, ReleasedFund.created_date >= date_today, ReleasedFund.created_date < date_tomorrow)).fetch()
		released_cash_today_dict = ndb_helper.filter_results(released_cash_today_objects)
		
		# Retrieve released checks today
		released_checks_today_objects = ReleasedFund.query(ndb.AND(ReleasedFund.payment_method == Payment_Method_Check, ReleasedFund.created_date >= date_today, ReleasedFund.created_date < date_tomorrow)).fetch()
		released_checks_today_dict = ndb_helper.filter_results(released_checks_today_objects)
		
		# Retrieve closed funds today
		closed_funds_today_objects = ReleasedFund.query(ndb.AND(ReleasedFund.date_closed >= date_today, ReleasedFund.date_closed < date_tomorrow)).fetch()
		closed_funds_today_dict  = ndb_helper.filter_results(closed_funds_today_objects)
		
		# Retrieve added expenses today
		expense_objects = Expense.query(ndb.AND(Expense.created_date >= date_today, Expense.created_date < date_tomorrow)).order(Expense.created_date).fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)
		
		# Retrieve overdue funds today
		overdue_liquidation_today_objects = ReleasedFund.query(ndb.AND(ReleasedFund.status == Rel_Fund_Status_Open, ReleasedFund.date_released <= date_today + datetime.timedelta(days=-7))).order(ReleasedFund.date_released).fetch()
		overdue_liquidation_today_dict = ndb_helper.filter_results(overdue_liquidation_today_objects)

		# Retrieve nearing due checks
		nearing_due_checks_objects = Credit.query(ndb.AND(Credit.payment_method == Payment_Method_Check, ndb.OR(Credit.check_status == Check_Status_Ready, Credit.check_status == Check_Status_Released), Credit.check_date <= date_today + datetime.timedelta(days=3))).order(Credit.check_date).fetch()
		nearing_due_checks_dicts = ndb_helper.filter_results(nearing_due_checks_objects)
		
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		
		# Removed Paging

	return render(request, 'budget/viewNewsfeed.html', {"date_today": date_today.strftime("%b %d, %Y"), "date_tomorrow": date_tomorrow, "released_cash_today": released_cash_today_dict, "released_check_today": released_checks_today_dict, 'added_expenses_today': expenses_dict, 'overdue_liquidation_today': overdue_liquidation_today_dict, 'closed_funds_today': closed_funds_today_dict, 'nearing_due_checks': nearing_due_checks_dicts, 'base_template': base_template})

def view_openliquidation(request):

	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':

		# Retrieve overdue funds today
		open_liquidation_today_objects = ReleasedFund.query(ReleasedFund.status == Rel_Fund_Status_Open).order(ReleasedFund.date_released).fetch()
		open_liquidation_today_dict = ndb_helper.filter_results(open_liquidation_today_objects)

		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

	return render(request, 'budget/viewOpenLiquidation.html', {'open_liquidation_today_dict': open_liquidation_today_dict, 'base_template': base_template})

@ensure_csrf_cookie
def list_released_fund_waybills(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
	    released_fund_id = released_fund_id.split("/", 1)[0] # remove other params after budget ID (ex. paged, page)
	    released_fund_key = ndb.Key(urlsafe=released_fund_id)
	    released_fund = released_fund_key.get()
	    released_fund_key = released_fund.key.urlsafe()
	    relfund_created_date = ndb_helper.convertToCurrentTZ(released_fund.created_date)
	    relfund_modified_date = ndb_helper.convertToCurrentTZ(released_fund.modified_date)
	    waybill_objects = Waybill.query(Waybill.released_fund_id == released_fund_id).order(-Waybill.date).fetch()
	    waybills_dict = ndb_helper.filter_results(waybill_objects)
	    expense_objects = Expense.query(Expense.released_fund_id == released_fund_id).order(-Expense.date).fetch()
	    expenses_dict = ndb_helper.filter_results(expense_objects)
	    user_email = user_helper.get_current_user_email_address().email()
	    base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
	    
	    return render(request, 'budget/viewReleasedFundWaybills.html', {"waybills": waybills_dict, "waybills_count": len(waybills_dict), "expenses" : expenses_dict, "released_fund":released_fund, "released_fund_key":released_fund_id, 'relfund_created_date': relfund_created_date, 'relfund_modified_date': relfund_modified_date, 'user_email': user_email, 'base_template': base_template})

@ensure_csrf_cookie
def list_releases_today(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		# Get Date Today
		date_today = datetime.datetime.now() + datetime.timedelta(hours=+8)
		date_today = date_today.replace(hour=0, minute=0)
		# Query Released Funds Today
		released_fund_objects = ReleasedFund.query(ReleasedFund.date_released == date_today).fetch()
		released_funds_dict = ndb_helper.filter_results(released_fund_objects)

		return render(request, 'budget/viewReleases.html', {"released_funds": released_funds_dict, 'currentIdx': 'Today', 'base_template': base_template})

@ensure_csrf_cookie
def list_releases_of_date(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set base template to BLANK
		base_template = user_helper.TEMPLATE_BLANK
		# Retrieve GET data
		release_start_date_year = request.GET['release_start_date_year']
		release_start_date_month = request.GET['release_start_date_month']
		release_start_date_day = request.GET['release_start_date_day']
		release_end_date_year = request.GET['release_end_date_year']
		release_end_date_month = request.GET['release_end_date_month']
		release_end_date_day = request.GET['release_end_date_day']
		currentIdx = request.GET['currentIdx']
		# Get date range
		start_date = datetime.datetime(int(release_start_date_year),int(release_start_date_month), int(release_start_date_day))
		end_date = datetime.datetime(int(release_end_date_year),int(release_end_date_month), int(release_end_date_day))
		# Query Released Funds
		released_fund_objects = ReleasedFund.query(ndb.AND(ReleasedFund.date_released >= start_date, ReleasedFund.date_released <= end_date)).fetch()
		released_funds_dict = ndb_helper.filter_results(released_fund_objects)

		return render(request, 'budget/viewReleases.html', {"released_funds": released_funds_dict, 'currentIdx': currentIdx, 'customYear': release_start_date_year, 'customMonth': release_start_date_month, 'customDate': release_start_date_day, 'base_template': base_template})

@ensure_csrf_cookie
def list_expenses_today(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set base template to BLANK
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		# Get date today
		date_today = datetime.datetime.now() + datetime.timedelta(hours=+8)
		date_today = date_today.replace(hour=0, minute=0)
		# Query expense objects
		expense_objects = Expense.query(Expense.date == date_today, Expense.charged_to_id == "N/A").fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)

		return render(request, 'budget/viewExpensesHistory.html', {"expenses": expenses_dict, 'currentIdx': 'Today', 'base_template': base_template})

@ensure_csrf_cookie
def list_expenses_of_date(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_NORMAL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.TEMPLATE_BLANK
		# Retrieve GET data
		expense_start_date_year = request.GET['expense_start_date_year']
		expense_start_date_month = request.GET['expense_start_date_month']
		expense_start_date_day = request.GET['expense_start_date_day']
		expense_end_date_year = request.GET['expense_end_date_year']
		expense_end_date_month = request.GET['expense_end_date_month']
		expense_end_date_day = request.GET['expense_end_date_day']
		currentIdx = request.GET['currentIdx']
		# Get date range
		start_date = datetime.datetime(int(expense_start_date_year),int(expense_start_date_month), int(expense_start_date_day))
		end_date = datetime.datetime(int(expense_end_date_year),int(expense_end_date_month), int(expense_end_date_day))
		# Query Expense objects
		expense_objects = Expense.query(ndb.AND(Expense.date >= start_date, Expense.date <= end_date), Expense.charged_to_id == "N/A").fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)

		return render(request, 'budget/viewExpensesHistory.html', {"expenses": expenses_dict, 'currentIdx': currentIdx, 'customYear': expense_start_date_year, 'customMonth': expense_start_date_month, 'customDate': expense_start_date_day, 'base_template': base_template})

@ensure_csrf_cookie
def list_charges_today(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		# Get Date Today
		date_today = datetime.datetime.now() + datetime.timedelta(hours=+8)
		date_today = date_today.replace(hour=0, minute=0)
		# Query Charges Today
		charges_objects = Expense.query(Expense.date == date_today, Expense.charged_to_id != "N/A").fetch()
		charges_dict = ndb_helper.filter_results(charges_objects)

		return render(request, 'budget/viewChargesHistory.html', {"charges": charges_dict, 'currentIdx': 'Today', 'base_template': base_template})

@ensure_csrf_cookie
def list_charges_of_date(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.TEMPLATE_BLANK
		# Retrieve GET data
		charge_start_date_year = request.GET['charge_start_date_year']
		charge_start_date_month = request.GET['charge_start_date_month']
		charge_start_date_day = request.GET['charge_start_date_day']
		charge_end_date_year = request.GET['charge_end_date_year']
		charge_end_date_month = request.GET['charge_end_date_month']
		charge_end_date_day = request.GET['charge_end_date_day']
		currentIdx = request.GET['currentIdx']
		# Get date range
		start_date = datetime.datetime(int(charge_start_date_year),int(charge_start_date_month), int(charge_start_date_day))
		end_date = datetime.datetime(int(charge_end_date_year),int(charge_end_date_month), int(charge_end_date_day))
		# Query Charge objects
		query = Expense.query(ndb.AND(Expense.date >= start_date, Expense.date <= end_date))
		charges_objects = query.filter(Expense.expense_type == 36).fetch()
		charges_dict = ndb_helper.filter_results(charges_objects)

		return render(request, 'budget/viewChargesHistory.html', {"charges": charges_dict, 'currentIdx': currentIdx, 'customYear': charge_start_date_year, 'customMonth': charge_start_date_month, 'customDate': charge_start_date_day, 'base_template': base_template})

@ensure_csrf_cookie
def list_settlements_today(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		# Get Date Today
		date_today = datetime.datetime.now() + datetime.timedelta(hours=+8)
		date_today = date_today.replace(hour=0, minute=0)
		# Query Settlements Today
		settlements_objects = Settlement.query(Settlement.date == date_today).fetch()
		settlements_dict = ndb_helper.filter_results(settlements_objects)

		return render(request, 'budget/viewSettlementsHistory.html', {"settlements": settlements_dict, 'currentIdx': 'Today', 'base_template': base_template})

@ensure_csrf_cookie
def list_settlements_of_date(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Set Base Template to appropriate base template
		base_template = user_helper.TEMPLATE_BLANK
		# Retrieve GET data
		settlement_start_date_year = request.GET['settlement_start_date_year']
		settlement_start_date_month = request.GET['settlement_start_date_month']
		settlement_start_date_day = request.GET['settlement_start_date_day']
		settlement_end_date_year = request.GET['settlement_end_date_year']
		settlement_end_date_month = request.GET['settlement_end_date_month']
		settlement_end_date_day = request.GET['settlement_end_date_day']
		currentIdx = request.GET['currentIdx']
		# Get date range
		start_date = datetime.datetime(int(settlement_start_date_year),int(settlement_start_date_month), int(settlement_start_date_day))
		end_date = datetime.datetime(int(settlement_end_date_year),int(settlement_end_date_month), int(settlement_end_date_day))
		# Query Settlement objects
		settlements_objects = Settlement.query(Settlement.date >= start_date, Settlement.date <= end_date).fetch()
		settlements_dict = ndb_helper.filter_results(settlements_objects)
		
		return render(request, 'budget/viewSettlementsHistory.html', {"settlements": settlements_dict, 'currentIdx': currentIdx, 'customYear': settlement_start_date_year, 'customMonth': settlement_start_date_month, 'customDate': settlement_start_date_day, 'base_template': base_template})


@ensure_csrf_cookie
def create_charge(request, employee_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_key = ndb.Key(urlsafe=employee_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'employee_key': employee_key,
			'employee': employee,
			'employees': employees,
			'base_template': base_template
		}
		return render(request, 'budget/addCharge.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_charge_post(request)		
		return response

@ensure_csrf_cookie
def create_cashadvance_charge(request, released_fund_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")
	
	# Check if expense submitter is same as released fund creator and not admin
	released_fund_key = ndb.Key(urlsafe=released_fund_id)
	released_fund = released_fund_key.get()
	user_email = user_helper.get_current_user_email_address().email()
	user_access_type = user_helper.get_user_access_type()    
	if str(user_email) == str(released_fund.created_by) and user_access_type != 1:
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		released_fund_key = ndb.Key(urlsafe=released_fund_id)
		released_fund = released_fund_key.get()
		clients = getClients()
		vendors = getVendors()
		employees = getEmployees()
		# Retrieve Source
		expense_type = request.GET.get('expense_type')
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'released_fund' : released_fund,
			'released_fund_key' : released_fund_id,
			'released_fund_date_released' : released_fund.date_released.strftime("%Y-%m-%d"),
			'clients' : clients,
			'vendors' : vendors,
			'employees': employees,
			'expense_type': expense_type,
			'base_template': base_template
		}
		return render(request, 'budget/addCACharge.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_cashadvance_charge_post(request)
		return response

@ensure_csrf_cookie
def create_charges(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'employees': employees,
			'base_template': base_template
		}
		return render(request, 'budget/addCharges.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_charges_post(request)		
		return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def create_charges_post(request):
	# Create Charges
	charge_expenses = simplejson.loads(request.POST['charges_array'])
	for item in charge_expenses:
		# Create expense
		expense = Expense()
		date = item['expense_date']
		date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
		expense.date = date_
		# Generate APV ID
		apv_id = IdCounter.record('apv_number')
		current_date = datetime.datetime.now()
		current_year = current_date.year
		current_month = current_date.month
		# Set values
		expense.apv_number = 'A' + str('%04d' % current_year) + '' + str('%02d' % current_month) + '' + str(apv_id)
		expense.total_amount = float(item['total_amount'])
		expense.check_no = item['check_no']
		expense.total_withholding_tax = float(item['total_withholding_tax'])
		expense.total_input_tax = float(item['total_input_tax'])
		expense.transaction_type = int(item['transaction_type'])
		expense.receipt = item['receipt']
		expense.OR_number = item['OR_number']
		expense.vendor_id = item['vendor_id']
		expense.vendor_name = item['vendor_name']
		expense.vendor_short_name = item['vendor_short_name']
		expense.vendor_tin = item['vendor_TIN']
		expense.vendor_address = item['vendor_address']
		expense.vendor_is_vat_registered = bool(int(item['vendor_is_vat_registered']))
		expense.budget_name = item['budget_name']
		expense.budget_id = item['budget_id']
		expense.expense_type = int(item['expense_type'])
		expense.released_fund_id = item['released_fund_id']
		expense.released_fund_remarks = item['released_fund_remarks']
		#expense.waybill_id = item['waybill_id']
		#expense.waybill_number = item['waybill_number']
		#expense.invoice_number = item['invoice_number']
		expense.particulars = item['particulars']
		expense.charged_to_id = item['charged_to_id']
		expense.charged_to_name = item['charged_to_name']
		expense.paid_amount = 0
		expense.remaining_amount_to_pay = expense.total_amount - expense.paid_amount
		expense.notes = item['notes']
		expense.department = int(item['department'])
		expense.charge_type = int(item['charge_type'])
		expense.is_charge_billed = bool(item['is_charge_billed'])

		# Add Expense
		ndb_helper.update_meta_data(expense, True)
		expense_key = expense.put()

		# Create Expense Items
		expense_item = ExpenseItem(parent=expense_key)
		expense_item.qty  = float(item["qty"])
		expense_item.units  = item["units"]
		expense_item.item_name  = item["item_name"]
		expense_item.withholding_tax  = float(item["withholding_tax"])
		expense_item.input_tax  = float(item["input_tax"])
		expense_item.amount  = float(item["expense_amount"])
		expense_item.expense_id = expense_key.urlsafe()
		ndb_helper.update_meta_data(expense_item, True)
		expense_item.put()

		# Update Total Advances of affected employee
		charged_person_key = ndb.Key(urlsafe=expense.charged_to_id)
		charged_person = charged_person_key.get()
		charged_person.total_balance += expense.total_amount
		charged_person.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def create_settlement(request, employee_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_key = ndb.Key(urlsafe=employee_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'employee_key': employee_key,
			'employee': employee,
			'employees': employees,
			'base_template': base_template
		}
		return render(request, 'budget/addSettlement.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_settlement_post(request)		
		return response

@ensure_csrf_cookie
def create_settlements(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employees = getEmployees()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'employees': employees,
			'base_template': base_template
		}
		return render(request, 'budget/addSettlements.html', context, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		response = create_settlements_post(request)		
		return response

@ndb.transactional(retries=3, xg=True)
@ensure_csrf_cookie
def create_settlements_post(request):
	# Create Settlements
	settlements = simplejson.loads(request.POST['settlements_array'])
	for item in settlements:
		# Create Settlement
		settlement = Settlement()
		date = item['settlement_date']
		date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
		settlement.date = date_
		settlement.settlement_number = "STL" + str(IdCounter.record('settlement'))
		settlement.total_amount = float(item['total_amount'])
		settlement.remarks = item['remarks']
		settlement.notes = item['notes']
		settlement.department = int(item['department'])
		settlement.settlement_type = int(item['settlement_type'])
		settlement.settled_charge_type = int(item['settled_charge_type'])
		settlement.credit_to_id = item['credit_to_id']
		settlement.credit_to_name = item['credit_to_name']
		
		# Add Settlement
		ndb_helper.update_meta_data(settlement, True)
		settlement_key = settlement.put()

		# Update Current Balance of affected employee
		credited_person_key = ndb.Key(urlsafe=settlement.credit_to_id)
		credited_person = credited_person_key.get()
		credited_person.total_balance -= settlement.total_amount
		credited_person.put()

	return HttpResponse('ok')
def cron_cache(request):
	logging.info('------------Cache Cron---------------')
	list_budget_expense(request)
	list_credit_fund(request)
	getEmployees()
	getClients()
	return HttpResponse('ok')

def cron_released_fund_mail(request):
	logging.info('------------Email Releases Cron---------------')
	yesterday = datetime.datetime.today() - datetime.timedelta(days=1) + datetime.timedelta(hours=+8)
	date_today = date_today = datetime.datetime.today() + datetime.timedelta(hours=+8)
	# Remove time
	yesterday =  yesterday.replace(hour=0, minute=0, second=0, microsecond=0)
	date_today = date_today.replace(hour=0, minute=0, second=0, microsecond=0)
	
	logging.info('------------DATE: ' + yesterday.strftime("%b %d, %Y, %I:%M %p") +' - ' + date_today.strftime("%b %d, %Y, %I:%M %p") + ' ---------------')

	funds = ReleasedFund.query(ndb.AND(ReleasedFund.created_date >= yesterday, ReleasedFund.created_date < date_today)).fetch()
	funds_dict = ndb_helper.filter_results(funds)

	subject = '[SHEMICA] Cash and Checks Releases: ' + (yesterday + datetime.timedelta(hours=+8)).strftime("%m/%d/%Y") 

	cash = []
	check = []

	for fund in funds_dict:
		cache = {}
		cache['fund_number'] = fund.get('fund_number')
		cache['source_name'] = fund.get('source_name')
		cache['released_amount'] = fund.get('released_amount')
		cache['remarks'] = fund.get('remarks')
		cache['point_person_name'] = fund.get('point_person_name')

		method = fund.get('payment_method')
		if method == Payment_Method_Cash:
			cash.append(cache)
		if method == Payment_Method_Check:
			check.append(cache)

	body = "Good day!\n\n"
	
	if cash:
		head = 'Cash released last ' + (yesterday + datetime.timedelta(hours=+8)).strftime("%m/%d/%Y") + ":\n"
		body += head
		counter = 1
		for entry in cash:
			fund_entry = '{0}. {1} | {2} | {3} | {4} | {5} \n'.format(
				counter, entry.get('fund_number'), entry.get('source_name'),
				"P{:,.2f}".format(entry.get('released_amount')), entry.get('remarks'), entry.get('point_person_name')
			)
			body += fund_entry
			counter = counter + 1
		body += '\n'
	else:
		body += 'No cash released.\n'

	if check:
		head = 'Check released last ' + (yesterday + datetime.timedelta(hours=+8)).strftime("%m/%d/%Y") + ":\n"
		body += head
		counter = 1
		for entry in check:
			fund_entry = '{0}. {1} | {2} | {3} | {4} | {5} \n'.format(
				counter, entry.get('fund_number'), entry.get('source_name'),
				"P{:,.2f}".format(entry.get('released_amount')), entry.get('remarks'), entry.get('point_person_name')
			)
			body += fund_entry
			counter = counter + 1
	else:
		body += 'No checks released.\n'

	body += '\nThis is an automatically generated email, please do not reply.\n\n'
	body += 'Regards,\n'
	body += EMAIL_SENDER_NAME + "\n"
	body += SITE_HOME_LINK
	
	mail.send_mail(sender=EMAIL_SENDER,to=REL_FUND_EMAIL_RECIPIENTS,subject=subject, body=body)
	return HttpResponse('ok')


def cron_cash_on_hand_mail(request):
	logging.info('------------Email Cash on Hand Cron---------------')
	
	# Query employees with cash on hand
	employee_coh_objects = Employee.query(Employee.cash_on_hand > 0.1).order(-Employee.cash_on_hand).fetch()
	employees_coh_dict = ndb_helper.filter_results(employee_coh_objects)
	
	# Query employees with released funds on hand
	employee_rfoh_objects = Employee.query(Employee.released_funds_on_hand > 0.1).order(-Employee.released_funds_on_hand).fetch()
	employees_rfoh_dict = ndb_helper.filter_results(employee_rfoh_objects)
	
	cash_on_hand = []
	released_funds_on_hand = []
	
	for employee in employees_coh_dict:
		cache = {}
		cache['first_name'] = employee.get('first_name')
		cache['last_name'] = employee.get('last_name')
		cache['cash_on_hand'] = employee.get('cash_on_hand')
		cash_on_hand.append(cache)
	
	for employee in employees_rfoh_dict:
		cache = {}
		cache['first_name'] = employee.get('first_name')
		cache['last_name'] = employee.get('last_name')
		cache['released_funds_on_hand'] = employee.get('released_funds_on_hand')
		released_funds_on_hand.append(cache)

	subject = '[SHEMICA] Cash on Hand: ' + (datetime.datetime.today()).strftime("%m/%d/%Y") 
	body = "Good day!\n\n"
	
	if cash_on_hand:
		head = "Budget Cash on Hand:\n"
		body += head
		for entry in cash_on_hand:
			employee_entry = '    {0} {1} : {2}\n'.format(
				entry.get('first_name'), entry.get('last_name'),
				"P{:,.2f}".format(entry.get('cash_on_hand'))
			)
			body += employee_entry
	else:
		body += 'No employee with cash on hand.\n'
		
	if released_funds_on_hand:
		head = "\nReleased Funds on Hand:\n"
		body += head
		for entry in released_funds_on_hand:
			employee_entry = '    {0} {1} : {2}\n'.format(
				entry.get('first_name'), entry.get('last_name'),
				"P{:,.2f}".format(entry.get('released_funds_on_hand'))
			)
			body += employee_entry
	else:
		body += 'No employee with released funds on hand.\n'
	
	body += '\nThis is an automatically generated email, please do not reply.\n\n'
	body += 'Regards,\n'
	body += EMAIL_SENDER_NAME + "\n"
	body += SITE_HOME_LINK
	
	mail.send_mail(sender=EMAIL_SENDER,to=REL_FUND_EMAIL_RECIPIENTS,subject=subject, body=body)
	return HttpResponse('ok')

def list_budget_expense(request, mode=None):
	budgets = memcache.get('list_budget_expense')
	if not budgets or mode == 'update':
		budget_objects = Budget.query().order(-Budget.name).fetch(projection=[Budget.name,Budget.remaining_amount, Budget.maintaining_amount])
		budgets = ndb_helper.filter_results(budget_objects)
		memcache.add('list_budget_expense', budgets)
	
	return budgets

def check_budget_name(request, name, budget_key=None):
	budgets = list_budget_expense(request)
	for item in budgets:
		if item['name'] == name:
			if not budget_key:
				return True
			else:
				if item['key'] != budget_key:
					return True
	return False

def check_check_number(request, check_no, check_key=None):
	# Get date today
	now = date.today()
	date_year = now.year

	year_start = datetime.datetime(int(date_year),1,1)
	year_end = datetime.datetime(int(date_year),12,31)
	
	query = Credit.query(ndb.AND(Credit.date_issued >= year_start, Credit.date_issued <= year_end))
	query = query.filter(Credit.payment_method == Payment_Method_Check)
	check_objects = query.order(-Credit.date_issued).fetch()
	checks = ndb_helper.filter_results(check_objects)
	
	for item in checks:
		if item['check_no'] == check_no:
			if not check_key:
				return True
			else:
				if item['key'] != check_key:
					return True
	return False

def list_credit_fund(request):
	credit_dict = ''
	try:
		credit_dict = memcache.get('list_credit_fund')
	except:
		pass
	if credit_dict:
		return credit_dict
	else:
		credit_objects = Credit.query().order(-Credit.date_issued).fetch(projection=[Credit.date_issued, Credit.amount])
		credit_dict = ndb_helper.filter_results(credit_objects)
		memcache.add('list_credit_fund', credit_dict)
		return credit_dict

def getEmployees():
	employees_dict = ''
	try:
		employees_dict = memcache.get('list_employees')
	except:
		pass
	if employees_dict:
		return employees_dict
	else:
		employee_objects = Employee.query().order(Employee.first_name).fetch()
		employees_dict = ndb_helper.filter_results(employee_objects)
		memcache.add('list_employees', employees_dict)

	return employees_dict

def getClients():
	clients_dict = ''
	try:
		clients_dict = memcache.get('list_client')
	except:
		pass
	if clients_dict:
		return clients_dict
	else:
		client_objects = Client.query().order(Client.client_name).fetch()
		clients_dict = ndb_helper.filter_results(client_objects)
		memcache.add('list_client', clients_dict)

	return clients_dict

def getVendors():
	vendor_objects = Vendor.query().order(Vendor.name).fetch()
	vendors_dict = ndb_helper.filter_results(vendor_objects)
	memcache.add('list_vendors', vendors_dict)

	return vendors_dict

def getFunds(credit_id):
	fund_objects = Fund.query(Fund.credit_id == credit_id).order().fetch()
	funds = ndb_helper.filter_results(fund_objects)

	return funds

def getExpenseItems(expense_id):
	expense_item_objects = ExpenseItem.query(ExpenseItem.expense_id == expense_id).order().fetch()
	expense_items = ndb_helper.filter_results(expense_item_objects)

	return expense_items

def toDate(date):
	date_ = datetime.datetime.strptime(date, "%Y-%m-%d")
	return date_

def getReleasedFundExpenses(released_fund_id):
	expense_objects = Expense.query(Expense.released_fund_id == released_fund_id).order().fetch()
	expenses = ndb_helper.filter_results(expense_objects)
	return expenses

def getChecksOfStatus(check_status, check_no=""):
	credit_objects = Credit.query(ndb.OR(ndb.AND(Credit.payment_method == Payment_Method_Check, Credit.check_status == check_status), Credit.check_no == check_no)).order(Credit.check_no).fetch()
	credits_dict = ndb_helper.filter_results(credit_objects)
	return credits_dict

def notify_newly_closed_fund(released_fund):
	email_subj_intro = "Funds Liquidated!"
	mail.send_mail(sender=EMAIL_SENDER,to=LIQUIDATED_EMAIL_RECIPIENTS,subject=email_subj_intro + " Rel Fund " + released_fund.fund_number,
		body="""Dear Carlo:

This is to notify you that a released fund was liquidated:

Released Fund No.: """ + released_fund.fund_number + """
Amount: P""" + str(released_fund.released_amount) + """
Purpose: """ + released_fund.remarks + """

For more details, click the link below:
""" + SITE_LINK_TO_REL_FUNDS + released_fund.key.urlsafe() + """

This is an automatically generated email, please do not reply.

Regards,
""" + EMAIL_SENDER_NAME)

	return HttpResponse('ok')