#use appengine datastore
from google.appengine.ext import ndb
from datetime import date
import datetime
import logging

class BasicModel(ndb.Model):
	created_by = ndb.StringProperty()
	modified_by = ndb.StringProperty()
	created_date = ndb.DateTimeProperty()
	modified_date = ndb.DateTimeProperty()

class Budget(BasicModel):
	name = ndb.StringProperty()
	maintaining_amount = ndb.FloatProperty()
	remaining_amount = ndb.FloatProperty(default=0.0)
	holder = ndb.StringProperty(indexed=False)
	holder_name = ndb.StringProperty(indexed=False)
	default_department = ndb.IntegerProperty(default=0)
	is_active = ndb.BooleanProperty(default=True)

class Credit(BasicModel):
	date_issued = ndb.DateProperty()
	amount = ndb.FloatProperty()
	check_no = ndb.StringProperty(default="N/A")
	payment_method = ndb.IntegerProperty(default=0)
	notes = ndb.StringProperty(indexed=False)
	check_status = ndb.IntegerProperty(default=0)
	check_vendor_id = ndb.StringProperty(default="N/A")
	check_vendor_name = ndb.StringProperty(default="N/A")
	check_date = ndb.DateProperty()
	check_received_by = ndb.StringProperty(default="N/A")
	check_rel_fund = ndb.StringProperty(default=None,indexed=False)
	check_expense = ndb.StringProperty(default=None,indexed=False)
	check_ref_no = ndb.StringProperty(default="N/A")

class Fund(BasicModel):
	date_funded = ndb.DateProperty()
	fund_number = ndb.StringProperty()
	amount = ndb.FloatProperty(indexed=False)
	credit_id = ndb.StringProperty()
	credit_notes = ndb.StringProperty(indexed=False)
	target_id = ndb.StringProperty()
	target_name = ndb.StringProperty(indexed=False)
	point_person_name = ndb.StringProperty()


class ReleasedFund(BasicModel):
	released_fund_id = ndb.IntegerProperty()
	fund_number = ndb.StringProperty()
	release_type = ndb.IntegerProperty(indexed=False,default=0)
	source_id = ndb.StringProperty()
	source_name = ndb.StringProperty(indexed=False)
	source_department = ndb.IntegerProperty(default=0)
	amount = ndb.FloatProperty()
	released_amount = ndb.FloatProperty(indexed=False,default=0)
	returned_amount = ndb.FloatProperty(indexed=False,default=0)
	actual_expense = ndb.FloatProperty(indexed=False,default=0)
	variance = ndb.FloatProperty(default=0)
	status = ndb.IntegerProperty(default=0)
	point_person = ndb.StringProperty()
	point_person_name = ndb.StringProperty()
	date_released = ndb.DateProperty()
	date_closed = ndb.DateProperty(default=None)
	remarks = ndb.StringProperty()
	notes = ndb.StringProperty(indexed=False)
	payment_method = ndb.IntegerProperty(default=0)
	check_no = ndb.StringProperty(default="-")
	check_credit_id = ndb.StringProperty(default="N/A")
	check_vendor_id = ndb.StringProperty(default="N/A")
	default_liquidator = ndb.StringProperty(indexed=False)
	default_liquidator_name = ndb.StringProperty(indexed=False)

class Expense(BasicModel):
	date = ndb.DateProperty()
	apv_number = ndb.StringProperty()
	total_amount = ndb.FloatProperty()
	check_no = ndb.StringProperty()
	total_withholding_tax = ndb.FloatProperty()
	total_input_tax = ndb.FloatProperty()
	transaction_type = ndb.IntegerProperty()
	#receipt
	receipt_image = ndb.BlobProperty()
	OR_number = ndb.StringProperty()
	#vendor
	vendor_id = ndb.StringProperty(default="")
	vendor_name = ndb.StringProperty(default="")
	vendor_short_name = ndb.StringProperty(default="")
	vendor_tin = ndb.StringProperty(default="", indexed=False)
	vendor_address = ndb.StringProperty(default="", indexed=False)
	vendor_is_vat_registered = ndb.BooleanProperty(default=True, indexed=False)
	#budget
	budget_name = ndb.StringProperty()
	budget_id = ndb.StringProperty()
	expense_type = ndb.IntegerProperty()
	#released_funds
	released_fund_id = ndb.StringProperty()
	released_fund_remarks = ndb.StringProperty(default="-")
	released_fund_status = ndb.IntegerProperty(default=0)
	#particulars
	particulars = ndb.StringProperty()
	#receipt
	receipt = ndb.StringProperty()
	#charged to
	charged_to_id = ndb.StringProperty(default="N/A")
	charged_to_name = ndb.StringProperty(default="N/A")
	paid_amount = ndb.FloatProperty(default=0.0)
	remaining_amount_to_pay = ndb.FloatProperty(default=0.0)
	#status
	status = ndb.IntegerProperty(default=0)
	#notes
	notes = ndb.StringProperty(indexed=False)
	#active status
	active_status = ndb.IntegerProperty(default=0)
	#payment method
	payment_method = ndb.IntegerProperty(default=0)
	#department
	department = ndb.IntegerProperty(default=0)
	# boolean that determines if a charge is billed
	is_charge_billed = ndb.BooleanProperty(default=False)
	# charge_type
	charge_type = ndb.IntegerProperty(default=0)
	# gross
	gross_amount = ndb.FloatProperty(default=0)

class ExpenseItem(BasicModel):
	expense_id = ndb.StringProperty()
	qty = ndb.FloatProperty(indexed=False)
	units = ndb.StringProperty(indexed=False)
	item_name = ndb.StringProperty(indexed=False)
	amount = ndb.FloatProperty(indexed=False)
	withholding_tax = ndb.FloatProperty(indexed=False)
	input_tax = ndb.FloatProperty(indexed=False)

class Settlement(BasicModel):
	# settlement number
	settlement_number = ndb.StringProperty()
	# credit to
	credit_to_id = ndb.StringProperty()
	credit_to_name = ndb.StringProperty()
	# date
	date = ndb.DateProperty()
	# total amount
	total_amount = ndb.FloatProperty(default=0.0)
	# remarks
	remarks = ndb.StringProperty()
	# notes
	notes = ndb.StringProperty(indexed=False)
	#department
	department = ndb.IntegerProperty(default=0)
	#settlement_type (0 if deducted from payroll, 1 if cash is paid)
	settlement_type = ndb.IntegerProperty(default=0)
	#settled charge type
	settled_charge_type = ndb.IntegerProperty(default=0)

class BlobStorage(BasicModel):
	obj = ndb.BlobProperty()

class IdCounter(BasicModel):
	unique_id = ndb.IntegerProperty(required=True, default=0, indexed=False)

	@classmethod
	def record(cls, counter_name, counter=1):
		now = date.today()
		datetime_now = datetime.datetime.now()
		#first = date(now.year, now.month + 1, 1)
		
		instance = ndb.Key(IdCounter, counter_name).get()
		if not instance:
			instance = IdCounter(id=counter_name)
			instance.modified_date = datetime_now
		instance_date = instance.modified_date
		#assign date if modified date is None
		if instance_date is None:
			instance_date = datetime_now
		if instance_date.year != now.year:
			instance.unique_id = 0
		#reset if new month for apv number
		if counter_name == 'apv_number' and instance_date.month != now.month:
			instance.unique_id = 0
		instance.unique_id += counter
		instance.modified_date = datetime_now
		instance.put()
		return '%04d' % instance.unique_id