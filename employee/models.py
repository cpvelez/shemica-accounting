#use appengine datastore
from google.appengine.ext import ndb

class BasicModel(ndb.Model):
	created_by = ndb.StringProperty(indexed=False)
	modified_by = ndb.StringProperty(indexed=False)
	created_date = ndb.DateTimeProperty(indexed=False,auto_now_add=True)
	modified_date = ndb.DateTimeProperty(indexed=False)
    
class Employee(BasicModel):
	first_name = ndb.StringProperty()
	middle_name = ndb.StringProperty()
	last_name = ndb.StringProperty()
	email = ndb.StringProperty()
	cash_on_hand = ndb.FloatProperty(default=0.0)
	released_funds_on_hand = ndb.FloatProperty(default=0.0)
	total_balance = ndb.FloatProperty(default=0.0)
	type = ndb.IntegerProperty(default=0)
	access_type = ndb.IntegerProperty(default=0)
	active_status = ndb.IntegerProperty(default=0)
	position = ndb.StringProperty()
	department = ndb.IntegerProperty(default=0)

class Vendor(BasicModel):
	name = ndb.StringProperty()
	short_name  = ndb.StringProperty()
	address  = ndb.StringProperty()
	TIN  = ndb.StringProperty()
	is_VAT_registered = ndb.BooleanProperty(default=True)
	contact_no  = ndb.StringProperty()

class Client(BasicModel):
	client_name = ndb.StringProperty()
	contact_no = ndb.StringProperty()
	status = ndb.IntegerProperty(default=0)