from django.conf.urls.defaults import *
from django.http import HttpResponse

from . import views

urlpatterns = patterns('',
	url(r'^employees/$', views.list_employee, name='list_employee'),
	url(r'^employees/add$', views.create_employee, name='create_employee'),
	url(r'^employees/edit/(?P<employee_id>.*)$', views.update_employee, name='update_employee'),
	url(r'^vendors/$', views.list_vendor, name='list_vendor'),
	url(r'^vendors/add$', views.create_vendor, name='create_vendor'),
	url(r'^vendors/edit/(?P<vendor_id>.*)$', views.update_vendor, name='update_vendor'),
	url(r'^vendors/(?P<vendor_id>.*)$', views.list_vendor_details, name='list_vendor_details'),
	url(r'^clients/$', views.list_client, name='list_client'),
	url(r'^clients/add$', views.create_client, name='create_client'),
	url(r'^clients/edit/(?P<client_id>.*)$', views.update_client, name='update_client'),
	url(r'^$', views.main, name='main'),
)