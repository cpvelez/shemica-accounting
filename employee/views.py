from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from calendar import monthrange
from cStringIO import StringIO

from google.appengine.api import users
from google.appengine.api import memcache
from google.appengine.ext import deferred
from google.appengine.ext import ndb

from budget.models import *
from employee.models import *

from helper import ndb_helper, user_helper

from google.appengine.api import mail

import urllib
import logging
import StringIO

#Configs
#Email Configs
EMAIL_SENDER = "Shemica Inc. Accounting System <system@shemica-accounting.appspotmail.com>"
EMAIL_SENDER_NAME = "Shemica Inc. Accounting System"

#Vendor Email Recipient
VENDOR_EMAIL_RECIPIENTS = "cpcvelez@gmail.com"
SITE_LINK_TO_VENDORS = "https://shemica-accounting.appspot.com/entities/vendors/"


def main(request):
	return render(request, 'budget/index.html', {},
        content_type="text/html")
		
def list_employee(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_objects = Employee.query().order(Employee.first_name).fetch()
		employees_dict = ndb_helper.filter_results(employee_objects)
		user_access_type = user_helper.get_user_access_type()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'employee/viewEmployee.html', {"employees": employees_dict, 'user_access_type': user_access_type, 'base_template': base_template})

def create_employee(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		return render(request, 'employee/addEmployee.html', {'base_template': base_template}, content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		try:
			if checkEmployeeName(request, request.POST['employee_first_name'], request.POST['employee_last_name'], request.POST['employee_email']):
				return HttpResponse('duplicate')

			key = request.POST['employee_email']
			employee = Employee(id=key)

			employee.first_name = request.POST['employee_first_name']
			employee.middle_name = request.POST['employee_middle_name']
			employee.last_name = request.POST['employee_last_name']
			employee.email = request.POST['employee_email']
			employee.department = int(request.POST['employee_department'])
			employee.position = request.POST['employee_position']
			employee.type = int(request.POST['employee_type'])
			employee.access_type = int(request.POST['employee_access_type'])
			employee.active_status = int(request.POST['employee_active_status'])
			ndb_helper.update_meta_data(employee, True)
			employee.put()
			employee_objects = Employee.query().order(-Employee.first_name).fetch()
			employees_dict = ndb_helper.filter_results(employee_objects)
			memcache.add('list_employees', employees_dict)
			memcache.flush_all()
		except Exception as e:
			logging.info(e)
			return HttpResponse(str(e))
		return HttpResponse('ok')

def update_employee(request, employee_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ADMIN_ONLY):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		employee_key = ndb.Key(urlsafe=employee_id)
		employee = employee_key.get()
		employee_key = employee.key.urlsafe()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'employee' : employee,
			'employee_key' : employee_key,
			'base_template': base_template
		}
		return render(request, 'employee/editEmployee.html', context,
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		if checkEmployeeName(request, request.POST['employee_first_name'], request.POST['employee_last_name'], request.POST['employee_email'], employee_id):
				return HttpResponse('duplicate')

		response = update_employee_post(request, employee_id)
		# Update employee list in cache
		employee_objects = Employee.query().order(-Employee.first_name).fetch()
		employees_dict = ndb_helper.filter_results(employee_objects)
		memcache.add('list_employees', employees_dict)
		return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def update_employee_post(request, employee_id):
	# Get Employee
	employee_key = ndb.Key(urlsafe=employee_id)
	employee = employee_key.get()
	# Update Employee Info
	employee.first_name = request.POST['employee_first_name']
	employee.middle_name = request.POST['employee_middle_name']
	employee.last_name = request.POST['employee_last_name']
	employee.email = request.POST['employee_email']
	employee.position = request.POST['employee_position']
	employee.department = int(request.POST['employee_department'])
	employee.type = int(request.POST['employee_type'])
	employee.access_type = int(request.POST['employee_access_type'])
	employee.active_status = int(request.POST['employee_active_status'])
	ndb_helper.update_meta_data(employee, False)
	employee.put()
	
	# Removed updating of employee names in created items
	'''
	# Update Employee Info in Budgets
	for item in budgets:
		if item['holder'] == employee_id:
			budget_key = ndb.Key(urlsafe=item['key'])
			budget = budget_key.get()
			budget.holder_name = employee.first_name + " " + employee.last_name
			budget.put()

	# Update Employee Info in Released Funds
	for item in released_funds:
		if item['point_person'] == employee_id:
			released_fund_key = ndb.Key(urlsafe=item['key'])
			released_fund = released_fund_key.get()
			released_fund.point_person_name = employee.first_name + " " + employee.last_name
			released_fund.put()
		if item['driver_id'] == employee_id:
			released_fund_key = ndb.Key(urlsafe=item['key'])
			released_fund = released_fund_key.get()
			released_fund.driver_name = employee.first_name + " " + employee.last_name
			released_fund.put()
	# Update Employee Info in Expenses
	for item in expenses:
		if item['charged_to_id'] == employee_id:
			expense_key = ndb.Key(urlsafe=item['key'])
			expense = expense_key.get()
			expense.charged_to_name = employee.first_name + " " + employee.last_name
			expense.put()
	'''
			
	return HttpResponse('ok')

def list_vendor(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		vendor_objects = Vendor.query().order(Vendor.name).fetch()
		vendors_dict = ndb_helper.filter_results(vendor_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'employee/viewVendor.html', {"vendors": vendors_dict, 'base_template': base_template})

def create_vendor(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		return render(request, 'employee/addVendor.html', {'base_template': base_template},
	    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		try:
			if checkVendorName(request, request.POST['vendor_name']):
				return HttpResponse('duplicate')
	
			vendor = Vendor()

			vendor.name = request.POST['vendor_name']
			vendor.short_name = request.POST['vendor_short_name']
			vendor.address = request.POST['vendor_address']
			vendor.TIN = request.POST['vendor_TIN']
			vendor.is_VAT_registered = bool(int(request.POST['vendor_is_VAT_registered']))
			vendor.contact_no = request.POST['vendor_contact_no']
			ndb_helper.update_meta_data(vendor, True)
			vendor.put()
			
			vendor_objects = Vendor.query().order(Vendor.name).fetch()
			vendors_dict = ndb_helper.filter_results(vendor_objects)
			memcache.add('list_vendors', vendors_dict)
			
			notify_new_vendor(vendor)

		except Exception as e:
			return HttpResponse(str(e))
		return HttpResponse('ok')

def notify_new_vendor(vendor):
	email_subj_intro = "Vendor Added!"
	is_reg = "No"
	if bool(vendor.is_VAT_registered) == 0:
		is_reg = "No"
	else:
		is_reg = "Yes"
	mail.send_mail(sender=EMAIL_SENDER,to=VENDOR_EMAIL_RECIPIENTS,subject=email_subj_intro + " " + str(vendor.name),
		body="""Dear Carlo:

This is to notify you that a vendor was submitted with the ff. details:

Vendor Name: """ + str(vendor.name) + """
Short Name: """ + str(vendor.short_name) + """
Address: """ + str(vendor.address) + """
TIN: """ + str(vendor.TIN) + """
VAT Registered: """ + str(is_reg) + """
Submitted By: """ + str(vendor.created_by) + """

For more details, click the link below:
""" + SITE_LINK_TO_VENDORS + vendor.key.urlsafe() + """

This is an automatically generated email, please do not reply.

Regards,
""" + EMAIL_SENDER_NAME)

	return HttpResponse('ok')

def update_vendor(request, vendor_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		vendor_key = ndb.Key(urlsafe=vendor_id)
		vendor = vendor_key.get()
		vendor_key = vendor.key.urlsafe()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'vendor' : vendor,
			'vendor_key' : vendor_key,
			'base_template': base_template
		}
		return render(request, 'employee/editVendor.html', context,
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		if checkVendorName(request, request.POST['vendor_name'], vendor_id):
			return HttpResponse('duplicate')
		# Update Vendor Post
		response = update_vendor_post(request, vendor_id)
		# Ignore vendore names in created items
		'''
		# Update expenses list in cache
		expense_objects = Expense.query().order(-Expense.apv_number).fetch()
		expenses = ndb_helper.filter_results(expense_objects)
		memcache.add('list_expenses', expenses)
		'''
		# Update vendors list in cache
		vendor_objects = Vendor.query().order(Vendor.name).fetch()
		vendors_dict = ndb_helper.filter_results(vendor_objects)
		memcache.add('list_vendors', vendors_dict)
		return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def update_vendor_post(request, vendor_id):
	# Get Vendor
	vendor_key = ndb.Key(urlsafe=vendor_id)
	vendor = vendor_key.get()
	# Update vendor
	vendor.name = request.POST['vendor_name']
	vendor.short_name = request.POST['vendor_short_name']
	vendor.address = request.POST['vendor_address']
	vendor.TIN = request.POST['vendor_TIN']
	vendor.is_VAT_registered = bool(int(request.POST['vendor_is_VAT_registered']))
	vendor.contact_no = request.POST['vendor_contact_no']
	ndb_helper.update_meta_data(vendor, False)
	vendor.put()
	
	# Update Vendor info in affected Expenses
	#for item in expenses:
	#	if item['vendor_id'] == vendor_id:
	#		expense_key = ndb.Key(urlsafe=item['key'])
	#		expense = expense_key.get()
	#		expense.vendor_name = vendor.name
	#		expense.vendor_short_name = vendor.short_name
	#		expense.vendor_tin = vendor.TIN
	#		expense.vendor_address = vendor.address
	#		expense.vendor_is_vat_registered = vendor.is_VAT_registered
	#		expense.put()

	return HttpResponse('ok')

@ensure_csrf_cookie
def list_vendor_details(request, vendor_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		# Get Vendor
		vendor_key = ndb.Key(urlsafe=vendor_id)
		vendor = vendor_key.get()
		vendor_key = vendor.key.urlsafe()
		# Get Expenses Paid to Vendor
		expense_objects = Expense.query(Expense.vendor_id == vendor_id).order(-Expense.date).fetch()
		expenses_dict = ndb_helper.filter_results(expense_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		
		#  Paginate
		paginator = Paginator(expenses_dict, 20)
		page = request.GET.get('page')
		paged = request.GET.get('paged')
		try:
			expenses_dict = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			expenses_dict = paginator.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			expenses_dict = paginator.page(paginator.num_pages)
		if paged:
			base_template = user_helper.TEMPLATE_BLANK

		context = {
			'vendor' : vendor,
			'vendor_key': vendor_key,
			'expenses' : expenses_dict,
			'base_template': base_template
		}
		return render(request, 'employee/viewVendorDetails.html', context, content_type="text/html")

def list_client(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		client_objects = Client.query().order(-Client.client_name).fetch()
		clients_dict = ndb_helper.filter_results(client_objects)
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())

		return render(request, 'employee/viewClient.html', {"clients": clients_dict, 'base_template': base_template})
	
def create_client(request):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		return render(request, 'employee/addClient.html', {'base_template': base_template},
	    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		try:
			if checkClientName(request, request.POST['client_name']):
				return HttpResponse('duplicate')
			
			client = Client()

			client.client_name = request.POST['client_name']
			client.contact_no = request.POST['contact_no']
			client.status = int(request.POST['active_status'])
			
			ndb_helper.update_meta_data(client, True)
			client.put()
			
			client_objects = Client.query().order(Client.client_name).fetch()
			clients = ndb_helper.filter_results(client_objects)
			memcache.add('list_client', clients)
		except Exception as e:
			return HttpResponse(str(e))
		return HttpResponse('ok')

def update_client(request, client_id):
	# Check user's permission
	if not user_helper.is_user_permitted(user_helper.PERMISSION_ALL_USERS):
		return render(request, 'budget/unathorizedAccessPage.html', {}, content_type="text/html")

	if request.method == 'GET':
		client_key = ndb.Key(urlsafe=client_id)
		client = client_key.get()
		client_key = client.key.urlsafe()
		base_template = user_helper.getBaseOfAccessType(user_helper.get_user_access_type())
		context = {
			'client' : client,
			'client_key' : client_key,
			'base_template': base_template
		}
		return render(request, 'employee/editClient.html', context,
    		content_type="text/html")
	elif request.method == 'POST' and request.is_ajax:
		if checkClientName(request, request.POST['client_name'], client_id):
				return HttpResponse('duplicate')
		response = update_client_post(request, client_id)
		# Update client list in cache
		client_objects = Client.query().order(Client.client_name).fetch()
		clients = ndb_helper.filter_results(client_objects)
		memcache.add('list_client', clients)
		return response

@ensure_csrf_cookie
@ndb.transactional(retries=3, xg=True)
def update_client_post(request, client_id):
	# Get Client
	client_key = ndb.Key(urlsafe=client_id)
	client = client_key.get()
	# Update Client
	client.client_name = request.POST['client_name']
	client.contact_no = request.POST['contact_no']
	client.status = int(request.POST['active_status'])
	ndb_helper.update_meta_data(client, False)
	client.put()

	return HttpResponse('ok')

def getAllExpenses():
	expense_objects = Expense.query().order().fetch()
	expenses = ndb_helper.filter_results(expense_objects)
	return expenses

def getAllBudgets():
	budget_objects = Budget.query().order().fetch()
	budgets = ndb_helper.filter_results(budget_objects)
	return budgets

def getAllReleasedFunds():
	released_fund_objects = ReleasedFund.query().order().fetch()
	released_funds_dict = ndb_helper.filter_results(released_fund_objects)
	return released_funds_dict

def checkVendorName(request, vendor_name, vendor_key=None):
	vendor_objects = Vendor.query().order(Vendor.name).fetch()
	vendors = ndb_helper.filter_results(vendor_objects)
	for item in vendors:
		if item['name'] == vendor_name:
			if not vendor_key:
				return True
			else:
				if item['key'] != vendor_key:
					return True
	return False

def checkClientName(request, client_name, client_key=None):
	client_objects = Client.query().order(-Client.client_name).fetch()
	clients = ndb_helper.filter_results(client_objects)
	for item in clients:
		if item['client_name'] == client_name:
			if not client_key:
				return True
			else:
				if item['key'] != client_key:
					return True
	return False

def checkEmployeeName(request, first_name, last_name, email, employee_key=None):
	employee_objects = Employee.query().order(Employee.first_name).fetch()
	employees = ndb_helper.filter_results(employee_objects)

	for item in employees:
		if item['first_name'] == first_name and item['last_name'] == last_name or item['email'] == email:
			if not employee_key:
				return True
			else:
				if item['key'] != employee_key:
					return True
	return False